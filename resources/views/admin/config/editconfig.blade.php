<?php $page="addconfig";?>
@extends('layout.mainlayout')
@section('content')		
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')                
			@slot('title') Config Add @endslot
			@slot('title_1') <a href="{{ url('listconfig') }}"> Config </a> <i class='fas fa-angle-right'></i> Edit Config @endslot
		@endcomponent
        <style>
        /* Custom styles for error messages */
        label.error {
            color: red;
            font-size: 14px;
            display: block;
            margin-top: 5px;
        }
        .required-field::after {
      content: ' *';
      color: #ff0000; /* Adjust color as needed */
    }
    /* Add this CSS to remove unwanted styling on the dynamically added rows */
#dynamicAddRemove tr {
    border: none; /* Remove any border on all rows */
}

#dynamicAddRemove tr.dynamic-row {
    /* Additional styling for dynamically added rows if needed */
    background-color: #fff; /* Set the background color to white */
}

    </style>
     
        <!-- /add -->
        <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <form id="editconfig" method="post" action="postupdateconfig">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="id" value="{{$configInfo->id}}"/>
                  <div class="col-lg-3 col-sm-6 col-12">
                  <div class="form-group">
                      <label>Name<span class="required-field"></span></label>
                      <input type="text" placeholder="Name" value="{{$configInfo->name}}" readonly name="name" onkeyup="convertToSlug(event.target.value)">
                    </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                    <div class="form-group">
                      <label>Slug<span class="required-field"></span></label>
                      <input type="text" placeholder="Slug" name="slug" id="slug" value="{{$configInfo->slug}}" readonly>
                    </div>
                    </div>

                </form>
            </div>
          </div> 
<!-- rows end -->

<div class="row">
    <div class="col-lg-12">
      <div class="card mb-4">
        <div class="card-body">
          <div class="row">
          <form id="editconfig" method="post" action="postupdateconfigoption">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="hidden" name="id" value="{{ $configInfo->id }}"/>
              <div class="col-md-6">
              <table class="table" id="dynamicAddRemove">
                  <!-- Add data-index attribute to keep track of option index -->
                  @foreach ($allconfigoptionInfo as $key => $configoptionInfo)
                  <tr data-index="{{ $key }}">
                      <td>
                          <input type="text" name="addMoreInputFields[{{ $key }}][subject]"
                              placeholder="Enter subject" value="{{ $configoptionInfo->optionname }}"
                              class="form-control" />
                      </td>
                      <td>
                          <button type="button" class="btn btn-danger remove-input-field" style="background-color: #dc3545!important" onclick="deleteConfigurationConfirmation('{{$configoptionInfo->id}}')">Delete</button>
                      </td>
                  </tr>
                  @endforeach
              </table>
                    <button type="button" id="dynamic-add" class="btn btn-primary">Add Option</button>
                    <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div> 
        </div>
      </div>
    </div>
  </div>

        </div>
      </div>


 
<script>
   
    jQuery(document).ready(function ($) {
    $("#addconfig").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: "Name is required"
            },
            errorClass: "error",
            submitHandler: function (form) {
                form.submit();
            }
        });

        var i = {{ count($allconfigoptionInfo) }};
    $("#dynamic-add").click(function () {
      
        ++i;
        $("#dynamicAddRemove").append('<tr data-index="' + i + '"><td><input type="text" name="addMoreInputFields[' + i +
            '][subject]" placeholder="Enter subject" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-input-field" style="background-color: #dc3545!important">Delete</button></td></tr>'
        );
    });
  })

  function convertToSlug(Text) {
    console.log(); ;
    document.getElementById('slug').value = Text.toLowerCase()
    .replace(/ /g, '-')
    .replace(/[^\w-]+/g, '');
  }

  

    $(document).on('click', '.remove-input-field', function () {
        var row = $(this).closest('tr');
        var index = row.data('index');
        row.remove();
        $('#dynamicAddRemove tr').each(function () {
            var currentIndex = $(this).data('index');
            if (currentIndex > index) {
                $(this).data('index', currentIndex - 1);
                $(this).find('input').attr('name', 'addMoreInputFields[' + (currentIndex - 1) + '][subject]');
            }
        });
    });
    
    function deleteConfigurationConfirmation(id) {
       Swal.fire({
           title: "Are you sure you want to delete ?",
           type: "warning",
           showCancelButton: true,
           confirmButtonClass: "btn-danger",
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: false,
           closeOnCancel: false
}).then((result) => {
   if (result.isConfirmed) {
       
       //location.href = 'deleteconfiguration?id=' + id;
       var csrfToken = $('meta[name="csrf-token"]').attr('content');
       var configid=$('#id').val();
       $.ajax({
                    url: '/deleteconfiguration',
                    type: 'GET',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: id,
                       },
                    success: function(response) {
                        console.log(response);
                        if(response ==1)
                        {
                            window.location.href = '/listconfig';
                        }
                        // Handle success response
                    },
                    error: function(error) {
                        console.error(error);
                        // Handle error
                    }
                });
   }
});

   }
</script>
@endsection
