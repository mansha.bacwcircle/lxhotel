<?php $page="addconfig";?>
@extends('layout.mainlayout')
@section('content')		
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')                
			@slot('title') Edit Config  @endslot
			@slot('title_1')  <a href="{{ url('listconfig') }}"> Config </a> <i class='fas fa-angle-right'></i> Add Config @endslot
		@endcomponent
        <style>
        /* Custom styles for error messages */
        label.error {
            color: red;
            font-size: 14px;
            display: block;
            margin-top: 5px;
        }
        .required-field::after {
      content: ' *';
      color: #ff0000; /* Adjust color as needed */
    }
    </style>
     
        <!-- /add -->
        <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <form id="addconfig" method="post" action="postaddconfig">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="col-lg-3 col-sm-6 col-12">
                  <div class="form-group">
                      <label>Name<span class="required-field"></span></label>
                      <input type="text" placeholder="Name" name="name" onkeyup="convertToSlug(event.target.value)">
                    </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                    <div class="form-group">
                      <label>Slug<span class="required-field"></span></label>
                      <input type="text" placeholder="Slug" name="slug" id="slug" readonly>
                    </div>
                    </div>

                    <div class="col-lg-12">
                    <button class="btn btn-submit me-2" type="submit">Save </button>
                    </div>
                </form>
            </div>
          </div> 
        </div>
      </div>
<script>
   
    jQuery(document).ready(function ($) {
    $("#addconfig").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: "Name is required"
            },
            errorClass: "error",
            submitHandler: function (form) {
                form.submit();
            }
        });
  })

  function convertToSlug(Text) {
    console.log(); ;
    document.getElementById('slug').value = Text.toLowerCase()
    .replace(/ /g, '-')
    .replace(/[^\w-]+/g, '');
  }

</script>
@endsection
