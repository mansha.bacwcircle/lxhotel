<div class="card">
            <div class="card-body">
            <div class="modal fade" id="flashMessageModal" tabindex="-1" role="dialog" aria-labelledby="flashMessageModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content success-modal">
                    
                    <div class="modal-body" style="text-align:center;">
                            @if(Session::has('messageType') && Session::has('message'))
                                <h5 class="flashMessageModal" id="flashMessageModal">
                                    @if(Session::has('messageType') && Session::has('message'))
                                        @if(Session::get('messageType') === 'fail')
                                            <i class="fas fa-times-circle failure-icon text-red"></i>
                                        @elseif(Session::get('messageType') === 'success')
                                            <i class="fas fa-check-circle success-icon success-text"></i>
                                        @endif
                                    @endif
                                </h5>
                                <h5 class="flashMessageModal {{ Session::get('messageType') === 'fail' ? 'text-red' : 'success-text' }}">
                                    {{ Session::get('message') }}
                                </h5>
                            @endif
                    </div>
                    
                </div>
            </div>
        </div>
                
                @if(count($allconfigs) > 0) 
                <div class="table-responsive">
                    <table class="table" id="configlist">
                        <thead>
                            <tr>
                                <th>Sr.No.</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1?>  
                            @foreach ($allconfigs as $configs)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$configs->name}}</td>
                                <td>{{strip_tags($configs->slug)}}</td>                          
                                <td>
                                    @if($configs->status == "active")
                                        <a href="#" id="statusid{{$configs->id}}" class="btn btn-sm font-sm btn-light rounded" style="color:green" onclick="statuschange('{{$configs->id}}','{{$configs->status}}')">
                                        Active
                                        </a>  
                                    @else
                                        <a href="#" id="statusid{{$configs->id}}"  class="btn btn-sm font-sm btn-light rounded" style="color:red" onclick="statuschange('{{$configs->id}}','{{$configs->status}}')">
                                        Inactive
                                        </a>
                                    @endif
                                </td>                            
                                <td class="text-center">                              
                                    <a href="{{URL::to('editconfig')}}/{{$configs->id}}" class="btn btn-md rounded font-sm">
                                    <img src="{{ URL::asset('/assets/img/icons/edit.svg')}}" alt="img">
                                    </a>
                                    <a href="#" class="btn btn-md rounded font-sm">
                                    <img src="{{ URL::asset('/assets/img/icons/delete.svg')}}" alt="img"  onclick="showDeleteConfirmation('{{$configs->id}}')">
                                    </a>                              
                                </td>
                            </tr>
                            <?php $i++ ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
        <div>No Record Found</div>
        @endif
            </div>
        </div>
        <!-- /product list -->
        <script>
           
   $(document).ready(function() {
       // Show the modal if flash messages are present
       @if (Session::has('messageType') && Session::has('message'))
    $('#flashMessageModal').modal('show');
    setTimeout(function() {
        $('#flashMessageModal').modal('hide');
        window.location.href = "{{ url('/clearSession') }}";
    }, 2000); 
@endif
       var dataTable = $('#configlist').DataTable({  
        paging: true,
        searching: true,
        ordering: false
       
   });
       
   });
   function statuschange(id,status){
       Swal.fire({
           title: "Are you sure to change the status?",
           type: "warning",
           showCancelButton: true,
           confirmButtonClass: "btn-danger",
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: false,
           closeOnCancel: false
       }).then(function(isConfirm) {
               if (isConfirm.value) {
                   $.ajax({
                       type   : "POST",
                       url    : "configstatuschange",
                       data   : {id:id,oldstatus:status},
                       headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                       success:function(data){
                       if(data == 1){
                           $("#statusid"+id).replaceWith('<a title="Click to inactive" class="btn btn-sm font-sm btn-light rounded" style="color:green" id="statusid'+ id +'" onclick="statuschange('+"'"+id+"'"+','+"'publish'"+')">Active</a>');
                           Swal.fire({
                                title: "Config status changed successfully!",
                                icon: "success",
                            });
                       }else if(data == 2){
                           $("#statusid"+id).replaceWith('<a title="Click to active" class="btn btn-sm font-sm btn-light rounded" style="color:red" id="statusid'+ id +'" onclick="statuschange('+"'"+id+"'"+','+"'unpublish'"+')">Inactive</a>');
                           Swal.fire({
                                title: "Config status changed successfully!",
                                icon: "success",
                            });
                       }
                       }  
                   });
               } 
           });
    }
   // Function to show the confirmation dialog
   function showDeleteConfirmation(id) {
       Swal.fire({
           title: "Are you sure you want to delete ?",
           type: "warning",
           showCancelButton: true,
           confirmButtonClass: "btn-danger",
           confirmButtonText: "Yes",
           cancelButtonText: "No",
           closeOnConfirm: false,
           closeOnCancel: false
}).then((result) => {
   if (result.isConfirmed) {
       //alert("delete");
       location.href = 'deleteconfig?id=' + id;
   }
});

   }
</script>
     
      