<?php $page="brandlist";?>
@extends('layout.mainlayout')
@section('content')	
<div class="page-wrapper">
    <div class="content">
       

        <div class="page-header">
        @component('components.pageheader')                
			@slot('title') Config List @endslot
			@slot('title_1') Manage your Config @endslot
		@endcomponent
                <div class="page-btn">
							<a href="{{url('/addconfig')}}" class="btn btn-added"><img src="assets/img/icons/plus.svg" class="me-2" alt="img"> Add Config</a>
				</div>
         </div>
         @include('admin.config.configlist')
        
    </div>
</div>
@endsection