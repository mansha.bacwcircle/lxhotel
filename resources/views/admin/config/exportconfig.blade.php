<head>
    <style>
        #configs {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }
        
        #configs td, #configs th {
        border: 1px solid #ddd;
        padding: 8px;
        }

        #configs tr:nth-child(even){background-color: #f2f2f2;}
        
        #configs tr:hover {background-color: #ddd;}
        
        #configs th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
        }

        a{
        text-decoration:none;
        }
    </style>
</head>
<section class="content-main">
    <div class="content-header">
        <div>
            <h2 class="content-title card-title">Configs</h2>
        </div>
    </div>
    <table id="configs" class="table table-hover ">
        <thead>
            <tr>
                <th>ID</th>
                <th scope="col">Name</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1?>  
            @foreach ($configs as $config)
            <tr>
                <td>{{$i}}</td>
                <td>{{$config->name}}</td>                                             
            </tr>
            <?php $i++ ?>
            @endforeach
        </tbody>
    </table>
</section>
