<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Stone River Total Beverages | {{ $mailData['subject'] }}</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f4f4f4;
            color: #333;
        }
        .container {
            max-width: 600px;
            margin: auto;
            padding: 20px;
            border: 1px solid #ddd;
            background-color: #fff;
        }
        h2 {
            color: #e77600;
        }
        img {
            max-width: 100%;
            height: auto;
            margin-top: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: left;
        }
    </style>
</head>
<body>

    <div class="container">
        <h3>{{ $mailData['title'] }}</h3>
        <p> <img src="logo.png" alt="Wine Shop Logo"> Visit our store or shop online to discover more premium wines. Cheers!</p>
        <p>{{ $mailData['subject'] }}</p>

        <img src="https://images.pexels.com/photos/2702805/pexels-photo-2702805.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="Newsletter" style="max-width: 100%; height: auto; margin-top: 20px;">

        <p>{!! $mailData['body'] !!}</p>

        <strong>Explore our wide selection of exquisite wines, carefully curated for your taste buds. Whether you're a connoisseur or a casual wine enthusiast, we have something for everyone.</strong>


        
        <p>Thank you for being a part of our newsletter!</p>
    </div>

</body>
</html>
