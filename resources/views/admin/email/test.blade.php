<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stone River Total Beverages</title>
</head>
<body style="font-family: Arial, sans-serif;">

    <div style="max-width: 600px; margin: auto; padding: 20px; background-color: #f4f4f4; border: 1px solid #ddd;">

        <h1 style="color: #e77600;">Order Confirmation</h1>

        <p><strong>Name:</strong> {{$mailData['title']}}</p>
        <p><strong>SUB:</strong> {{$mailData['body']}}</p>

        <hr style="border: 1px solid #ddd;">

        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facere quod iure id porro? Molestias, ullam,
            labore recusandae quidem atque nisi eum doloribus ex consectetur quia nihil. In ullam labore molestiae.</p>

        <hr style="border: 1px solid #ddd;">

        <p>Thank you for shopping with us!</p>

    </div>

</body>
</html> 
