<?php $page = 'signin'; ?>
@extends('layout.mainlayout')
@section('content')
    <style>
        /* Custom styles for error messages */
        label.error {
            color: red !important;
            font-size: 14px;
            display: block;
            margin-top: 5px;
        }
        .login-wrapper .login-content .toggle-password {
            top:24px!important;
        }
    </style>
    <!-- <Form action="{{-- {{ route('signin.custom') }} --}}" method="POST" class="account-content"> -->
    <form action="postadminlogin" method="post" id="postadminlogin">
        @csrf
        <div class="login-wrapper">
            <div class="login-content">
                <div class="login-userset">
                    <div class="login-logo logo-normal">
                        <img src="{{ URL::to($sitesettings['logo']) }}" class="rounded-pill" alt="img" >
                    </div>
                    <a href="{{ url('index') }}" class="login-logo logo-white">
                        <img src="{{ URL::asset('/assets/img/logo-white.png') }}" alt="">
                    </a>
                    <div class="login-userheading">
                        <h3>Sign In</h3>
                        <h4>Please login to your account</h4>
                    </div>
                    @if (session('message'))
                        @if (session('messageType') == 'success')
                            <div class="alert alert-success text-center" id="success-alert">
                                <h5>{{ session()->get('message') }}</h5>
                            </div>
                        @else
                            <div class="alert alert-danger text-center" id="error-alert">
                                <h5>{{ session()->get('message') }}</h5>
                            </div>
                        @endif
                    @endif
                    <div class="form-login">
                        <label>Email</label>
                        <div class="form-addons">
                            <input type="text" name="email" id="Email">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <img src="{{ URL::asset('/assets/img/icons/mail.svg') }}" alt="img">
                        </div>
                    </div>
                    <div class="form-login">
                        <label>Password</label>
                        <div class="pass-group">
                            <input type="password" class="pass-input" name="password" id="password">
                            <span class="fas toggle-password fa-eye-slash mb-2"></span>
                        </div>
                    </div>

                    <div class="form-login">
                        <div class="alreadyuser">
                            <h4><a href="{{ url('forgetpassword') }}" class="hover-a">Forgot Password?</a></h4>
                        </div>
                    </div>
                    <div class="form-login">
                        <button class="btn btn-login" type="submit">Sign In</button>
                    </div>

                </div>
            </div>
            <div class="login-img">
                <img src="{{ URL::asset('/assets/img/login.jpg') }}" alt="img">
            </div>
        </div>
    </Form>
    <script>
        // jQuery.noConflict();
        jQuery(document).ready(function($) {
            // Adding custom method to check for spaces
            $.validator.addMethod("noSpace", function(value, element) {
                return value.indexOf(" ") === -1; // Return true if no space is found
            }, "Spaces are not allowed in the password.");

            $("#postadminlogin").validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                        noSpace: true, // Using the custom noSpace method
                        // You can adjust the minimum length as needed
                        nowhitespace: true,
                    }
                },
                messages: {
                    email: {
                        required: "Please fill in the required email.",
                        email: "Please enter a valid emailid.",
                    },
                    password: {
                        required: "Please fill in the required password.",
                        nowhitespace: "White space at the beginning is not allowed",

                    }
                },
                errorClass: "error",
                submitHandler: function(form) {
                    form.submit();
                }
            });
            $.validator.addMethod("nowhitespace", function(value, element) {
      // Check if there's whitespace at the beginning
      return this.optional(element) || !/^\s/.test(value);
    }, "White space at the beginning is not allowed");

            $('#reload').on('click', function() {
                $.ajax({
                    type: 'GET',
                    url: 'reloadcaptcha', // Make sure this URL is correctly set up in your routes and controller
                    success: function(data) {
                        $(".captcha span").html(data.captcha);
                    },
                    error: function() {
                        console.log('Error reloading captcha');
                    }
                });
            });
        });
            // Automatically close the success message after 1 second
            setTimeout(function() {
                $("#success-alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove();
                });
            }, 1000);

            // Automatically close the error message after 1 second
            setTimeout(function() {
                $("#error-alert").fadeTo(500, 0).slideUp(500, function(){
                    $(this).remove();
                });
            }, 3000);

    </script>
@endsection
