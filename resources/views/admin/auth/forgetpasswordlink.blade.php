<?php $page = 'signin'; ?>
@extends('layout.mainlayout')
@section('content')
    <style>
        label.error {
            color: red !important;
            display: block;
            margin-top: 5px;
        }

        .login-wrapper .login-content .toggle-password {
            top: 22px !important;
        }

        p {
            position: absolute;
            right: 10px;
            top: 7px;
            cursor: pointer;
        }
    </style>

    <div class="account-content">
        <div class="login-wrapper">
            <div class="login-content">
                <div class="login-userset ">
                    <div class="login-logo">
                        <img src="{{ URL::to($sitesettings['logo']) }}" alt="img">
                    </div>
                    @if (session('message'))
                        <div class="alert alert-{{ session('messageType', 'info') }}" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    <div class="login-userheading">
                        <h3>Reset password</h3>
                    </div>
                    <form id="forgrtpassword" method="POST" action="{{ url('postresetpassword') }}">
                        <input type="hidden" name="token" value="{{ $token }}">
                        @csrf
                        <div class="form-login">
                            <label>Email</label>
                            <div class="form-addons">
                                <input type="email" placeholder="Enter your email address" name="email">
                                <img src="{{ URL::asset('/assets/img/icons/mail.svg') }}" alt="img">
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-login">
                            <label>Password</label>
                            <div class="pass-group">
                                <input type="password" class="pass-input" name="password" id="password"
                                    placeholder="Enter your password">
                                    <span class="fas toggle-password fa-eye-slash mb-2"></span>
                                    <p id="toggle-password">show</p>
                            </div>
                        </div>
                        <div class="form-login">
                            <label>Confirm Password</label>
                            <div class="pass-group">
                            <input type="password" class="pass-inputs" name="confirmpassword"
                                    id="confirmpassword" placeholder="Enter your confirm password">
                                    <span class="fas toggle-passwords fa-eye-slash"></span>
                            </div>
                        </div>
                        <input type="password" class="pass-input" name="confirmpassword" id="confirmPassword"
                            placeholder="Enter your confirm password">
                        <p id="toggle-cpassword">show</p>
                </div>
            </div>
            <div class="form-login">
                <button class="btn btn-login" type="submit"> Reset Password </button>
            </div>
            <div class="signinform text-center">
                <h4>Return to <a href="{{ url('/adminlogin') }}" class="hover-a"> Sigin </a></h4>
            </div>
            </form>
        </div>
    </div>
    <div class="login-img">
        <img src="{{ URL::asset('/assets/img/login.jpg') }}" alt="img">
    </div>
    </div>
    </div>
    <script>
        jQuery(document).ready(function($) {

            $.validator.addMethod("passwordRequirement", function(value, element) {
                    return this.optional(element) || /^(?=.*[a-z])(?=.*\d).{6,}$/.test(value);
                },
                "Your password must include at least one lowercase letter and one number and be at least 6 characters long"
                );


            $("#forgrtpassword").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        passwordRequirement: true,
                    },
                    confirmpassword: {
                        required: true,
                        equalTo: "#password", // Ensure the password matches
                    },
                },
                messages: {
                    email: {
                        required: "Please fill in the required email.",
                        email: "Please enter a valid email address."
                    },
                    password: {
                        required: "Please fill in the required password.",
                        minlength: "Your password must be at least 6 characters long",
                        pattern: "Your password must include at least one lowercase letter and one number"
                    },
                    confirmpassword: {
                        required: "Confirm password is required",
                        equalTo: "Passwords do not match",
                    },
                },
                errorClass: "error", // Apply the 'error' class to error labels
                submitHandler: function(form) {
                    form.submit();
                }
            });
        })

        $(document).ready(function() {
            $("#toggle-password").click(function() {
                var passwordField = $("#password");
                var type = passwordField.attr("type");
                if (type === "password") {
                    passwordField.attr("type", "text");
                    $("#toggle-password").text("Hide");
                } else {
                    passwordField.attr("type", "password");
                    $("#toggle-password").text("Show");
                }
            });

        });
        $(document).ready(function() {

            $("#toggle-cpassword").click(function() {
                var passwordField = $("#confirmPassword");
                var type = passwordField.attr("type");
                if (type === "password") {
                    passwordField.attr("type", "text");
                    $("#toggle-cpassword").text("Hide");
                } else {
                    passwordField.attr("type", "password");
                    $("#toggle-cpassword").text("Show");
                }
            });
        });
    </script>
@endsection
