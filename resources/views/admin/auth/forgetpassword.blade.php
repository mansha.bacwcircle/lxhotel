<?php $page="forgetpassword";?>
@extends('layout.mainlayout')
@section('content')
<style>
     label.error {
            color: red!important;
            display: block;
            margin-top: 5px;
        }
    </style>
<div class="account-content">
    <div class="login-wrapper">
        <div class="login-content">
            <div class="login-userset ">
                <div class="login-logo">
                <img src="{{ URL::to($sitesettings['logo']) }}" alt="img">
                </div>
                @if (session('message'))
                    @if (session('messageType') == 'success')
                        <div class="alert alert-success text-center" id="success-alert">
                            <h5>{{ session()->get('message') }}</h5>
                        </div>
                    @else
                        <div class="alert alert-danger text-center" id="error-alert">
                            <h5>{{ session()->get('message') }}</h5>
                        </div>
                    @endif
                @endif
                <div class="login-userheading">
                    <h3>Forgot password?</h3>
                    <h4>If you've forgotten your password, don't worry! Follow the steps below to reset it</h4>
                </div>
                <form id="forgrtpassword" method="POST" action="{{ url('passwordresetrequest') }}">
                    @csrf
                    <div class="form-login">
                        <label>Email</label>
                        <div class="form-addons">
                            <input type="email" placeholder="Enter your email address" name="email">
                            <img src="{{ URL::asset('/assets/img/icons/mail.svg')}}" alt="img">
                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-login">
                        <button class="btn btn-login" type="submit"> Send Password Reset Link</button>
                    </div>
                    <div class="signinform text-center">
                            <h4>Return to <a href="{{url('/adminlogin')}}" class="hover-a"> Sigin </a></h4>
                            </div>
                </form>
            </div>
        </div>
        <div class="login-img">
            <img src="{{ URL::asset('/assets/img/login.jpg')}}" alt="img">
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($) {
      $("#forgrtpassword").validate({
        rules        : {
          email    : {
            required : true,
            email: true
          }
        },
        messages     : {
          email: {
            required: "Please fill in the required email.",
            email: "Please enter a valid email address."
          }
        },
        errorClass: "error", // Apply the 'error' class to error labels
        submitHandler: function (form) {
          form.submit();
        }
      });
  })
  // Automatically close the success message after 1 second
  setTimeout(function() {
      $("#success-alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
      });
  }, 1000);

  // Automatically close the error message after 1 second
  setTimeout(function() {
      $("#error-alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove();
      });
  }, 3000);
</script>
@endsection
