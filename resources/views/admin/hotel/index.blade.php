<?php $page="hotellist";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        <div class="page-header">
            @component('components.pageheader')
                @slot('title')Hotel List @endslot
                @slot('title_1') Manage Your Hotel @endslot

            @endcomponent

            <div class="page-btn">
            <a href="{{url('addhotel')}}" class="btn btn-added"><img src="assets/img/icons/plus.svg" class="me-2" alt="img"> Add Hotel</a>
            </div>
        </div>
        @include('admin.hotel.listhotel')
    </div>
</div>
@endsection
