<?php $page="edithotel";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')
          @slot('title') Edit Hotel @endslot
            @slot('title_1')
              <a href="{{url('hotellist')}}"> Hotel </a> <i class='fas fa-angle-right'></i> Edit Hotel
            @endslot
        @endcomponent
        <style>
        /* Custom styles for error messages */
        label.error {
            color: red;
            font-size: 14px;
            display: block;
            margin-top: 5px;
        }
        .required-field::after {
      content: ' *';
      color: #ff0000; /* Adjust color as needed */
    }
    </style>

        <div class="card">
            <div class="card-body">
            <form id="addbrand" method="post" enctype="multipart/form-data" action="/postupdatehotel">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" id="id" value="{{$hotelInfo->id}}">
                <div class="row">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Name<span class="required-field"></span></label>
                                <input type="text" id="name" required name="name" class="form-control"value="{{$hotelInfo->name}}">
                                <span id="errorName" style="color:red"> </span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Owner<span class="required-field"></span></label>
                                <input type="text" id="owner" required name="owner" class="form-control"value="{{$hotelInfo->owner}}">
                                <span id="errorOwner" style="color:red"> </span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Email<span class="required-field"></span></label>
                                <input class="form-control" type="email" id="email" name="email"value="{{$hotelInfo->email}}">
                            </div>
                        </div>
                    <div class="col-lg-12">
                    <button class="btn btn-submit me-2" type="submit" id="submitbtn">Update </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function ($) {

        $.validator.addMethod("customName", function(value, element) {
                return this.optional(element) || /^[A-Za-z0-9][A-Za-z0-9\s']{0,28}$/.test(value);
            }, "Please enter a valid name. Spaces are allowed only within the name, and it should not exceed 25 characters.");

      $("#addbrand").validate({
          rules: {
            name: {
                  required: true,
                  customName: true  // Custom rule for alphabets, numbers and spaces (not at start)
              }
          },
          messages: {
            name: {
              required: "Please enter a brand name",
                  customName: "Please enter a valid name. No leading spaces are allowed, and the name must not exceed 25 characters."
              }
          },
          errorClass: "error", // Apply the 'error' class to error labels
          submitHandler: function (form) {
              form.submit();
          }
      });
   })

        function performOperationOnSecondInput(processedValue) {
               var name =  document.getElementById('name').value;
               var brandid = document.getElementById('brandid').value;
               if (name !== '') {
                      $.ajax({
                       type: 'GET',
                       url: '/checkbrandname', // Laravel route pointing to the checkNameController
                       data: { name: name,id:brandid },
                       dataType: 'json',
                       success: function(response) {
                           console.log(response);
                           displayMessage(response.message);
                       }
                   });
               } else {
                   // Clear the message if the input is empty
                   displayMessage('');
               }
           }
           function displayMessage(message) {
                $('#resultMessagef').text(message);
               if(message=="Name is already in the database.") {
                  $('#submitbtn').prop('disabled', true); // Disable submit button
               } else {
                  $('#submitbtn').prop('disabled', false); // Enable submit button
               }
           }
</script>
@endsection
