<div class="card">
    <div class="card-body">
        <div class="modal fade" id="flashMessageModal" tabindex="-1" role="dialog" aria-labelledby="flashMessageModal"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content success-modal">

                <div class="modal-body" style="text-align:center;">
                    @if (Session::has('messageType') && Session::has('message'))
                        <h5 class="flashMessageModal" id="flashMessageModal">
                            @if (Session::has('messageType') && Session::has('message'))
                                @if (Session::get('messageType') === 'fail')
                                    <i class="fas fa-times-circle failure-icon text-red"></i>
                                @elseif(Session::get('messageType') === 'success')
                                    <i class="fas fa-check-circle success-icon success-text"></i>
                                @endif
                            @endif
                        </h5>
                        <h5
                            class="flashMessageModal {{ Session::get('messageType') === 'fail' ? 'text-red' : 'success-text' }}">
                            {{ Session::get('message') }}
                        </h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
        <div class="table-top">
            <div class="search-set">
            </div>
            @if (count($hotellist) > 0)
                <div class="wordset">
                    <ul>
                        <li>
                            <a data-bs-toggle="tooltip" href="{{ url('hotelpdf') }}" data-bs-placement="top"
                                title="PDF"><img src="{{ URL::asset('/assets/img/icons/pdf.svg') }}"
                                    alt="img"></a>
                        </li>
                        <li>
                            <a data-bs-toggle="tooltip" href="{{ url('exporthotelcsv') }}" data-bs-placement="top"
                                title="Excel"><img src="{{ URL::asset('/assets/img/icons/excel.svg') }}"
                                    alt="img"></a>
                        </li>
                    </ul>
                </div>
            @endif
        </div>
        @if (count($hotellist) > 0)
            <div class="table-responsive">
                <table class="table" id="hotellist">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Hotel Name</th>
                            <th>Owner Name</th>
                            <th>Email</th>
                            <th>Action</th>
                            <th>Send Document</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($hotellist as $hotel)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $hotel->name }}</td>
                                <td>{{ $hotel->owner }}</td>
                                <td>{{ $hotel->email }}</td>
                                <td>
                                    <a class="me-3" href="{{ url('edithotel') }}/{{ $hotel->id }}">
                                        <img src="{{ URL::asset('/assets/img/icons/edit.svg') }}" alt="img">
                                    </a>
                                    <a class="me-3" href="javascript:void(0);">
                                        <img src="{{ URL::asset('/assets/img/icons/delete.svg') }}" alt="img"
                                            onclick="showDeleteConfirmation('{{ $hotel->id }}')">
                                    </a>
                                </td>
                                <td>

                                <a href="{{ url('sendMail') }}/{{ $hotel->id }}" class="btn btn-sm font-sm btn-light rounded text-center"
                                        style="color: {{ $hotel->send_flag == '1' ? 'green' : 'red' }}">
                                        {{ $hotel->send_flag == '1' ? 'Send' : 'PendingToSend' }}
                                    </a>
                                    
                                </td>
                            </tr>
                            <?php $i++; ?>
                        @endforeach

                    </tbody>
                </table>
            </div>
        @else
            <div>No Record Found</div>
        @endif
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        // Show the modal if flash messages are present
        @if (Session::has('messageType') && Session::has('message'))
            $('#flashMessageModal').modal('show');
            setTimeout(function() {
                $('#flashMessageModal').modal('hide');
            }, 1500);
        @endif

        var dataTable = $('.newsletter').DataTable({
            paging: true,
            searching: true,
            ordering: false
        });
    });

    function showDeleteConfirmation(id) {
        Swal.fire({
            title: "Are you sure you want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then((result) => {
            if (result.isConfirmed) {
                // alert("delete");
                location.href = 'softdeletehotel?id=' + id;
            }
        });
    }
</script>
