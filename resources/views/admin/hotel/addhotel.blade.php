<?php $page = 'addhotel'; ?>
@extends('layout.mainlayout')
@section('content')
    <div class="page-wrapper">
        <div class="content">
            @component('components.pageheader')
                @slot('title')
                    Add Hotel
                @endslot
                @slot('title_1')
                    <a href="{{ url('admindashboard') }}">Home </a><i class='fas fa-angle-right'></i>
                    <a href="{{ url('hotellist') }}"> Hotel </a> <i class='fas fa-angle-right'></i> Create Hotel
                @endslot
            @endcomponent
            <style>
                /* Custom styles for error messages */
                label.error {
                    color: red;
                    font-size: 14px;
                    display: block;
                    margin-top: 5px;
                }

                .required-field::after {
                    content: ' *';
                    color: #ff0000;
                    /* Adjust color as needed */
                }
            </style>
            <div class="card">
                <div class="card-body">
                    @if (Session::has('messageType') && Session::has('message'))
                        <h5 style="font-size: 25px;color: red;">{{ Session::get('message') }}</h5>
                    @endif
                    <form id="addhotel" method="post" enctype="multipart/form-data" action="postaddhotel">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Name<span class="required-field"></span></label>
                                    <input type="text" id="name" required name="name" class="form-control">
                                    <span id="errorName" style="color:red"> </span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Owner<span class="required-field"></span></label>
                                    <input type="text" id="owner" required name="owner" class="form-control">
                                    <span id="errorOwner" style="color:red"> </span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Email<span class="required-field"></span></label>
                                    <input class="form-control" type="email" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <input type="submit" class="btn btn-submit me-2" id="submitbtn" value="Save">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($) {
            $.validator.addMethod("customName", function(value, element) {
          return this.optional(element) || /^[A-Za-z0-9][A-Za-z0-9\s]{0,29}$/.test(value);
      }, "Please enter a valid name. Spaces are allowed only within the name, and it should not exceed 25 characters.");

            $.validator.addMethod("custval", function(value, element) {
                    // This regex allows any character to start except for spaces, followed by any characters including special characters and spaces, limiting the total length to 25 characters.
                return this.optional(element) || /^[^\s][\s\S]{0,24}$/.test(value);
            },"Please enter a valid value. Spaces are allowed only within the value, and it should not exceed 25 characters.");

            $("#addhotel").validate({
                rules: {
                    collection: "required", // Add this line for maincategory validation
                    name: {
                        required: true,
                        customName: true
                    },
                    owner: {
                        required: true,
                        customName: true
                    },
                    email: {
                        required: true,
                        custval: true
                    }
                },
                messages: {

                    name: {
                        required: "Please fill in the required field.",
                    },
                    owner: {
                        required: "Please fill in the required field.",
                    },
                    email: {
                        required: "Please fill in the required field."
                    }
                },
                errorClass: "error", // Apply the 'error' class to error labels
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>
@endsection
