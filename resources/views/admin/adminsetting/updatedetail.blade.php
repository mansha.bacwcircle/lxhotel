<?php $page="updatedetail";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')
			@slot('title') General Settings @endslot
			@slot('title_1') Update general setting @endslot
		@endcomponent
        <style>
        /* Custom styles for error messages */
        label.error {
            color: red;
            font-size: 14px;
            display: block;
            margin-top: 5px;
        }
        .required-field::after {
            content: ' *';
            color: #ff0000; /* Adjust color as needed */
        }

            .float-input {
                display: none;
            }

            input[type="file"] {
                display: block;
            }

            .imageThumb {
                max-height: 100px;
                width: 100px;
                border: 2px solid;
                padding: 1px;
                cursor: pointer;
            }

            .pip {
                display: inline-block;
                margin: 10px 10px 0 0;
            }

            .remove {
                display: block;
                background: #444;
                border: 1px solid black;
                color: white;
                text-align: center;
                cursor: pointer;
            }

            .remove:hover {
                background: white;
                color: black;
            }

            .error {
                color: red;
                font-weight: bold;
            }
            .tab {
        display: none;
        }

        /* Style the tab buttons */
        .tab-button {
        cursor: pointer;
        padding: 10px 20px;
        border: none;
        color: #FF9F43;
        margin-right: 10px;
        position: relative;
        transition: background-color 0.3s, border-bottom 0.3s, margin-bottom 0.3s;
        }
        .tab-button.active {
            border-bottom: 2px solid #FF9F43;
        }
        .tab-button:hover {
        color: #FF9F43;
        border-bottom: 2px solid #FF9F43;
        margin-bottom: -2px; /* To compensate for the 2px border, creating space */
        }

    </style>
        <!-- /add -->
        <div class="card">
            <div class="card-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-12 mb-5">
                            <a class="tab-button active" data-tab="tab1" onclick="openTab('tab1')">Admin Profile</a>
                            <a class="tab-button" data-tab="tab2" onclick="openTab('tab2')">Site Settings</a>
                            <a class="tab-button" data-tab="tab3" onclick="openTab('tab3')">SEO Settings</a>
                            <a class="tab-button" data-tab="tab4" onclick="openTab('tab4')">SMTP Settings</a>
                            <a class="tab-button" data-tab="tab5" onclick="openTab('tab5')">Change Password</a>
                        </div>
                            <!-- Tab content -->
                        <div class="col-12 mt-3">
                            <div id="tab1" class="tab">
                                <form id="addsetting" method="POST" action="{{ route('admin.savesetting') }}"enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$setting->id}}">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Full Name<span class="required-field"></span></label>
                                                <input type="text" name="name" id="name" class="form-control"  value="{{$setting->name}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Phone Number<span class="required-field"></span></label>
                                                <input type="text" name="phone" id="phone" class="form-control" value="{{$setting->phone}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Email<span class="required-field"></span></label>
                                                <input type="email" name="email" id="email" class="form-control" value="{{$setting->email}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Address<span class="required-field"></span></label>
                                                <input type="text" name="address" id="address" class="form-control" value="{{$setting->address}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Twitter Profile</label>
                                                <input type="text" name="twitterlink" id="twitterlink" class="form-control" value="{{$setting->twitterlink}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Facebook Profile</label>
                                                <input type="text" name="facebooklink" id="facebooklink" class="form-control" value="{{$setting->facebooklink}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Instagram Profile</label>
                                                <input type="text" name="instagramlink" id="instagramlink" class="form-control"  value="{{$setting->instagramlink}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6 col-12">
                                            <div class="form-group">
                                                <label>Youtube Profile</label>
                                                <input type="text" name="youtubelink" id="youtubelink" class="form-control" value="{{$setting->youtubelink}}">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="form-label">Profile Image<span class="required-field"></span></label>
                                                <input type="file" id="profileInput" class="form-control" name="image" accept=".jpg, .jpeg, .png" onchange="loadFile('profileInput', 'output', 'profileError')"><br>
                                                <img id="output" src="../{{ $setting->image}}" style="height: 100px; width: 200px; object-fit: cover; " />
                                                <span id="profileError" style="color: red;"></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>About<span class="required-field"></span></label>
                                                <textarea class="form-control" id="About" name="about"  rows="4">{{$setting->about}}</textarea>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <button class="btn btn-submit me-2" onclick="SettingFormSubmit()" type="submit" id="submitbtn">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="tab2" class="tab">
                                <form id="addsitesetting" method="POST" action="{{ route('admin.savesitesetting') }}"enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$setting->id}}">
                                    <div class="row">
                                        <div class="col-6">
                                        <div class="form-group">
                                                <label class="form-label">Logo<span class="required-field"></span></label>
                                                <input type="file" id="logoInput" class="form-control" name="logo_image" accept=".jpg, .jpeg, .png" onchange="loadFile('logoInput', 'logo', 'logoError')"><br>
                                                <img id="logo" src="{{URL::to( $sitesetting->logo) }}" style="height: 100px; width: 200px; object-fit: cover;" />
                                                <span id="logoError" style="color: red;"></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="form-label">Fav-Icon<span class="required-field"></span></label>
                                                <input type="file" id="faviconInput" class="form-control" name="favicon_image" accept=".jpg, .jpeg, .png" onchange="loadFile('faviconInput', 'favicon', 'faviconError')"><br>
                                                <img id="favicon" src="../{{ $sitesetting->favicon}}" style="height: 100px; width: 200px; object-fit: cover; " />
                                                <span id="faviconError" style="color: red;"></span>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Name<span class="required-field"></span></label>
                                                <input type="text" name="name" id="name" value="{{$sitesetting->name}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <button class="btn btn-submit me-2" type="submit" onclick="SiteSettingFormSubmit()" id="submitbtn">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="tab3" class="tab">
                                <form action="{{ route('admin.saveseosetting') }}" method="POST" id="seosettings" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$seosettings->id}}">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label>Meta Title<span class="required-field"></span></label>
                                                <input type="text" name="metatitle" id="metatitle" value="{{$seosettings->metatitle}}">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label>Keyword<span class="required-field"></span></label>
                                                <input type="text" name="keyword" id="keyword" value="{{$seosettings->keyword}}">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                        <div class="form-group">
                                                <label>Canonical Tag<span class="required-field"></span></label>
                                                <input type="text" name="tag" id="tag " value="{{$seosettings->canonicaltag}}">
                                            </div>

                                        </div>
                                        <div class="col-4">
                                        <div class="form-group">
                                                <label>Google Analytics Code<span class="required-field"></span></label>
                                                <input type="text" name="analyticscode" id="analyticscode" value="{{$seosettings->analyticscode}}">
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="form-group">
                                                <label>Description<span class="required-field"></span></label>
                                                <textarea class="form-control" id="description" name="description"  rows="4">{{$seosettings->description}}</textarea>
                                            </div>
                                        </div>
                                        <div class=" col-12">
                                            <button class="btn btn-submit me-2" type="submit"  onclick="seosettingFormSubmit()" id="submitbtn">Save</button>
                                        </div>
                                    </div>
                                 </form>
                            </div>
                            <div id="tab4" class="tab">
                                <form action="{{ route('admin.savemailsetting') }}" method="POST" id="mailsettings" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$mailsettings->id}}">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Port<span class="required-field"></span></label>
                                                <input type="text" name="port" id="port" value="{{$mailsettings->port}}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Name<span class="required-field"></span></label>
                                                <input type="text" name="name" id="name" value="{{$mailsettings->name}}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Key<span class="required-field"></span></label>
                                                <input type="text" name="key" id="key" value="{{$mailsettings->key}}">
                                            </div>
                                        </div>
                                        <div class=" col-12">
                                            <button class="btn btn-submit me-2" onclick="mailFormSubmit()" type="submit" id="submitbtn">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="tab5" class="tab">
                                <form action="{{ route('admin.postchangepassword') }}" method="POST" id="AdminPassword">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="hidden" name="id" value="{{$setting->id}}">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>New Password<span class="required-field"></span></label>
                                                    <input type="password" name="password" id="password">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Re-enter New Password<span class="required-field"></span></label>
                                                    <input type="password" name="cpassword" id="cpassword">
                                                </div>
                                            </div>
                                            <div class=" col-12">
                                                <button class="btn btn-submit me-2" type="submit" id="submitbtn">Save</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <!-- /add -->
    </div>
</div>
<script>
  document.getElementById('tab1').style.display = 'block';
    // Function to open a specific tab
    function openTab(tabId) {
    // Hide all tabs
    var tabs = document.getElementsByClassName('tab');
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].style.display = 'none';
    }

    // Show the selected tab
    document.getElementById(tabId).style.display = 'block';

    // Remove the 'active' class from all tab buttons
    var tabButtons = document.getElementsByClassName('tab-button');
    for (var i = 0; i < tabButtons.length; i++) {
        tabButtons[i].classList.remove('active');
    }

    // Add the 'active' class to the clicked tab button
    var activeButton = document.querySelector('.tab-button[data-tab="' + tabId + '"]');
    activeButton.classList.add('active');
    }

function loadFile(inputId, imageId, errorId) {
        var submitBtn = document.getElementById('submitbtn');
        var input = document.getElementById(inputId);
        var errorimg = document.getElementById(errorId);

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            if (input.files[0].type.startsWith('image/')) {
                reader.onload = function () {
                    var output = document.getElementById(imageId);
                    output.src = reader.result;
                    output.style.display = 'block';
                    submitBtn.disabled = false;
                    errorimg.textContent = "";
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                submitBtn.disabled = true;
                errorimg.textContent = "Please select a valid image file.";
                input.value = '';
               // document.getElementById(imageId).src = '';
               // document.getElementById(imageId).style.display = 'none';
            }
        }
    }

    ClassicEditor
    .create( document.querySelector( '#additionalinfo' ) )
    .catch( error => {
        console.error( error );
    } );
    function FormSubmit() {
        $("#AdminPassword").validate({
            rules: {
                password: {
                    required: true,
                },
                Cpassword: {
                    required: true,
                    equalTo: "#password",
                },
            },
            messages: {
                password: "Password is required",
                Cpassword: {
                    required: "ConfirmPassword is required",
                    equalTo: "Mismatch Password",
                },
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    function seosettingFormSubmit() {
        $("#seosettings").validate({
            rules: {
                metatitle: {
                    required: true,
                },
                keyword: {
                    required: true,
                },
                description: {
                    required: true,
                },
                tag: {
                    required: true,

                },
                analyticscode: {
                    required: true,

                }

            },
            messages: {
                metatitle: "Meta Title is required",
                keyword: "Keyword is required",
                description: "Description is required",
                tag: " Canonical Tag is required",
                analyticscode: "Analytics Code is required",


            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    function mailFormSubmit() {
        $("#mailsettings").validate({
            rules: {
                port: {
                    required: true,
                },
                name: {
                    required: true,

                },
                key: {
                    required: true,

                }
            },
            messages: {
                port: "Port is required",
                name: "Name is required",
                key: "Key is required",

            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }

    function SettingFormSubmit() {
        $.validator.addMethod("customName", function(value, element) {
        return this.optional(element) || /^[^\s][\s\S]{0,24}$/.test(value);;
      }, "Please enter a valid name. Spaces are allowed only within the name, and it should not exceed 25 characters.");
      $.validator.addMethod("custval", function(value, element) {
          // This regex allows any character to start except for spaces, followed by any characters including special characters and spaces, limiting the total length to 25 characters.
          return this.optional(element) || /^[^\s][\s\S]{0,24}$/.test(value);
      }, "Please enter a valid value. Spaces are allowed only within the value, and it should not exceed 25 characters.");
      $.validator.addMethod("standardPhoneNumber", function(value, element) {
            return this.optional(element) || /^\d{3}\s\d{3}\s\d{4}$/.test(value);
        }, "Please enter a valid phone number in the format 'xxx xxx xxxx'.");
        $.validator.addMethod("customDesc", function(value, element) {
          // This regex allows any character to start except for spaces, followed by any characters including special characters and spaces, limiting the total length to 255 characters.
          return this.optional(element) || /^[^\s][\s\S]{0,254}$/.test(value);
      }, "Please enter a valid details. Spaces are allowed only within the details, and it should not exceed 255 characters.");
        $("#addsetting").validate({
            rules: {
                name: {
                    required: true,
                    customName:true
                },
                email: {
                    required: true,
                    custval:true
                },
                phone: {
                    required: true,
                    standardPhoneNumber:true
                },
                address: {
                    required: true,
                    customDesc:true
                },
                about: {
                    required: true,
                    customDesc:true
                }
            },
            messages: {
                name: {required:"Name is required"},
                email: {required:"Email is required"},
                phone: {required:"Phone is required"},
                address: {required:"Address is required"},
                about: {required:"About is required"},


            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    }


        </script>
@endsection
