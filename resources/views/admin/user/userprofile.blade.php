<?php $page="viewuser";?>
@extends('layout.mainlayout')
@section('content')		
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')                
			@slot('title') View User @endslot
			@slot('title_1') 
      <a href="{{url('userlist')}}"> User </a> <i class='fas fa-angle-right'></i> View User @endslot
		@endcomponent
        <style>
    .success-modal {
        color: #155724;
        background-color: #fff;
        border-color: #6c757d;

    }

    .success-icon {
        color: #155724;
        font-size: 2em;
    }
   
    </style>

<div class="card">
                <div class="card-body">
                    <div class="modal fade" id="flashMessageModal" tabindex="-1" role="dialog" aria-labelledby="flashMessageModal"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content success-modal">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="flashMessageModal">
                                        @if (Session::has('messageType') && Session::has('message'))
                                            @if (Session::get('messageType') === 'error')
                                                <i class="fas fa-times-circle failure-icon"></i> Failure
                                            @elseif(Session::get('messageType') === 'success')
                                                <i class="fas fa-check-circle success-icon"></i> Success
                                            @endif
                                        @endif
                                    </h5>
                                </div>
                                <div class="modal-body">
                                    @if (Session::has('messageType') && Session::has('message'))
                                        <h5>{{ Session::get('message') }}</h5>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    <section class="content-main">
                        <div class="content-header">
                           {{--<a href="{{ url('userlist') }}"><i class="material-icons md-arrow_back"></i> Go back </a>--}}
                        </div>
                        <div class="card mb-4">
                            <div class="card-header" style="height:150px; background-color:#FF9F43">
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <input type="hidden" name="userprofileid" value="{{$user->id}}">
                                    <div class="col-xl col-lg flex-grow-0" style="flex-basis:230px">
                                        <div class="img-thumbnail  position-relative text-center" style="height:190px; width:200px; margin-top:-120px">
                                        @if($user->image)
                                            <img src="{{ asset($user->image) }}" class="center-xy img-fluid" style="height:200px; width:200px" alt="Logo Brand">
                                        @else
                                            <img src="{{ asset('assets/img/profiles/avatar-01.jpg') }}" class="center-xy img-fluid" style="height:200px; width:200px" alt="Logo Brand">
                                        @endif

                                        </div>
                                    </div>
                                    <div class="col-xl col-lg mt-5">
                                        <h3>{{ $user->name}}</h3>
                                        <input type="hidden" name="userNumber" value="{{ $user->number }}">
                                        <button type="button" onclick="redirectToWhatsApp()" class="btn" style="background-color:#FF9F43">Leave message<i class="material-icons md-launch"></i></button>
                                    </div> 
                                    <div class="col-xl-4 text-md-end">
                                            
                                    </div>
                                </div> 
                                <hr class="my-4">
                                <div class="row g-4">
                                    <div class="col-md-12 col-lg-4 col-xl-2">
                                        <article class="box">
                                            <p class="mb-0 text-muted">Total Orders:</p>
                                            <h5 class="text-success">{{ $totalOrders }}</h5>
                                            <p class="mb-0 text-muted">Number of Reviews:</p>
                                            <h5 class="text-success mb-0">{{ $totalReviews }}</h5>
                                        </article>
                                    </div>
                                    <div class="col-sm-6 col-lg-4 col-xl-3">
                                       <article class="box">
                                            <p class="mb-0 text-muted">Email:</p>
                                            <span class="text-success">{{ $userEmail }}</span>
                                           
                                        </article>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </section>
                </div>
            </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // Show the modal if flash messages are present
        @if (Session::has('messageType') && Session::has('message'))
            $('#flashMessageModal').modal('show');
        @endif
    });
</script>
@endsection
	  