<?php $page="userlist";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        <div class="page-header">
            @component('components.pageheader')
                @slot('title') User List @endslot
                @slot('title_1') View/Search User @endslot

            @endcomponent

        </div>
        @include('admin.user.userlist')
    </div>
</div>
@endsection
