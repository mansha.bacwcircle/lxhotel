 <!-- /product list -->
 <div class="card">
            <div class="card-body">
                <div class="table-top">
                    <div class="search-set">
                        <!-- <div class="search-path">
                            <a class="btn btn-filter" id="filter_search">
                                <img src="{{ URL::asset('/assets/img/icons/filter.svg')}}" alt="img">
                                <span><img src="{{ URL::asset('/assets/img/icons/closes.svg')}}" alt="img"></span>
                            </a>
                        </div> -->
                        <div class="search-input">
                            <a class="btn btn-searchset"><img src="{{ URL::asset('/assets/img/icons/search-white.svg')}}" alt="img"></a>
                        </div>
                    </div>
                     <!-- <div class="wordset">
                        <ul>
                            <li>
                                <a data-bs-toggle="tooltip" data-bs-placement="top" title="pdf"><img src="{{ URL::asset('/assets/img/icons/pdf.svg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a data-bs-toggle="tooltip" data-bs-placement="top" title="excel"><img src="{{ URL::asset('/assets/img/icons/excel.svg')}}" alt="img"></a>
                            </li>
                            <li>
                                <a data-bs-toggle="tooltip" data-bs-placement="top" title="print"><img src="{{ URL::asset('/assets/img/icons/printer.svg')}}" alt="img"></a>
                            </li>
                        </ul>
                    </div> -->
                </div>

                <div class="table-responsive">
                    @if (count($users) > 0)

                        <table class="table" id="userlist">
                        <thead>
                            <tr>
                                <th >#</th>
                                <th >Name</th>
                                <th >Email</th>
                                <th >Created_at</th>
                                <th >Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)

                                    <tr>
                                        <td  style="white-space: normal;">{{ $loop->iteration }}</td>
                                        <td  style="white-space: normal;" width="40%">
                                            <a href="{{ url('/user-profile') }}/{{ $user->id }}" class="itemside">
                                                <div class="left">
                                                    @if($user->image)
                                                        <img src="{{ asset($user->image) }}" class="img-sm img-avatar" style="width: 100px; height: 100px; " alt="Userpic" />
                                                    @else
                                                        <img src="{{ asset('assets/img/profiles/avatar-01.jpg') }}" class="img-sm img-avatar" style="width: 100px; height: 100px" alt="Userpic" />
                                                    @endif
                                                </div>
                                                <div class="info pl-3">
                                                    <h6 class="mb-0 title">{{ $user->name }}</h6>
                                                    <small class="text-muted">Total Orders: {{ $user->order_count }}</small>
                                                </div>
                                            </a>
                                        </td>
                                        <td   style="white-space: normal;">{{ $user->email }}</td>
                                        <td   style="white-space: normal;">{{ date('F j, Y', strtotime($user->created_at)) }}</td>
                                        <td>                                                          
                                            <a 
                                                href="{{ url('/user-profile') }}/{{ $user->id }}"><img src="{{ URL::asset('/assets/img/icons/eye.svg') }}" alt="img"></a>
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                        </table>
                    @else
                        <div>No Record Found</div>
                    @endif
                </div>
            </div>
        </div>
        <!-- /product list -->

<script>
    $(document).ready(function() {
        // Show the modal if flash messages are present
        @if (Session::has('messageType') && Session::has('message'))
            $('#flashMessageModal').modal('show');
        @endif
        var dataTable = $('#userlist').DataTable({  
        paging: true,
        searching: true,
        ordering: false
       
   });

    });

</script>
