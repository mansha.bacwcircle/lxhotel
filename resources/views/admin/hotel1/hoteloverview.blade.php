<?php $page="maincategorylist";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        
<div class="container mt-5">
        <h4 class="p-3 mb-3 bg-primary text-white">Overview</h4>
        @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

        <form method="POST" action="add_hotel_overview">
        @csrf
            <div class="form-group">
                <label for="hotelName">Hotel Name</label>
                <input type="text" class="form-control" id="hotelName" name="hotelName" placeholder="Enter hotel name" >
            </div>
            <div class="form-group">
                <label for="hotelDescription">Hotel Description</label>
                <textarea class="form-control" id="hotelDescription" name="hotelDescription" rows="3" placeholder="Enter hotel description"></textarea>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="smokeFreeHotel">100% Smoke Free Hotel (including all public areas)</label>
                        <select class="form-control" id="smokeFreeHotel" name="smokeFreeHotel" >
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nonSmokingRooms">Non-Smoking Rooms Available</label>
                        <select class="form-control" id="nonSmokingRooms" name="nonSmokingRooms" >
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="allocatedSuits">Total Number Of Allocated Non-Smoking Rooms Available</label>
                <input type="number" class="form-control" id="allocatedSuits" name="allocatedSuits" placeholder="Enter number of suites" >
            </div>
            <div class="form-group">
                <label for="allocatedAccessible">Total Number of Allocated Accessible Rooms</label>
                <input type="number" class="form-control" id="allocatedAccessible" name="allocatedAccessible" placeholder="Enter number of accessible rooms" >
            </div>
            <div class="form-group">
                <label for="corridors">What types of Corridors Does Your Hotel Have (select one ?)</label> 
                <select class="form-control" id="corridors" name="corridors" >
                    <option value="">Select</option>
                    <option value="interior">Interior Corridors</option>
                    <option value="exterior">Exterior Corridors</option>
                    <option value="interiorExterior">Interior/Exterior Corridors</option>
                </select>
            </div>
            <hr/>
            <h4 class="p-3 mb-3 bg-primary text-white">Hotel Location And Type</h4>
            <h5>Driving Direction</h5>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="drivingDirection1">Driving Direction 1</label>
                        <input type="text" class="form-control" id="drivingDirection1" name="drivingDirection1" placeholder="Driving Direction 1" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="drivingDirection2">Driving Direction 2</label>
                        <input type="text" class="form-control" id="drivingDirection2" name="drivingDirection2" placeholder="Driving Direction 2" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="drivingDirection3">Driving Direction 3</label>
                        <input type="text" class="form-control" id="drivingDirection3" name="drivingDirection3" placeholder="Driving Direction 3" >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="drivingDirection4">Driving Direction 4</label>
                        <input type="text" class="form-control" id="drivingDirection4" name="drivingDirection4" placeholder="Driving Direction 4" >
                    </div>
                </div>
            </div>
            <hr/>
            <h4 class="p-3 mb-3 bg-primary text-white">Service Level And Hotel Service</h4>
            <div class="form-group">
                <label for="serviceLevel">Full Service or Limited Service</label>
                <select class="form-control" id="serviceLevel" name="serviceLevel" >
                    <option value="">Select</option>
                    <option value="Limited Service Offer">Limited Service Hotel</option>
                    <option value="Full Service Offer">Full Service Hotel</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
    </div>
</div>
@endsection