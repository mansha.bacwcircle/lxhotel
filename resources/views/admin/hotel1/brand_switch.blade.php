<?php $page="maincategorylist";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        
<div class="container mt-5">
        <h4 class="p-3 mb-3 bg-primary text-white">Brand Switch</h4>
        @if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

        <form method="POST" action='add_brand_switch'>
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label for="switchingFromAnotherBrand">1. Is your hotel switching from another brand?</label>
                <select class="form-control" id="switchingFromAnotherBrand" name="switchingFromAnotherBrand" >
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>           
                </div>
                </div>
           
          
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="hotelNameListedInGDS">2. Current hotel name listed in the GDS</label>
                        <select class="form-control" id="hotelNameListedInGDS" name="hotelNameListedInGDS" >
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nameOfChain">3. Name of chain </label>
                        <input type="text" class="form-control" id="nameOfChain" name="nameOfChain" placeholder="If not please write N/A" >
                       </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="twoletterchain">4. Two letter chain code </label>
                        <input type="text" class="form-control" id="twoletterchain" name="twoletterchain" placeholder="If not please write N/A" >
                       
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="currentGDSCode">5. Current GDS Codes  </label>
                        <select class="form-control" id="currentGDSCode" name="currentGDSCode" >
                            <option value="">Select</option>
                            <option value="no">N/A</option>
                            <option value="Amadeus">Amadeus</option>
                            <option value="WorldSpan">WorldSpan</option>
                            <option value="Sabre">Sabre</option>
                            <option value="Room Master/Apollo">Room Master/Apollo</option>
                           
                        </select>
                    </div>
                </div>
            </div>
           
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
    </div>
</div>
@endsection