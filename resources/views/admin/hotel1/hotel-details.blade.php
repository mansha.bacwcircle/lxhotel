<?php $page="hotel-details";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')
			@slot('title') Hotel Details @endslot
			@slot('title_1') View / <a href="{{url('hotellist')}}">Hotel Details</a>
            @endslot
		@endcomponent
        <!-- /add -->
        <div class="row">
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="productdetails">
                            <ul class="product-bar">
                                <li>
                                    <h4>Hotel Name</h4>
                                    <h6>{{$hoteldetails[0]->name}}</h6>
                                </li>
                                <li>
                                    <h4>Hostname</h4>
                                    <h6>{{$hoteldetails[0]->hostname}}</h6>
                                </li>
                                <li>
                                    <h4>Email</h4>
                                    <h6>{{$hoteldetails[0]->email}}</h6>
                                </li>
                                <li>
                                    <h4>Contact</h4>
                                    <h6>{{$hoteldetails[0]->contact}}</h6>
                                </li>
                                <li>
                                    <h4>Gtagid</h4>
                                    <h6>{{$hoteldetails[0]->gtagid}}</h6>
                                </li>
                                <li>
                                    <h4>Google Analytics Code</h4>
                                    <h6>{{$hoteldetails[0]->googleanalyticscode}}</h6>
                                </li>
                                <li>
                                    <h4>Address</h4>
                                    <h6>{{$hoteldetails[0]->address}}</h6>
                                </li>
                                <li>
                                    <h4>Status</h4>
                                    <h6>{{ ucwords($hoteldetails[0]->status) }}</h6>

                                </li>
                               
                              
                               
                            </ul>
                   
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="slider-product-details">
                            <div class="owl-carousel owl-theme product-slide">
                           
                                <div class="slider-product">
                                @if ($hoteldetails[0]->image)
                                <img src="../{{ $hoteldetails[0]->image }}" alt="Hotel Image" onerror="this.onerror=null; this.src='assets/img/profiles/avatar-01.jpg';">
                                @else
                                    <img src="{{ URL::asset('/assets/img/icons/closes.svg')}}" alt="img">
                                    @endif
                                    </div>
                            

                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>

        <!-- /add -->
    </div>
</div>
@endsection
