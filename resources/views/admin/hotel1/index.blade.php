
<?php $page="maincategorylist";?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        <div class="page-header">
            @component('components.pageheader')
                @slot('title')Hotel Overview @endslot
                <!-- @slot('title_1') Manage Your Hotels @endslot -->

            @endcomponent

            <!-- <div class="page-btn">
            <a href="{{url('addhotel')}}" class="btn btn-added"><img src="assets/img/icons/plus.svg" class="me-2" alt="img"> Add Hotel</a>
            </div> -->
        </div>
        @include('admin.hotel.hoteloverview')
    </div>
</div>
@endsection

