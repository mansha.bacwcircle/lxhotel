<div class="card">
            <div class="card-body">
            <div class="modal fade" id="flashMessageModal" tabindex="-1" role="dialog" aria-labelledby="flashMessageModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content success-modal">

                    <div class="modal-body" style="text-align:center;">
                           @if(Session::has('messageType') && Session::has('message'))
                                <h5 class="flashMessageModal" id="flashMessageModal">
                                    @if(Session::has('messageType') && Session::has('message'))
                                        @if(Session::get('messageType') === 'fail')
                                            <i class="fas fa-times-circle failure-icon text-red"></i>
                                        @elseif(Session::get('messageType') === 'success')
                                            <i class="fas fa-check-circle success-icon success-text"></i>
                                        @endif
                                    @endif
                                </h5>
                                <h5 class="flashMessageModal {{ Session::get('messageType') === 'fail' ? 'text-red' : 'success-text' }}">
                                    {{ Session::get('message') }}
                                </h5>
                            @endif
                    </div>

                </div>
            </div>
        </div>
               
        @if (count($hotelList) > 0)
            <div class="table-responsive">
                <table class="table" id="hotelList">
                    <thead>
                        <tr>
                            <!-- <th>#</th> -->
                            <th>Hotel Name</th>
                            <th>Logo</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($hotelList as $hotel)
                            <tr>
                          {{--  <td>CT -{{ str_pad($hotel->id, 6, '0', STR_PAD_LEFT) }}</td>--}}
                                <td>{{ $hotel->name }}</td>
                                <td>
                                      <a class="product-img">
                                            @if ($hotel->image)
                                                <img src="../{{ $hotel->image }}" onerror="this.onerror=null; this.src='assets/img/profiles/avatar-01.jpg';" alt="product">
                                            @else
                                                <img src="{{ URL::asset('/assets/img/icons/closes.svg') }}"
                                                    class="contain_imager rounded"
                                                    style="width:50px; height:50px;object-fit: cover;"
                                                    alt="hotel-image" />
                                            @endif
                                        </a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-sm font-sm btn-light rounded text-center"
                                        style="color: {{ $hotel->status == 'active' ? 'green' : 'red' }}"
                                        onclick="statuschange('{{ $hotel->id }}')">
                                        {{ $hotel->status == 'active' ? 'Active' : 'Inactive' }}
                                    </a>
                                </td>
                                <td>
                                <a class="me-3" href="{{ url('hotel-details') }}/{{ $hotel->id }}">
                                         <img src="{{ URL::asset('/assets/img/icons/eye.svg') }}" alt="img">
                                     </a>
                                    <a class="me-3" href="{{ url('edithotel') }}/{{ $hotel->id }}">
                                        <img src="{{ URL::asset('/assets/img/icons/edit.svg') }}" alt="edit">
                                    </a>
                                    <a class="me-3" href="javascript:void(0);" onclick="showDeleteConfirmation('{{ $hotel->id }}')">
                                        <img src="{{ URL::asset('/assets/img/icons/delete.svg') }}" alt="delete">
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div>No Record Found</div>
        @endif
            </div>
        </div>
        <script>

    $(document).ready(function() {
        @if (Session::has('messageType') && Session::has('message'))
            $('#flashMessageModal').modal('show');
            setTimeout(function() {
                $('#flashMessageModal').modal('hide');
            }, 2000); // 1000 milliseconds = 1 second
        @endif

        var dataTable = $('#maincategorytable').DataTable({paging: true,searching: true,ordering: false});
    });

    function statuschange(id) {
        Swal.fire({
            title: "Are you sure to change the status?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then((result) => {
            if (result.isConfirmed) {
                //alert("delete");
                location.href = 'hotelchangestatus?id=' + id;
            }
        });
    }

    // Function to show the confirmation dialog
    function showDeleteConfirmation(id) {
        Swal.fire({
            title: "Are you sure you want to delete ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then((result) => {
            if (result.isConfirmed) {
            //  alert("delete");
                location.href = 'deletehotel?id=' + id;
            }
        });
    }
</script>