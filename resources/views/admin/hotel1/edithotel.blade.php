<?php $page = 'edithotel'; ?>
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        @component('components.pageheader')
            @slot('title') Edit Hotels @endslot
                @slot('title_1')
                <a href="{{url('hotellist')}}"> Hotel </a> <i class='fas fa-angle-right'></i> Edit Hotel
                @endslot
		@endcomponent
            <style>
                /* Custom styles for error messages */
                label.error {
                    color: red;
                    font-size: 14px;
                    display: block;
                    margin-top: 5px;
                }

                .required-field::after {
                    content: ' *';
                    color: #ff0000;
                    /* Adjust color as needed */
                }
            </style>
            <div class="card">
                <div class="card-body">
                    @if (Session::has('messageType') && Session::has('message'))
                        <h5 style="font-size: 25px;color: red;">{{ Session::get('message') }}</h5>
                    @endif
                    <form id="edithotel" enctype="multipart/form-data" method="POST" action="postupdatehotel">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="hotelid" id="hotelid" value="{{ $hotelInfo->id }}">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Name<span class="required-field"></span></label>
                                    <input type="text" id="name"  required name="name" placeholder="Hotel Name" class="form-control"  value="{{ $hotelInfo->name }}"  onkeyup="performOperationOnSecondInput(event.target.value)">
                                    <span id="errorName" style="color:red"> </span>
                                </div>
                            </div>
                            <div id="resultMessages" style="font-size:18px;color:red;font-weight: 600;"></div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Host name <span class="required-field"></span></label>
                                    <input type="text" id="hostname" name="hostname" placeholder="Ex:home2vicksburg.com"  value="{{ $hotelInfo->hostname }}" class="form-control">
                                    <span id="errorslug" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Email <span class="required-field"></span></label>
                                    <input type="email" id="email" name="email" placeholder="Hotel Email"  value="{{ $hotelInfo->email }}" class="form-control" >
                            </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Contact #</label>
                                    <input type="text" id="contact" name="contact" placeholder="Hotel contact"  value="{{ $hotelInfo->contact }}" class="form-control" >
                            </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>G-Tag ID <span class="required-field"></span></label>
                                    <input type="text" id="tagid" name="tagid" placeholder="G-DCT84PE0XN" class="form-control"  value="{{ $hotelInfo->gtagid }}">
                            </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Google Analytics Code <span class="required-field"></span></label>
                                    <textarea name="analyticscode" id="analyticscode" placeholder="Google Analytics Code"  class="form-control" >{{ $hotelInfo->googleanalyticscode }}</textarea>
                           </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Address <span class="required-field"></span></label>
                                    <textarea name="address" id="address" class="form-control" placeholder="Hotel Address">{{$hotelInfo->address}}</textarea>
                           </div>
                            </div>
                            <div class="col-lg-12 col-sm-6 col-12">
                        <div class="form-group">
                                <label class="form-label">Image<span class="required-field"></span></label>
                                <input class="form-control" type="file" name="image" accept=".jpg, .jpeg, .png" onchange="loadFile(event)"><br>
                                <span id="errorimg" style="color:red;"></span>
                                                             
                            </div>
                        </div>
                    </div>
                            <div class="col-12">
                            <div class="productviews">
                          <div class="productviewsimg">
                            @if($hotelInfo->image)
                              <img id='output'  src="../{{$hotelInfo->image}}" onerror="this.onerror=null; this.src='../assets/img/profiles/avatar-01.jpg';" style="height: 250px;width: 380px; object-fit: cover;" name="image"/>
                            @else
                              <img src="{{URL::asset('assets/imgs/img-01.jpg')}}" style="height: 250px;width: 380px; object-fit: cover;" name="image"/>
                            @endif
                          </div>
                        </div>
                         </div>
                            <div class="col-lg-12">
                                <input type="submit" class="btn btn-submit me-2" id="submitbtn" value="Update">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>

jQuery(document).ready(function($) {
    $.validator.addMethod("customName", function(value, element) {
    return this.optional(element) || /^[^\s][\s\S0-9]{0,49}$/.test(value);
}, "Please enter a valid details. Spaces are allowed only within the details, and it should not exceed 50 characters.");

$.validator.addMethod("tagid", function(value, element) {
    return this.optional(element) || /^[^\s][\s\S0-9]{0,20}$/.test(value);
}, "Please enter a valid details. Spaces are allowed only within the details, and it should not exceed 20 characters.");

    $.validator.addMethod("customDesc", function(value, element) {
        return this.optional(element) || /^[^\s][\s\S]{0,254}$/.test(value);
    }, "Please enter a valid details. Spaces are allowed only within the details, and it should not exceed 255 characters.");

    $.validator.addMethod("custval", function(value, element) {
        // This regex allows any character to start except for spaces, followed by any characters including special characters and spaces, limiting the total length to 25 characters.
        return this.optional(element) || /^[^\s][\s\S]{0,24}$/.test(value);
    }, "Please enter a valid value. Spaces are allowed only within the value, and it should not exceed 25 characters.");

    $.validator.addMethod("standardPhoneNumber", function(value, element) {
        return this.optional(element) || /^\d{1,3}\s\d{3}\s\d{3}\s\d{4}$/.test(value);
    }, "Please enter a valid phone number in the format 'xx,xxx xxx xxxx'.");

    $.validator.addMethod("imageSize", function(value, element, param) {
        var maxSizeInBytes = param * 1024;
        var selectedImage = $('#image')[0].files[0];
        if (selectedImage) {
            return selectedImage.size <= maxSizeInBytes;
        }
        return true;
    }, $.validator.format("Image size must be less than {0} KB"));

    $("#edithotel").validate({
        rules: {
            name: {
                required: true,
                customName: true
            },
            hostname: {
                required: true,
                custval: true
            },
            email: {
                required: true,
                custval: true
            },
            contact: {
                standardPhoneNumber: true
            },
            tagid: {
                required: true,
                tagid: true
            },
            analyticscode: {
                required: true
            },
            address: {
                required: true,
                customDesc: true
            }
        },
        messages: {
            name: {
                required: "Please fil in the required field"
            },
            hostname: {
                required: "Please fil in the required field"
            },
            email: {
                required: "Please fil in the required field"
            },
            contact: {
                required: "Please fil in the required field"
            },
            tagid: {
                required: "Please fil in the required field"
            },
            analyticscode: {
                required: "Please fil in the required field"
            },
            address: {
                required: "Please fil in the required field",
            }
        },
        errorClass: "error", // Apply the 'error' class to error labels
        submitHandler: function(form) {
            form.submit();
        },errorPlacement: function(error, element) {
                // Customize the error placement
                if (element.is("select")) {
                  error.insertAfter(element.next());

                // console.log(element);
                } else {
                  error.insertAfter(element);
                }
              }
    });
});


    var loadFile = function(event) {
            var input = event.target;
            var errorimg = document.getElementById('errorimg');
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                if (input.files[0].type.startsWith('image/')) {
                    reader.onload = function(){
                        var output = document.getElementById('output');
                        var img = new Image();
                        img.src = reader.result;
                        img.onload = function() {
                            if (img.height > 300) {
                            $("#output").css("display", "block");
                            output.src = reader.result;
                            errorimg.textContent = ""; // Clear error message
                        } else {
                            errorimg.textContent = "Image width should be flexible (auto) and height should be at least 300px.";
                            input.value = ''; // Clear the file input
                        }
                        };
                    };
                    reader.readAsDataURL(input.files[0]);
                } else {
                    errorimg.textContent = "Please select a valid image file.";
                    input.value = ''; // Clear the file input
                }
            }
        };

        function performOperationOnSecondInput(processedValue) {
            var name =  processedValue;
            var hotelid = document.getElementById('hotelid').value;
           if (name !== '') {
                $.ajax({
                    type: 'GET',
                    url: '/checkname', // Laravel route pointing to the checkNameController
                    data: {
                        name: name,
                        id:hotelid
                    },
                    dataType: 'json',
                    success: function(response) {
                        displayMessage(response.message);
                    }
                });
            } else {
                displayMessage('');
            }
        }

        function displayMessage(message) {
           
            if (message == "Hotel name is already in the database.") {
                $('#submitbtn').prop('disabled', true); // Disable submit button
                $('#resultMessages').text(message);
            } else  {
                $('#submitbtn').prop('disabled', false); // Enable submit button
                $('#resultMessages').text(message);
            }
        }

        

    </script>
    @endsection
