<?php $page="maincategorylist";?> 
@extends('layout.mainlayout')
@section('content')
<div class="page-wrapper">
    <div class="content">
        <div class="container mt-5">
            <form>
                <h5 class="p-3 mb-3 bg-primary text-white">Cable Or Satellite, DVD</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="smokeFreecableSatelliteTvOnPremisesotel">Cable or satellite tV on Premises</label>
                            <select class="form-control" id="cableSatelliteTvOnPremises" name="cableSatelliteTvOnPremises" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hotelDescription">Additional Cable or satellite tv on Premises information</label>
                            <textarea class="form-control" id="additonalCableSatelliteTvOnPremises" name="additonalCableSatelliteTvOnPremises" rows="3" placeholder="Additional Cable or satellite tv on Premises information"></textarea>
                        </div>
                    </div>
                </div>
                <h5 class="p-3 mb-3 bg-primary text-white">Child Care Activities</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="babySittingChildCare">Baby sitting / Child care available</label>
                            <select class="form-control" id="babySittingChildCare" name="babySittingChildCare" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="babySittingChildCare">Baby sitting / Child care available for 24 hrs</label>
                            <select class="form-control" id="babySittingChildCareAvailable24hrs" name="babySittingChildCareAvailable24hrs" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <h5>If Baby sitting not available for 24 hours, What hours is it available ?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="babySittingChildCareFromTime"> From Time</label>
                                    <input type="time" class="form-control" id="babySittingChildCareFromTime" name="babySittingChildCareFromTime" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="babySittingChildCareToTime">To Time</label>
                                    <input type="time" class="form-control" id="babySittingChildCareToTime" name="babySittingChildCareToTime" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="childrenActivities">Children activities</label>
                            <select class="form-control" id="childrenActivities" name="childrenActivities" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="childrenActivitiesavailable24hrs">Children activities available for 24 hrs</label>
                            <select class="form-control" id="childrenActivitiesavailable24hrs" name="childrenActivitiesavailable24hrs" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5>If Children activities not available for 24 hours, What hours is it available ?</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesFromTime"> From time</label>
                                    <input type="time" class="form-control" id="childrenActivitiesFromTime" name="childrenActivitiesFromTime" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesToTime"> To time</label>
                                    <input type="time" class="form-control" id="childrenActivitiesToTime" name="childrenActivitiesToTime" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="childrenActivities">Are baby sitting /child care or children's activities complementary, or is there any fees </label>
                            <select class="form-control" id="childrenActivities" name="childrenActivities" required>
                                <option value="">Select</option>
                                <option value="Complementary">Complementary</option>
                                <option value="Available for fee">Available for fee</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="childrenActivitiesavailable24hrs">If baby sitting /child care or children's activities are not complementary , What is the fee? </label>
                            <input type="text" class="form-control" id="babySittingActivityFee" name="babySittingActivityFee" placeholder="Enter $ Fee per hour/ per day" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="hotelDescription">Additional baby sitting /child care or children's activities information</label>
                            <textarea class="form-control" id="additionalBabySittingChildCareInfo" name="additionalBabySittingChildCareInfo" rows="3" placeholder="Additional baby sitting /child care or children's activities information"></textarea>
                        </div>
                    </div>
                </div>
                <h5 class="p-3 mb-3 bg-primary text-white">Playground</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="childrenActivities">Playground</label>
                            <select class="form-control" id="playground" name="playground" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5>Playground what hour is it open?</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesFromTime">Play ground from time</label>
                                    <input type="time" class="form-control" id="playgroundFromTime" name="playgroundFromTime" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesToTime"> Play ground to time</label>
                                    <input type="time" class="form-control" id="playgroundToTime" name="playgroundToTime" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="additionalPlaygroundInfo">Additional playground information</label>
                            <textarea class="form-control" id="additionalPlaygroundInfo" name="additionalPlaygroundInfo" rows="3" placeholder="Additional playground information"></textarea>
                        </div>
                    </div>
                </div>
                <h5 class="p-3 mb-3 bg-primary text-white">Health Club / exercise Facilities</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="healthClubHeltFacility">Health club / exercise facilities</label>
                            <select class="form-control" id="healthClubHeltFacility" name="healthClubHeltFacility" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="healthclubopen24hrs">Is it open 24 hours?</label>
                            <select class="form-control" id="healthclubopen24hrs" name="healthclubopen24hrs" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h5>If Health club / exercise facilities is not open for 24 hours, What hours is it open?</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="healthClubFromTime">Health club / exercise facilities open from </label>
                                    <input type="time" class="form-control" id="healthClubFromTime" name="healthClubFromTime" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="healthClubToTime"> Health club / exercise facilities close at</label>
                                    <input type="time" class="form-control" id="healthClubToTime" name="healthClubToTime" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="healthClubExcersiseComplementaryOrFee">Is Health club / exercise facilities complementary, or is there any fees </label>
                            <select class="form-control" id="healthClubExcersiseComplementaryOrFee" name="healthClubExcersiseComplementaryOrFee" required>
                                <option value="">Select</option>
                                <option value="Complementary">Complementary</option>
                                <option value="Available for fee">Available for fee</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="healthClubExersiseFee">If Health club / exercise facilities are not complementary , What is the fee? </label>
                            <input type="text" class="form-control" id="healthClubExersiseFee" name="healthClubExersiseFee" placeholder="Enter $ Fee per hour/ per day" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="additionalHealthCareInfo">Additional Health club / exercise facilities information</label>
                            <textarea class="form-control" id="additionalHealthCareInfo" name="additionalHealthCareInfo" rows="3" placeholder="Additional baby sitting /child care or children's activities information"></textarea>
                        </div>
                    </div>
                </div>
                <h5 class="p-3 mb-3 bg-primary text-white">Exercise Fitness</h5>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="sportTrainer">Sport trainer</label>
                            <select class="form-control" id="sportTrainer" name="sportTrainer" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="aerobicsInstructor">Aerobics Instructor</label>
                            <select class="form-control" id="aerobicsInstructor" name="aerobicsInstructor" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="universalGYM">Universal gym</label>
                            <select class="form-control" id="universalGYM" name="universalGYM" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="spaFacilities">Spa facilities</label>
                            <select class="form-control" id="spaFacilities" name="spaFacilities" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="lockerRoom">Locker room</label>
                            <select class="form-control" id="lockerRoom" name="lockerRoom" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="treadmill">Treadmill</label>
                            <select class="form-control" id="treadmill" name="treadmill" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="stationaryBike"> Stationary bike</label>
                            <select class="form-control" id="stationaryBike" name="stationaryBike" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="stairStepper">Stair stepper</label>
                            <select class="form-control" id="stairStepper" name="stairStepper" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="rowingMachine">Rowing machine</label>
                            <select class="form-control" id="rowingMachine" name="rowingMachine" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="weightLiftingEquipment">Weight lifting equipment</label>
                            <select class="form-control" id="weightLiftingEquipment" name="weightLiftingEquipment" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="joggingTrack">Jogging track</label>
                            <select class="form-control" id="joggingTrack" name="joggingTrack" required>
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <h5 class="p-3 mb-3 bg-primary text-white">Other hotel amenities</h5>
                <div class="row">
                   
                          <div class="form-group">
                              <label for="otherHotelAmenities1">Other hotel amenities 1</label>
                              <input type="text" class="form-control" id="otherHotelAmenities1" name="otherHotelAmenities1" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities2">Other hotel amenities 2</label>
                              <input type="text" class="form-control" id="otherHotelAmenities2" name="otherHotelAmenities2" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities3">Other hotel amenities 3</label>
                              <input type="text" class="form-control" id="otherHotelAmenities3" name="otherHotelAmenities3" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities4">Other hotel amenities 4</label>
                              <input type="text" class="form-control" id="otherHotelAmenities4" name="otherHotelAmenities4" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities5">Other hotel amenities 5</label>
                              <input type="text" class="form-control" id="otherHotelAmenities5" name="otherHotelAmenities5" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities6">Other hotel amenities 6</label>
                              <input type="text" class="form-control" id="otherHotelAmenities6" name="otherHotelAmenities6" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities7">Other hotel amenities 7</label>
                              <input type="text" class="form-control" id="otherHotelAmenities7" name="otherHotelAmenities7" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities8">Other hotel amenities 8</label>
                              <input type="text" class="form-control" id="otherHotelAmenities8" name="otherHotelAmenities8" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities9">Other hotel amenities 9</label>
                              <input type="text" class="form-control" id="otherHotelAmenities9" name="otherHotelAmenities9" required>
                          </div>
                          <div class="form-group">
                              <label for="otherHotelAmenities10">Other hotel amenities 10</label>
                              <input type="text" class="form-control" id="otherHotelAmenities10" name="otherHotelAmenities10" required>
                          </div>
                        
                   

                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection
