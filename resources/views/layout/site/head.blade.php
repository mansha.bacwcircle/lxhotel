<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>LX Hotel </title>
  <link rel="stylesheet" href="{{url('site/assets/bootstrap-5.3.2-dist/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('site/assets/js-plugins/fontawesome-free-6.1.1-web/css/all.min.css')}}">

  <!-- color scheme -->
  <link rel="stylesheet" href="{{url('site/assets/css/style.css')}}">
  <link rel="apple-touch-icon" sizes="57x57" href="{{url('site/assets/icons/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{url('site/assets/icons/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{url('site/assets/icons/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{url('site/assets/icons/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{url('site/assets/icons/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{url('site/assets/icons/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{url('site/assets/icons/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{url('site/assets/icons/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{url('site/assets/icons/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{url('site/assets/icons/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{url('site/assets/icons/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{url('site/assets/icons/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{url('site/assets/icons/favicon-16x16.png')}}">
  <link rel="manifest" href="{{url('site/assets/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{url('site/assets/icons/ms-icon-144x144.png')}}">
  <meta name="theme-color" content="#ffffff">

  <script src="{{url('site/assets/js/jquery-3.6.0.min.js')}}"></script>

<link href="{{url('site/assets/css/select2.min.css')}}" rel="stylesheet">
<script src="{{url('site/assets/js/select2.min.js')}}"></script>

</head>