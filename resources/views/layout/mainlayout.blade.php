<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.partials.head')
  </head>
  @if(!Route::is(['error-404','error-500']))
<body>
 @endif
@if(Route::is(['error-404','error-500']))
<body class="error-page">
@endif
@if(Route::is(['forgetpassword','resetpassword','signin','signup','reset.password.get']))
<body class="account-page">
@endif
  <!-- Main Wrapper -->
<div class="main-wrapper">
  @if(!Route::is(['error-404','error-500','forgetpassword','reset.password.get','adminlogin','resetpassword','signin','signup']))
    @include('layout.partials.header')
    @endif
    @if(!Route::is(['error-404','error-500','forgetpassword','pos','adminlogin','reset.password.get','resetpassword','signin','signup']))
    @include('layout.partials.sidebar')
  @endif
    @yield('content')
</div>
<!-- /Main Wrapper -->
    @include('layout.partials.footer-scripts')
  </body>
</html>
