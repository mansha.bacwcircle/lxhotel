<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
<meta name="description" content="<?php echo isset($seosettings['description']) ? $seosettings['description'] : ''; ?>">
<meta name="keywords" content="<?php echo isset($seosettings['keywords']) ? $seosettings['keywords'] : ''; ?>">
<meta name="title" content="<?php echo isset($seosettings['metatitle']) ? $seosettings['metatitle'] : ''; ?>">
<meta name="author" content="Dreamguys - Bootstrap Admin Template">
<meta name="robots" content="noindex, nofollow">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{$sitesettings['name']}} | admin </title>

<!-- Favicon -->
@if($sitesettings->favicon)
   <link rel="shortcut icon" href="{{ URL::to($sitesettings->favicon)}}">
@else
   <link rel="shortcut icon" href="{{url('assets/img/favicon.png')}}">
@endif
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,500;0,600;0,700;1,400&display=swap" rel="stylesheet">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('assets/css/customstyle.css')}}">
<!-- bootstrap-toggle.min.css  -->
<link rel="stylesheet" href="{{url('assets/css/bootstrap-toggle.min.css')}}">
<!-- Fontawesome CSS -->
<link rel="stylesheet" href="{{url('assets/plugins/fontawesome/css/fontawesome.min.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/fontawesome/css/all.min.css')}}">
<!-- animation CSS -->
<link rel="stylesheet" href="{{url('assets/css/animate.css')}}">
 <!-- Owl Carousel CSS -->
 <link rel="stylesheet" href="{{url('assets/plugins/owlcarousel/owl.carousel.min.css')}}">
 <link rel="stylesheet" href="{{url('assets/plugins/owlcarousel/owl.theme.default.min.css')}}">
<!-- Select2 CSS -->
<link rel="stylesheet" href="{{url('assets/plugins/select2/css/select2.min.css')}}">
<!-- Dragula CSS -->
<link rel="stylesheet" href="{{url('assets/plugins/dragula/css/dragula.min.css')}}">
<!-- Datatable CSS -->
<link rel="stylesheet" href="{{url('assets/css/dataTables.bootstrap4.min.css')}}">
<!-- Main CSS -->
<link rel="stylesheet" href="{{url('assets/css/style.css')}}">

<!-- <link rel="stylesheet" href="{{url('assets/phonecode/css/intlTelInput.css')}}"> -->
<!-- <link rel="stylesheet" href="{{url('assets/phonecode/css/intlTelInput.min.css')}}"> -->



<!-- jQuery -->
<script src="{{ URL::asset('/assets/js/jquery-3.6.0.min.js')}}"></script>

<!-- <script src="{{url('assets/phonecode/js/intlTelInput.min.js')}}"></script>
<script src="{{url('assets/phonecode/js/intlTelInput-jquery.min.js')}}"></script> -->


<script src="{{ URL::asset('/assets/js/jquery.validate.min.js')}}"></script>
<!-- Bootstrap Core JS -->
<script src="{{ URL::asset('/assets/js/bootstrap.bundle.min.js')}}"></script>
<!-- Datatable JS -->
<script src="{{ URL::asset('/assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('/assets/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- ckeditor.js -->
<!-- <script src="{{ URL::asset('/assets/js/ckeditor.js')}}"></script> -->
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<!-- Select2 JS -->
<script src="{{ URL::asset('/assets/plugins/select2/js/select2.min.js')}}"></script>
<!-- Feather Icon JS -->
<script src="{{ URL::asset('/assets/js/feather.min.js')}}"></script>
<!-- Slimscroll JS -->
<script src="{{ URL::asset('/assets/js/jquery.slimscroll.min.js')}}"></script>
<!-- Sweetalert 2 -->
<script src="{{ URL::asset('/assets/plugins/sweetalert/sweetalert2.all.min.js')}}"></script>
<script src="{{ URL::asset('/assets/plugins/sweetalert/sweetalerts.min.js')}}"></script>
<!-- Owl JS -->
<script src="{{ URL::asset('/assets/plugins/owlcarousel/owl.carousel.min.js')}}"></script>
<!-- Fileupload JS -->
<script src="{{ URL::asset('/assets/plugins/fileupload/fileupload.min.js')}}"></script>
<!-- Custom JS -->
<script src="{{ URL::asset('/assets/js/script.js')}}"></script>
<!-- bootstrap-toggle js -->
<script src="{{ URL::asset('/assets/js/bootstrap-toggle.min.js')}}"></script>
<script src="{{ URL::asset('assets/apexcharts/apexcharts.min.js') }}"></script>
