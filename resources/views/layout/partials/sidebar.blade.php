<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="submenu-open">
                    <h6 class="submenu-hdr">Main</h6>
                    <ul>
                        <li class="{{ Request::is('admindashboard','index-two') ? 'active' : '' }}" >
                            <a href="{{url('admindashboard')}}"><i data-feather="grid"></i><span>Dashboard</span></a>
                        </li>
                    </ul>
                </li>
                <li class="submenu-open">
                    <h6 class="submenu-hdr">Products</h6>
                    <ul>
                        <li class="{{ Request::is('hotellist','addhotel','edithotel') ? 'active' : '' }}"><a href="{{url('hotellist')}}"><i data-feather="tag"></i><span>Hotel</span></a></li>
                      
                       
                    </ul>
                </li>
              
              
                <li class="submenu-open">
                    <h6 class="submenu-hdr">Settings</h6>
                    <ul>
                        <li class="submenu">
                            <a class="{{ Request::is('taxlist','addtax','edittax','addconfig','editconfig','listconfig','adminsetting') ? 'active subdrop' : '' }}" href="javascript:void(0);"><i data-feather="settings"></i><span>Settings</span><span class="menu-arrow"></span></a>
                            <ul>
                                <li><a class="{{ Request::is('adminsetting') ? 'active' : '' }}" href="{{url('adminsetting')}}">General Settings</a></li>
                              
                            </ul>
                        </li>
                        <li>
                            <a href="{{URL::to('/adminlogout')}}" ><i data-feather="log-out"></i><span>Logout</span> </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
