<div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <div class="left-cnt">
            <ul>
              <li class="one">
                <p>
                Hotel Description
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="hotelDescription" name="hotelDescription" rows="3" placeholder="Enter hotel description"></textarea>
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                100% Smoke Free Hotel (including all public areas)
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="smokeFreeHotel" name="smokeFreeHotel" >
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                Non-Smoking Rooms Available
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="nonSmokingRooms" name="nonSmokingRooms" >
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                Total Number Of Allocated Non-Smoking Rooms Available
                </p>
              </li>
              <li class="two">
              <input type="number" class="form-control" id="allocatedSuits" name="allocatedSuits" placeholder="Enter number of suites" >
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                Total Number of Allocated Accessible Rooms
                </p>
              </li>
              <li class="two">
              <input type="number" class="form-control" id="allocatedAccessible" name="allocatedAccessible" placeholder="Enter number of accessible rooms" >
              </li>
              
            </ul>
            <ul>
              <li class="one">
                <p>
                What types of Corridors Does Your Hotel Have (select one ?)
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="corridors" name="corridors" >
                    <option value="">Select</option>
                    <option value="interior">Interior Corridors</option>
                    <option value="exterior">Exterior Corridors</option>
                    <option value="interiorExterior">Interior/Exterior Corridors</option>
                </select>
              </li>
              
            </ul>
            <h4 class="p-3 mb-3 bg-warning text-white">Hotel Location And Type</h4>
            <h5>Driving Direction</h5>

            <ul>
              <li class="one">
                <p>
                Driving Direction 1
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="drivingDirection1" name="drivingDirection1" rows="3" placeholder="Enter drivingDirection 1"></textarea>
              </li>
              
            </ul>
            <ul>
              <li class="one">
                <p>
                Driving Direction 2
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="drivingDirection2" name="drivingDirection2" rows="3" placeholder="Enter drivingDirection 2"></textarea>

              </li>
              
            </ul>
            <ul>
              <li class="one">
                <p>
                Driving Direction 3
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="drivingDirection3" name="drivingDirection3" rows="3" placeholder="Enter drivingDirection 3"></textarea>

              </li>
              
            </ul>
            <ul>
              <li class="one">
                <p>
                Driving Direction 4
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="drivingDirection4" name="drivingDirection4" rows="3" placeholder="Enter drivingDirection 4"></textarea>


              </li>
              
            </ul>
            <h4 class="p-3 mb-3 bg-warning text-white">Service Level And Hotel Service</h4>
            <ul>
              <li class="one">
                <p>
                Full Service or Limited Service
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="serviceLevel" name="serviceLevel" >
                    <option value="">Select</option>
                    <option value="Limited Service Offer">Limited Service Hotel</option>
                    <option value="Full Service Offer">Full Service Hotel</option>
                </select>


              </li>
              
            </ul>
           
            
           
            <div class="bottom-buttons">
              <a href="Javascript:void(0);" class="previous">Previous</a>
              <a href="Javascript:void(0);" class="next">Next</a>
            </div>
          </div>

        </div>
        <div class="col-lg-5">
          <div class="right-cnt">
            <div class="right-image">
              <img src="{{url('site/assets/images/white-lx-logo.webp')}}" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <script>
    function showAdditionalOptions() {
        var selectElement = document.getElementById("currentGDSCode");
        var additionalOptions = document.getElementById("additionalOptions");

        if (selectElement.value === "yes") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }

    $(document).ready(function() {
        $('.next').click(function(e) {
            e.preventDefault(); // Prevent default link behavior
            window.location.href = '/hotelamenities'; // Replace 'next-page-url' with the URL of the next page
        });
    });



    </script>