<div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <div class="left-cnt">
          <h5 class="p-3 mb-3 bg-warning text-white">Cable Or Satellite, DVD</h5>
            <ul>
              <li class="one">
                <p>
                Cable or satellite tV on Premises
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="cableSatelliteTvOnPremises" name="cableSatelliteTvOnPremises" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                Additional Cable or satellite tv on Premises information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additonalCableSatelliteTvOnPremises" name="additonalCableSatelliteTvOnPremises" rows="3" placeholder="Additional Cable or satellite tv on Premises information"></textarea>
              </li>
            </ul>
            <h5 class="p-3 mb-3 bg-warning text-white">Child Care Activities</h5>
            <ul>
              <li class="one">
                <p>
                Baby sitting / Child care available
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="babySittingChildCare" name="babySittingChildCare" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                Baby sitting / Child care available for 24 hrs
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="babySittingChildCareAvailable24hrs" name="babySittingChildCareAvailable24hrs" onchange="showAdditionalOptionsforbabysitting()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>
            </ul>
            <ul>
                    <div class="row" id="Babysittingavailabel" style="display:none;" >
                            <h5>If Baby sitting not available for 24 hours, What hours is it available ?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="babySittingChildCareFromTime"> From Time</label>
                                    <input type="time" class="form-control" id="babySittingChildCareFromTime" name="babySittingChildCareFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="babySittingChildCareToTime">To Time</label>
                                    <input type="time" class="form-control" id="babySittingChildCareToTime" name="babySittingChildCareToTime" >
                                </div>
                            </div>
                        </div>
              
            </ul>
            <ul>
              <li class="one">
                <p>
                Children activities
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="childrenActivities" name="childrenActivities" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>
              
            </ul>
              <ul>
              <li class="one">
                <p>
                Children activities available for 24 hrs
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="childrenActivitiesavailable24hrs" name="childrenActivitiesavailable24hrs" onchange="showAdditionalOptionsforChildrenactivity()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>
              
            </ul>
         
            <ul>
          
            <div class="row" id="childrenActavailable" style="display:none;" >
            <h5>If Children activities not available for 24 hours, What hours is it available ?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesFromTime"> From time</label>
                                    <input type="time" class="form-control" id="childrenActivitiesFromTime" name="childrenActivitiesFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesToTime"> To time</label>
                                    <input type="time" class="form-control" id="childrenActivitiesToTime" name="childrenActivitiesToTime" >
                                </div>
                            </div>
              </div>
              
            </ul>
            <ul>
              <li class="one">
                <p>
                Are baby sitting /child care or children's activities complimentary, or is there any fee
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="childrenActivitiescomplmentaryorfee" name="childrenActivitiescomplmentaryorfee" onchange="complimentaryOrFree()">
                                <option value="">Select</option>
                                <option value="complimentary">complimentary</option>
                                <option value="Available for fee">Available for fee</option>
                            </select>

              </li>
              
            </ul>
            <div id="babySittingActivityFeeul" style="display:none;">
            <ul>
              <li class="one">
                <p>
                If baby sitting /child care or children's activities are not complimentary , What is the fee?
                </p>
              </li>
              <li class="two">
              <input type="text" class="form-control" id="babySittingActivityFee" name="babySittingActivityFee" placeholder="Enter $ Fee per hour/ per day" >


              </li>
              
            </ul>
          </div>
            <ul>
              <li class="one">
                <p>
                Additional baby sitting /child care or children's activities information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalBabySittingChildCareInfo" name="additionalBabySittingChildCareInfo" rows="3" placeholder="Additional baby sitting /child care or children's activities information"></textarea>


              </li>
              
            </ul>
            <h5 class="p-3 mb-3 bg-warning text-white">Playground</h5>
            <ul>
              <li class="one">
                <p>
                Playground
                </p>
              </li>
              <li class="two">
              <select class="form-control" id="playground" name="playground" onchange="showAdditionalOptionsplayground()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
            </ul>
            <ul>
            <div class="row" id="childrenActivitiesTime" style="display:none;">
            <h5>Playground what hour is it open?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesFromTime">Play ground from time</label>
                                    <input type="time" class="form-control" id="playgroundFromTime" name="playgroundFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="childrenActivitiesToTime"> Play ground to time</label>
                                    <input type="time" class="form-control" id="playgroundToTime" name="playgroundToTime" >
                                </div>
                            </div>
                        </div>
            </ul>

            <ul>
              <li class="one">
                <p>
                Additional playground information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalPlaygroundInfo" name="additionalPlaygroundInfo" rows="3" placeholder="Additional playground information"></textarea>
              </li>              
            </ul>
            <h5 class="p-3 mb-3 bg-warning text-white">Health Club / exercise Facilities</h5>
            <ul>
              <li class="one">
                <p>
                Health club / exercise facilities
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="healthClubHeltFacility" name="healthClubHeltFacility" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
            </ul>

            <ul>
              <li class="one">
                <p>
                Is it open 24 hours?
                </p>
              </li>
              <li class="two">
              <select class="form-select" id="healthclubopen24hrs" name="healthclubopen24hrs" onchange="showAdditionalOptionsHealthclub()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
            </ul>
            <ul>
          
                        <div class="row" id="healthClubTime" style="display:none">
                        <h5>If Health club / exercise facilities is not open for 24 hours, What hours is it open?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="healthClubFromTime">Health club / exercise facilities open from </label>
                                    <input type="time" class="form-control" id="healthClubFromTime" name="healthClubFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="healthClubToTime"> Health club / exercise facilities close at</label>
                                    <input type="time" class="form-control" id="healthClubToTime" name="healthClubToTime" >
                                </div>
                            </div>
                        </div>
            </ul>

            <ul>
              <li class="one">
                <p>
                Is Health club / exercise facilities complimentary, or is there any fee?
                </p>
              </li>
              <li class="two">
              <select class="form-control" id="healthClubExcersisecomplimentaryOrFee" name="healthClubExcersisecomplimentaryOrFee" onchange="heatthclubcomplimentaryOrfee()">
                                <option value="">Select</option>
                                <option value="complimentary">complimentary</option>
                                <option value="Available for fee">Available for fee</option>
                            </select>
              </li>              
            </ul>
            <div id="healthClubExersiseFeeul" style="display:none;">
            <ul>
              <li class="one">
                <p>
                If Health club / exercise facilities are not complimentary , What is the fee?
                </p>
              </li>
              <li class="two">
              <input type="text" class="form-control" id="healthClubExersiseFee" name="healthClubExersiseFee" placeholder="Enter $ Fee per hour/ per day" >
              </li>              
            </ul>
            </div>

            <ul>
              <li class="one">
                <p>
                Additional Health club / exercise facilities information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalHealthCareInfo" name="additionalHealthCareInfo" rows="3" placeholder="Additional baby sitting /child care or children's activities information"></textarea>
              </li>              
            </ul>
            <h5 class="p-3 mb-3 bg-warning text-white">Exercise Fitness</h5>

            <ul>
              <li class="one">
                <p>Sport trainer</p>
              </li>
              <li class="two">
              <select class="form-select" id="sportTrainer" name="sportTrainer" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Aerobics Instructor</p>
              </li>
              <li class="two">
              <select class="form-select" id="aerobicsInstructor" name="aerobicsInstructor" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Universal gym</p>
              </li>
              <li class="two">
              <select class="form-select" id="universalGYM" name="universalGYM" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Spa facilities</p>
              </li>
              <li class="two">
              <select class="form-select" id="spaFacilities" name="spaFacilities" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Locker room</p>
              </li>
              <li class="two">
              <select class="form-select" id="lockerRoom" name="lockerRoom" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Treadmill</p>
              </li>
              <li class="two">
              <select class="form-select" id="treadmill" name="treadmill" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Stationary bike</p>
              </li>
              <li class="two">
              <select class="form-select" id="stationaryBike" name="stationaryBike" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>

              <ul>
              <li class="one">
                <p>Stair stepper</p>
              </li>
              <li class="two">
              <select class="form-select" id="stairStepper" name="stairStepper" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>


              <ul>
              <li class="one">
                <p>Rowing machine</p>
              </li>
              <li class="two">
              <select class="form-select" id="rowingMachine" name="rowingMachine" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>


              <ul>
              <li class="one">
                <p>Weight lifting equipment</p>
              </li>
              <li class="two">
              <select class="form-select" id="weightLiftingEquipment" name="weightLiftingEquipment" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>


              <ul>
              <li class="one">
                <p>Jogging track</p>
              </li>
              <li class="two">
              <select class="form-select" id="joggingTrack" name="joggingTrack" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
              </li>              
              </ul>


              <h5 class="p-3 mb-3 bg-warning text-white">Other hotel amenities</h5>
              <ul id="hotelAmenitiesList">
              <li class="hotel-amenity">
                <p>Hotel Amenity 1</p>
                <input type="text" class="form-control" name="hotelAmenity1" >
              </li>
            </ul>
            <button id="addAmenityBtn">Add Another Amenity</button>

            

              <h5 class="p-3 mb-3 bg-warning text-white">Pool Steam Room, Whirlpool</h5>
              <ul>
                <li class="one"> <p>Outdoor pool</p> </li>
                <li class="two">
                <select class="form-select" id="outdoorpool" name="outdoorpool" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>
              <ul>
                <li class="one"> <p>If Yes to outdoor pool, Is it heated ?</p> </li>
                <li class="two">
                <select class="form-select" id="isoutdoorpoolheated" name="isoutdoorpoolheated" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
                <li class="one"> <p>If Yes to outdoor pool, Is it open 24 hours ?</p> </li>
                <li class="two">
                <select class="form-select" id="isoutdoorpoolopen24hrs" name="isoutdoorpoolopen24hrs" onchange="showAdditionActionisoutdoorpoolopen24hrs()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
              <div class="row" id="outdoorpooltime" style="display:none;">
                        <h5>If outdoor pool is not open for 24 hours, What hours is it open?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolFromTime">Outdoor pool open from </label>
                                    <input type="time" class="form-control" id="outdoorPoolFromTime" name="outdoorPoolFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolToTime"> Outdoor pool close at</label>
                                    <input type="time" class="form-control" id="outdoorPoolToTime" name="outdoorPoolToTime" >
                                </div>
                            </div>
                        </div>
              </ul>

              <ul>
              <li class="one">
                <p>
                Additional outdoor pool information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionaloutdoorpoolinfo" name="additionaloutdoorpoolinfo" rows="3" placeholder="Additional outdoor pool information"></textarea>
              </li>              
            </ul>
            
            <ul>
                <li class="one"> <p>Indoor pool</p> </li>
                <li class="two">
                <select class="form-select" id="indoorpool" name="indoorpool" onchange="indoorpoolchange()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>
              <div id="indoorpooldiv" style="display:none;">
              <ul>
                <li class="one"> <p>If Yes to indoor pool, Is it heated ?</p> </li>
                <li class="two">
                <select class="form-select" id="isindoorpoolheated" name="isindoorpoolheated" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
                <li class="one"> <p>If Yes to indoor pool, Is it open 24 hours ?</p> </li>
                <li class="two">
                <select class="form-select" id="isindoorpoolopen24hrs" name="isindoorpoolopen24hrs" onchange="isindoorpoolopen24hrs()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
              <div class="row" id="indoorpooltime" style="display:none;">
                        <h5>If indoor pool is not open for 24 hours, What hours is it open?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolFromTime">Indoor pool open from </label>
                                    <input type="time" class="form-control" id="indoorPoolFromTime" name="indoorPoolFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolToTime"> Indoor pool close at</label>
                                    <input type="time" class="form-control" id="IndoorPoolToTime" name="IndoorPoolToTime" >
                                </div>
                            </div>
                        </div>
              </ul>
              </div>
              <ul>
         
              <li class="one">
                <p>
                Additional Indoor pool information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalIndoorpoolinfo" name="additionalIndoorpoolinfo" rows="3" placeholder="Additional Indoor pool information"></textarea>
              </li>              
            </ul>


            <ul>
                <li class="one"> <p>Poolside services</p> </li>
                <li class="two">
                <select class="form-select" id="poolsideservice" name="poolsideservice" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>
             

              <ul>
                <li class="one"> <p>If Yes to poolside services, Are they available 24 hours ?</p> </li>
                <li class="two">
                <select class="form-select" id="poolSideService24hrs" name="poolSideService24hrs" onchange="poolSideService24hrs()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
              <div class="row" id="poolSideServicetime" style="display:none;">
                        <h5>If poolside services not available for 24 hours, What hours are they available ?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolFromTime">Indoor pool open from </label>
                                    <input type="time" class="form-control" id="poolsideFromTime" name="poolsideFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolToTime"> Indoor pool close at</label>
                                    <input type="time" class="form-control" id="poolsideToTime" name="poolsideToTime" >
                                </div>
                            </div>
                        </div>
              </ul>

              <ul>
              <li class="one">
                <p>
                Additional poolside services information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalPoolsideServiceInformation" name="additionalPoolsideServiceInformation" rows="3" placeholder="Additional poolside services information"></textarea>
              </li>              
            </ul>


            <ul>
                <li class="one"> <p>Stream room/Sauna</p> </li>
                <li class="two">
                <select class="form-select" id="streamroomsauna" name="streamroomsauna" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>
             

              <ul>
                <li class="one"> <p>If Yes to stream room /sauna, Is it open 24 hours ?</p> </li>
                <li class="two">
                <select class="form-select" id="streamRoomSauna24hrs" name="streamRoomSauna24hrs" onchange="streamRoomSauna24hrs()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
              <div class="row" id="straemRoomsaunaTime" style="display:none;">
                        <h5>If stream room /sauna not available for 24 hours, What hours is it open ?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolFromTime">stream room /sauna open from </label>
                                    <input type="time" class="form-control" id="straemRoomsaunaFromTime" name="straemRoomsaunaFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolToTime"> stream room /sauna close at</label>
                                    <input type="time" class="form-control" id="straemRoomsaunaToTime" name="straemRoomsaunaToTime" >
                                </div>
                            </div>
                        </div>
              </ul>

              <ul>
              <li class="one">
                <p>
                Additional stream room /sauna information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalstraemRoomSaunaInformation" name="additionalstraemRoomSaunaInformation" rows="3" placeholder="Additional stream room /sauna information"></textarea>
              </li>              
            </ul>


            <ul>
                <li class="one"> <p>Whirlpool or Hot Tub or Therapy Pool</p> </li>
                <li class="two">
                <select class="form-select" id="whirpoolOrhottubOrtherapypool" name="whirpoolOrhottubOrtherapypool" >
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>
             

              <ul>
                <li class="one"> <p>If Yes to whirlpool or hot tub or therapy pool, Is it open 24 hours ?</p> </li>
                <li class="two">
                <select class="form-select" id="whirpoolOrhottubOrtherapypool24hrs" name="whirpoolOrhottubOrtherapypool24hrs" onchange="whirpoolOrhottubOrtherapypool24hrs()">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                </li>              
              </ul>

              <ul>
              <div class="row" id="whirpoolOrhottubOrtherapypoolTime" style="display:none;">
                        <h5>If whirlpool or hot tub or therapy pool not open for 24 hours, What hours is it open ?</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolFromTime">Whirlpool or hot tub or therapy pool open from </label>
                                    <input type="time" class="form-control" id="whirpoolOrhottubOrtherapypoolFromTime" name="whirpoolOrhottubOrtherapypoolFromTime" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="outdoorPoolToTime"> Whirlpool or hot tub or therapy pool close at</label>
                                    <input type="time" class="form-control" id="whirpoolOrhottubOrtherapypoolToTime" name="whirpoolOrhottubOrtherapypoolFromTime" >
                                </div>
                            </div>
                        </div>
              </ul>

              <ul>
              <li class="one">
                <p>
                Additional whirlpool or hot tub or therapy pool information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalwhirpoolOrhottubOrtherapypooInformation" name="additionalwhirpoolOrhottubOrtherapypooInformation" rows="3" placeholder="Additional whirlpool or hot tub or therapy pool information"></textarea>
              </li>              
            </ul>


            
           
            
           
            <div class="bottom-buttons">
              <a href="Javascript:void(0);" class="previous">Previous</a>
              <a href="Javascript:void(0);" class="next">Next</a>
            </div>
          </div>

        </div>
        <div class="col-lg-5">
          <div class="right-cnt">
            <div class="right-image">
              <img src="{{url('site/assets/images/white-lx-logo.webp')}}" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
  <script>
  // JavaScript to add new hotel amenities on button click
  document.getElementById('addAmenityBtn').addEventListener('click', function() {
    // Find the last hotel amenity index
    const lastAmenityIndex = document.querySelectorAll('.hotel-amenity').length;

    // Create new elements
    const newLi = document.createElement('li');
    newLi.classList.add('hotel-amenity');

    const newP = document.createElement('p');
    newP.textContent = 'Other hotel Amenities ' + (lastAmenityIndex + 1);

    const lineBreak = document.createElement('br');

    const newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.classList.add('form-control');
    newInput.name = 'hotelAmenity' + (lastAmenityIndex + 1);
    newInput.id = 'hotelAmenity' + (lastAmenityIndex + 1);
    newInput. = true;

    // Append new elements
    newLi.appendChild(newP);
    newLi.appendChild(newInput);
    document.getElementById('hotelAmenitiesList').appendChild(newLi);
    newLi.appendChild(lineBreak);  
  });
</script>
    <script>
    function showAdditionalOptionsforbabysitting() {
        var selectElement = document.getElementById("babySittingChildCareAvailable24hrs");
        var additionalOptions = document.getElementById("Babysittingavailabel");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function showAdditionalOptionsforChildrenactivity() {
        var selectElement = document.getElementById("childrenActivitiesavailable24hrs");
        var additionalOptions = document.getElementById("childrenActavailable");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }

    function complimentaryOrFree() {
    var selectElement = document.getElementById("childrenActivitiescomplmentaryorfee");
    console.log(selectElement);
    var additionalOptions = document.getElementById("babySittingActivityFeeul");

    if (selectElement.value === "Available for fee") {
        additionalOptions.style.display = "block";
    } else {
        additionalOptions.style.display = "none";
    }
}


function showAdditionalOptionsplayground() {
        var selectElement = document.getElementById("playground");
        var additionalOptions = document.getElementById("childrenActivitiesTime");

        if (selectElement.value === "yes") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }

    function showAdditionalOptionsHealthclub() {
        var selectElement = document.getElementById("healthclubopen24hrs");
        var additionalOptions = document.getElementById("healthClubTime");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function heatthclubcomplimentaryOrfee() {
        var selectElement = document.getElementById("healthClubExcersisecomplimentaryOrFee");
        var additionalOptions = document.getElementById("healthClubExersiseFeeul");

        if (selectElement.value === "Available for fee") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function showAdditionActionisoutdoorpoolopen24hrs() {
        var selectElement = document.getElementById("isoutdoorpoolopen24hrs");
        var additionalOptions = document.getElementById("outdoorpooltime");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }


    function indoorpoolchange() {
        var selectElement = document.getElementById("indoorpool");
        var additionalOptions = document.getElementById("indoorpooldiv");

        if (selectElement.value === "yes") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function isindoorpoolopen24hrs() {
        var selectElement = document.getElementById("isindoorpoolopen24hrs");
        var additionalOptions = document.getElementById("indoorpooltime");

        if (selectElement.value === "yes") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function poolSideService24hrs() {
        var selectElement = document.getElementById("poolSideService24hrs");
        var additionalOptions = document.getElementById("poolSideServicetime");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function streamRoomSauna24hrs() {
        var selectElement = document.getElementById("streamRoomSauna24hrs");
        var additionalOptions = document.getElementById("straemRoomsaunaTime");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }
    function whirpoolOrhottubOrtherapypool24hrs() {
        var selectElement = document.getElementById("whirpoolOrhottubOrtherapypool24hrs");
        var additionalOptions = document.getElementById("whirpoolOrhottubOrtherapypoolTime");

        if (selectElement.value === "no") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }


    $(document).ready(function() {
        $('.next').click(function(e) {
         
            e.preventDefault(); // Prevent default link behavior
            window.location.href = '/hotelservices'; // Replace 'next-page-url' with the URL of the next page
        });
    });



    </script>