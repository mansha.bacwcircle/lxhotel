@include('layout.site.head')
<div class="navbar" id="navbar">
    <div class="container">
      <div class="brand">
        <a href="Javascript:void(0);">Hotel Amenities</a>
      </div>
      <div class="logo">
        <a href="Javascript:void(0)">
          <img src="{{url('site/assets/images/logo.webp')}}" alt="">
        </a>
      </div>
    </div>
  </div>

@include('site.hotelamenities.addamenities')


@include('layout.site.footer')