<div class="content">
<div class="container">
   <div class="row">
      <div class="col-lg-7">
         <div class="left-cnt">
            <ul>
               <li class="one">
                  <p>
                     Foreign currency exchange
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="foreigncurrencyexchange" name="foreigncurrencyexchange" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Banking
                  </p>
               </li>
               <li class="two">
                  <select  class="form-select" aria-label="Default select example" id="banking" name="banking" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Cash machine (ATM)
                  </p>
               </li>
               <li class="two">
                  <select  class="form-select" aria-label="Default select example" id="cashMachineatm" name="cashMachineatm" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <h5 class="p-3 mb-3 bg-warning text-white">Business Services</h5>
            <ul>
               <li class="one">
                  <p>
                     Business center available
                  </p>
               </li>
               <li class="two">
                  <select  class="form-select" aria-label="Default select example" id="businessCenter" name="businessCenter" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     If Yes to business center, Is it available 24 hours?
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="businesscenter24hrs" name="businesscenter24hrs" onchange="showAdditionalbusinesscenter24hrs()">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <div class="row" id="businesscentertime" style="display:none;" >
                  <h5>If business center not open for for 24 hours, What hours is it open ?</h5>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="businesscenterFromTime"> From Time</label>
                        <input type="time" class="form-control" id="businesscenterFromTime" name="businesscenterFromTime" >
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="businesscenterToTime">To Time</label>
                        <input type="time" class="form-control" id="businesscenterToTime" name="businesscenterToTime" >
                     </div>
                  </div>
               </div>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Additional business center information
                  </p>
               </li>
               <li class="two">
                  <textarea class="form-control" id="additionalBusinesscenterInfo" name="additionalBusinesscenterInfo" rows="3" placeholder=" Additional business center information"></textarea>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Printer available
                  </p>
               </li>
               <li class="two">
                  <select  class="form-select" aria-label="Default select example" id="printeravailable" name="businessCenter" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Computer available
                  </p>
               </li>
               <li class="two">
                  <select  class="form-select" aria-label="Default select example" id="computeravailable" name="computeravailable" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Fax service available
                  </p>
               </li>
               <li class="two">
                  <select  class="form-select" aria-label="Default select example" id="faxservice" name="faxservice" >
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     If Yes to fax is there any fee?
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="Isthereafaxfee" name="Isthereafaxfee" onchange="additionalActionIsthereafaxfee()">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <div id="faxfeeperpageul" style="display:none;">
               <ul>
                  <li class="one">
                     <p>
                        If yes to fax fee , What is the fee?
                     </p>
                  </li>
                  <li class="two">
                     <input type="text" class="form-control" id="faxfeeperpage" name="faxfeeperpage" placeholder="Enter $ Fee per page" >
                  </li>
               </ul>
            </div>
            <ul>
               <li class="one">
                  <p>
                     Equipment rental available
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="equipmentrentalavailable" name="equipmentrentalavailable">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Cellular/ mobile phone rental
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="cellularmobilerental" name="cellularmobilerental">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     High-speed internet access in public area
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="highSpeedInternetAccess" name="highSpeedInternetAccess" onchange="additionalActionhighSpeedInternetAccess()">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <div id="highSpeedInternetAccesscomplimentaryOrfeeul" style="display:none;">
               <ul>
                  <li class="one">
                     <p>
                        If yes to high-speed internet access in public area,Is it complimentary or there is a fee?
                     </p>
                  </li>
                  <li class="two">
                     <select class="form-select" id="highSpeedInternetAccesscomplimentaryOrfee" name="highSpeedInternetAccesscomplimentaryOrfee" onchange="highSpeedInternetAccesscomplimentaryOrfeeaction()">
                        <option value="">Select</option>
                        <option value="complimentary">complimentary</option>
                        <option value="fee">Available for fee</option>
                     </select>
                  </li>
               </ul>
            </div>
            <div id="highspeedinternetperdayul" style="display:none;">
               <ul>
                  <li class="one">
                     <p>
                        If high-speed internet access in public area is not complimentary,What is the fee?
                     </p>
                  </li>
                  <li class="two">
                     <input type="text" class="form-control" id="highspeedinternetperday" name="highspeedinternetperday" placeholder="Enter $ Fee per day" >
                  </li>
               </ul>
            </div>
            <ul>
               <li class="one">
                  <p> Video conferencing  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="videoconference" name="videoconference">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Shipping  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="shipping" name="shipping">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Notary public  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="notarypublic" name="notarypublic">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Courier  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="courier" name="courier">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Conference services  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="conferenceServices" name="conferenceServices">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Computer modem hookup  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="computermodemhookup" name="computermodemhookup">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Photocopy service  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="photocopyservice" name="photocopyservice">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Secretarial service  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="secretarial" name="secretarial">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> 1.Free wireless internet available in public area  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="freeWirelessinternetAvailable" name="freeWirelessinternetAvailable">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> 2.Free wireless internet available in room  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="freeWirelessinternetinroomAvailable" name="freeWirelessinternetinroomAvailable">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> 3.Free wireless internet available in public area and all guest rooms  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="freeWirelessinternetinallguestroomAvailable" name="freeWirelessinternetinallguestroomAvailable">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> 4.All guest room are hardwired and offer free wireless internet access  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="guestRoomHardwiredOfferFreeWirelessInternet" name="guestRoomHardwiredOfferFreeWirelessInternet">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> 5.All guest room are hardwired for internet access  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="guestRoomHardwiredForInternetAccess" name="guestRoomHardwiredForInternetAccess">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <h5 class="p-3 mb-3 bg-warning text-white">Evening Reception</h5>
            <b>Evening Reception: <u>Complimentary</u> drink and snacks provided in commom area of hotel</b>
            <ul>
               <li class="one">
                  <p> Does you have an evening reception?  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="haveeveningreception" name="haveeveningreception" onchange="haveeveningreception()">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <div class="row" id="eveningreceptionTime" style="display:none">
                  <h5>If yes to evening reception , What are the hours?</h5>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="eveningreceptionFromTime">Evening reception open from </label>
                        <input type="time" class="form-control" id="eveningreceptionFromTime" name="eveningreceptionFromTime" >
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="eveningreceptionToTime"> Evening reception  close at</label>
                        <input type="time" class="form-control" id="eveningreceptionToTime" name="eveningreceptionToTime" >
                     </div>
                  </div>
               </div>
            </ul>
            <ul>
               <div class="row" id="receptionWeekAvailableul" style="display:none;">
                  <h5>If yes to evening reception , What days of the week is it available?</h5>
                  <div class="col-md-6">
                     <select class="form-select" id="receptionWeekAvailable" name="receptionWeekAvailable" multiple>
                        <option value="">Select Week days</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wensday">Wensday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                        <option value="Sunday">Sunday</option>
                     </select>
                     <input class="form-control" type="text" id="selectedDaysCount" name="selectedDaysCount" readonly>
                  </div>
               </div>
            </ul>
            <h4 class="p-3 mb-3 bg-warning text-white">Executive/ club floors/ VIP services</h4>
            <ul>
               <li class="one">
                  <p> Executive/ club floors/ VIP services  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="executiveClubFloorVIPServices" name="executiveClubFloorVIPServices">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Additional Executive/ club floors/ VIP services
                  </p>
               </li>
               <li class="two">
                  <textarea class="form-control" id="additionalExecutiveClubFloorVIPservice" name="additionalExecutiveClubFloorVIPservice" rows="3" placeholder=" Additional Executive/ club floors/ VIP services"></textarea>
               </li>
            </ul>
            <h4 class="p-3 mb-3 bg-warning text-white">Front desk information</h4>
            <ul>
               <li class="one">
                  <p> Wake-up calls </p>
               </li>
               <li class="two">
                  <select class="form-select" id="wakeupcall" name="wakeupcall">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Portes/ Bellman </p>
               </li>
               <li class="two">
                  <select class="form-select" id="portesBellman" name="portesBellman">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Theater ticket desk </p>
               </li>
               <li class="two">
                  <select class="form-select" id="theaterTicketDesk" name="theaterTicketDesk">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <li class="one">
                  <p> Hotel safe deposit box at front desk </p>
               </li>
               <li class="two">
                  <select class="form-select" id="safeDepositboxatFrontdesk" name="safeDepositboxatFrontdesk" onchange="safeDepositboxatFrontdesk()">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <div id="safedepositatfrondeskcomplmentaryorfeeul" style="display:none;">
                  <li class="one">
                     <p>
                        If yes to hotel safe deposit box at front desk, Is it complimentary or is there a fee?
                     </p>
                  </li>
                  <li class="two">
                     <div class="col-md-6">
                        <select class="form-select" id="safedepositatfrondeskcomplmentaryorfee" name="safedepositatfrondeskcomplmentaryorfee" onchange="safedepositatfrondeskcomplmentaryorfee()">
                           <option value="">Select</option>
                           <option value="complimentary">complimentary</option>
                           <option value="fee">Available for fee</option>
                        </select>
                     </div>
                  </li>
               </div>
            </ul>
            <ul>
               <div id="safedepositboxFeeul" style="display:none;">
                  <li class="one">
                     <p>
                        If hotel safe deposit box not complimentary , What is the fee?
                     </p>
                  </li>
                  <li class="two">
                     <input type="text" class="form-control" id="safedepositboxFee" name="safedepositboxFee" placeholder="Enter $ Fee per hour/ per day" >
                  </li>
               </div>
            </ul>
            <ul>
               <li class="one">
                  <p>
                     Multilingual staff
                  </p>
               </li>
               <li class="two">
                  <select class="form-select" id="multilingualstaff" name="multilingualstaff" onchange="multilingualstaff()">
                     <option value="">Select</option>
                     <option value="yes">Yes</option>
                     <option value="no">No</option>
                  </select>
               </li>
            </ul>
            <ul>
               <div id="multilingualstafflanguagelistul" style="display:none;">
                  <li class="one">
                     <p>
                        If yes to Multilingual staff, you must list languages spoken by staff
                     </p>
                  </li>
                  <li class="two">
                     <select class="form-select" id="multilingualstafflanguagelist" name="multilingualstafflanguagelist" multiple>
                        <option value="English">English</option>
                        <option value="Spanish">Spanish</option>
                        <option value="French">French</option>
                        <option value="German">German</option>
                        <!-- Add more languages as needed -->
                     </select>
                  </li>
            </ul>
            <ul>
   <li class="one">
      <p>
         Translation services
      </p>
   </li>
   <li class="two">
      <select class="form-select" id="translationServices" name="translationServices">
         <option value="">Select</option>
         <option value="yes">Yes</option>
         <option value="no">No</option>
      </select>
   </li>
</ul>
<ul>
   <li class="one">
      <p>
         24 hours front desk
      </p>
   </li>
   <li class="two">
      <select class="form-select" id="24hoursfrontDesk" name="24hoursfrontDesk" onchange="ActionahoursfrontDesk()">
         <option value="">Select</option>
         <option value="yes">Yes</option>
         <option value="no">No</option>
      </select>
   </li>
</ul>
<ul>
   <div class="row" id="deskAvailableTime" style="display:none">
      <h5>If front desk is not open for 24 hours, What hours is it open?</h5>
      <div class="col-md-6">
         <div class="form-group">
            <label for="deskAvailableFromTime">Front desk open from </label>
            <input type="time" class="form-control" id="deskAvailableFromTime" name="deskAvailableFromTime" >
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="deskAvailableToTime">Front desk close at</label>
            <input type="time" class="form-control" id="deskAvailableToTime" name="deskAvailableToTime" >
         </div>
      </div>
   </div>
</ul>
<ul>
   <li class="one">
      <p>
         Additional front desk information concierge information
      </p>
   </li>
   <li class="two">
      <textarea class="form-control" id="additionalFrontdeskinformation" name="additionalFrontdeskinformation" rows="3" placeholder="Additional front desk information concierge information"></textarea>
   </li>
</ul>
<ul>
   <li class="one">
      <p>
         Concierge
      </p>
   </li>
   <li class="two">
      <select class="form-select" id="concierge" name="concierge">
         <option value="">Select</option>
         <option value="yes">Yes</option>
         <option value="no">No</option>
      </select>
   </li>
</ul>
<h5 class="p-3 mb-3 bg-warning text-white">Gift shop, Sundaries, Pharmacy</h5>
<ul>
   <li class="one">
      <p>
         Pharmacy
      </p>
   </li>
   <li class="two">
      <select class="form-select" id="pharmacy" name="pharmacy" onchange="availablepharmacy()">
         <option value="">Select</option>
         <option value="yes">Yes</option>
         <option value="no">No</option>
      </select>
   </li>
</ul>
<ul>
   <div class="row" id="pharmacyopenhourul" style="display:none">
      <li class="one">
         <p>
            If yes to pharmacy is it open 24 hours?
         </p>
      </li>
      <li class="two">
         <select class="form-select" id="pharmacyopenhour" name="pharmacyopenhour" onchange="pharmacyopenhour()">
            <option value="">Select</option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
         </select>
      </li>
   </div>
</ul>
<ul>
   <div class="row" id="pharmacyTime" style="display:none">
      <h5>If pharmacy is not open for 24 hours, What hours is it open?</h5>
      <div class="col-md-6">
         <div class="form-group">
            <label for="pharmacyFromTime"> Pharmacy open from </label>
            <input type="time" class="form-control" id="pharmacyFromTime" name="pharmacyFromTime" >
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="pharmacyToTime"> Pharmacy close at</label>
            <input type="time" class="form-control" id="pharmacyToTime" name="pharmacyToTime" >
         </div>
      </div>
   </div>
</ul>
<ul>
   <li class="one">
      <p>
         Additional pharmacy information
      </p>
   </li>
   <li class="two">
      <textarea class="form-control" id="additionalpharmacyinformation" name="additionalpharmacyinformation" rows="3" placeholder="Additional pharmacy information"></textarea>
   </li>
</ul>
<!-- ----------------------------------------------------------------------------- -->
<ul>
   <li class="one">
      <p>
         Convenience / Sundry store
      </p>
   </li>
   <li class="two">
      <select class="form-select" id="pharmconvenienceSundryStoreacy" name="pharmconvenienceSundryStoreacy" onchange="pharmconvenienceSundryStoreacy()">
         <option value="">Select</option>
         <option value="yes">Yes</option>
         <option value="no">No</option>
      </select>
   </li>
</ul>
<ul>
   <div class="row" id="convenienceopenhourdiv" style="display:none;">
      <li class="one">
         <p>
            If yes to convenience / Sundry store is it open 24 hours?
         </p>
      </li>
      <li class="two">
         <select class="form-select" id="convenienceopenhour" name="convenienceopenhour" onchange="convenienceopenhour()">
            <option value="">Select</option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
         </select>
      </li>
   </div>
</ul>
<ul>
   <div class="row" id="convenienceopenTime" style="display:none">
      <h5>If  convenience / Sundry store is not open for 24 hours, What hours is it open?</h5>
      <div class="col-md-6">
         <div class="form-group">
            <label for="convenienceFromTime"> Open from </label>
            <input type="time" class="form-control" id="convenienceFromTime" name="convenienceFromTime" >
         </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            <label for="convenienceToTime">Close at</label>
            <input type="time" class="form-control" id="convenienceToTime" name="convenienceToTime" >
         </div>
      </div>
   </div>
</ul>
<ul>
   <li class="one">
      <p>
         convenience / Sundry store information
      </p>
   </li>
   <li class="two">
      <textarea class="form-control" id="convenienceinformation" name="convenienceinformation" rows="3" placeholder="Additional convenience / Sundry store information"></textarea>
   </li>
</ul>


<!-- ---------------------------------------------------------------------------------------------------------------- -->

        <ul>
          <div class="row">
              <li class="one">
                <p>
                    Gift shop newsstand
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="gitshopnewsstand" name="gitshopnewsstand" onchange="gitshopnewsstand()">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>
          </div>
        </ul>

        <ul>
        <div class="row" id="gitshopnewsstand24hrsul" style="display:none;">
            <li class="one">
              <p>
                  If yes to gift shop newsstand, is it open 24 hours?
              </p>
            </li>
            <li class="two">
              <select class="form-select" id="gitshopnewsstand24hrs" name="gitshopnewsstand24hrs" onchange="gitshopnewsstandhrs()">
                  <option value="">Select</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
              </select>
            </li>
        </div>
      </ul>

      <ul>
        <div class="row" id="giftShopNewsstandtime" style="display:none">
            <h5>If  gift shop newsstand is not open for 24 hours, What hours is it open?</h5>
            <div class="col-md-6">
              <div class="form-group">
                  <label for="giftShopNewsstandFromTime"> Open from </label>
                  <input type="time" class="form-control" id="giftShopNewsstandFromTime" name="giftShopNewsstandFromTime" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label for="giftShopNewsstandToTime">Close at</label>
                  <input type="time" class="form-control" id="giftShopNewsstandToTime" name="giftShopNewsstandToTime" >
              </div>
            </div>
        </div>
      </ul>


      <!-- --------------------------------------------------------------------------------------------------------- -->
      <h5 class="p-3 mb-3 bg-warning text-white">Guest Laundary</h5>
      <ul>
          <div class="row">
              <li class="one">
                <p>
                    Guest laundary facility
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="guestLaundaryFacility" name="guestLaundaryFacility" onchange="guestLaundaryFacility()">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>
          </div>
        </ul>

        <ul>
        <div class="row" id="guestlaundary24hrsul" style="display:none;">
            <li class="one">
              <p>
                  If yes to guest laundary facility, is it open 24 hours?
              </p>
            </li>
            <li class="two">
              <select class="form-select" id="guestlaundary24hr" name="guestlaundary24hr" onchange="guestlaundaryhr()">
                  <option value="">Select</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
              </select>
            </li>
        </div>
      </ul>

      <ul>
        <div class="row" id="guestlaundaryFacilityHrs" style="display:none">
            <h5>If  guest laundary facility is not open for 24 hours, What hours is it open?</h5>
            <div class="col-md-6">
              <div class="form-group">
                  <label for="guestlaundaryFacilityFromTime"> Open from </label>
                  <input type="time" class="form-control" id="guestlaundaryFacilityFromTime" name="guestlaundaryFacilityFromTime" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  <label for="guestlaundaryFacilityToTime">Close at</label>
                  <input type="time" class="form-control" id="guestlaundaryFacilityToTime" name="guestlaundaryFacilityToTime" >
              </div>
            </div>
        </div>
      </ul>

      <ul>
          <div class="row">
              <li class="one">
                <p>
                   Does guest laundary facility contain self service washer and dryer
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="guestLaundaryFacilitycontainSelfservice" name="guestLaundaryFacilitycontainSelfservice">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>
          </div>
        </ul>

        <ul>
          
              <li class="one">
                <p>
                   Are the washer and dryer coin-operated
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="coinoperated" name="coinoperated">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>
         
        </ul>

        <ul>
          
              <li class="one">
                <p>
                   Would you like to list an actual fee, or just "nominal" fee
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="nominalOrFree" name="nominalOrFree" onchange="nominalOrFree()">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="nominal">Nominal fee</option>
                </select>
              </li>
         
        </ul>

        <div id="feePercycleul" style="display:none;">
            <ul>
              <li class="one">
                <p>
                If yes to actual fee , What is the fee?
                </p>
              </li>
              <li class="two">
              <input type="text" class="form-control" id="feePercycle" name="feePercycle" placeholder="Enter $ Fee per cycle" >
              </li>              
            </ul>
            </div>

            <ul>
              <li class="one">
                <p>
                Additional guest laundary facilities information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalGuestLaundaryfacilityInfo" name="additionalGuestLaundaryfacilityInfo" rows="3" placeholder="Additional guest laundary facilities information"></textarea>
              </li>              
            </ul>

            <ul>
          
          <li class="one">
            <p>
              Dry Cleaning
            </p>
          </li>
          <li class="two">
            <select class="form-select" id="dryCleaning" name="dryCleaning">
                <option value="">Select</option>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
          </li>
     
         </ul>

         <ul>          
          <li class="one">
            <p>
              Same day dry cleaning services
            </p>
          </li>
          <li class="two">
            <select class="form-select" id="sameDayDrycleaning" name="sameDayDrycleaning">
                <option value="">Select</option>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
          </li>     
         </ul>

            <ul>
              <li class="one">
                <p>
                Additional dry cleaning  information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalDryCleaningInfo" name="additionalDryCleaningInfo" rows="3" placeholder="Additional dry cleaning  information"></textarea>
              </li>              
            </ul>

            <!-- ---------------------------------------------------------------------------------------------------- -->
            <h5 class="p-3 mb-3 bg-warning text-white">Hotel Services</h5>

            <ul>          
              <li class="one">
                <p>
                  Turndown services
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="turnDownServices" name="turnDownServices">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                  Ice/Vending machine
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="iceVendingMachine" name="iceVendingMachine">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                  Doctor On-call
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="doctorOnCall" name="doctorOnCall">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                  Dietician
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="Dietician" name="Dietician">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                  Wedding services
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="weddingServices" name="weddingServices">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                  Mail services
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="mailService" name="mailService">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                 Tanning bed
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="tanningBed" name="tanningBed">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                 Cold weather hook-ups
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="coldWeatherhookups" name="coldWeatherhookups">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                 Game room
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="gameRoom" name="gameRoom">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>


            <ul>          
              <li class="one">
                <p>
                 Solarium
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="solarium" name="solarium">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                 Barber/ Beauty shop
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="barberBeautyShop" name="barberBeautyShop">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul>          
              <li class="one">
                <p>
                  Elevator access to all floors
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="elevatorAccesstofloor" name="elevatorAccesstofloor">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>
            <b style="color:red"> (NOT Loaded for U.S or Canada Properties)</b> 
            <ul> 
                      
              <li class="one">
                <p>
                  Air conditioning in public area
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="airconditioningInPublicArea" name="airconditioningInPublicArea">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul> 
                      
              <li class="one">
                <p>
                  Storage space
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="storageSpace" name="storageSpace">
                    <option value="">Select</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
              </li>     
            </ul>

            <ul> 
                      
                      <li class="one">
                        <p>
                          Barbeque grill for guests
                        </p>
                      </li>
                      <li class="two">
                        <select class="form-select" id="barbequeGrillForGuest" name="barbequeGrillForGuest">
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                      </li>     
                    </ul>

                    <ul> 
                      
                      <li class="one">
                        <p>
                          Massage services
                        </p>
                      </li>
                      <li class="two">
                        <select class="form-select" id="massageService" name="massageService">
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                      </li>     
                    </ul>

                    <h5 class="p-3 mb-3 bg-warning text-white">Pet Service</h5>
                    <ul> 
                      
                      <li class="one">
                        <p>
                         Kennels
                        </p>
                      </li>
                      <li class="two">
                        <select class="form-select" id="kennels" name="kennels" onchange="addactionkennels()">
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                      </li>     
                    </ul>

                    <ul> 
                      <div  id="kenelAvailablehours" style="display:none;">
                      <li class="one">
                        <p>
                         If yes to Kennels, are they available 24 hours ?
                        </p>
                      </li>
                      <li class="two">
                        <select class="form-select" id="kennelsavailablehrs" name="kennelsavailablehrs" onchange="kennelsavailablehrs()">
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                      </li> 
                      </div>    
                    </ul>

                    <ul>
          
                    <div class="row" id="kennelsTime" style="display:none">
                    <h5>If kennels not available for 24 hours, What hours they are available?</h5>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kennelsfrom">From </label>
                                <input type="time" class="form-control" id="kennelsfrom" name="kennelsfrom" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="kennelsto">To</label>
                                <input type="time" class="form-control" id="kennelsto" name="kennelsto" >
                            </div>
                        </div>
                    </div>
                </ul>

            <ul>
              <li class="one">
                <p>
                Is kennel complimentary, or is there any fee?
                </p>
              </li>
              <li class="two">
              <select class="form-control" id="kennelcomplimentaryOrFee" name="kennelcomplimentaryOrFee" onchange="kennelcomplimentaryOrFee()">
                                <option value="">Select</option>
                                <option value="complimentary">complimentary</option>
                                <option value="fee">Available for fee</option>
                            </select>
              </li>              
            </ul>

            <div id="kennelfeeul" style="display:none;">
            <ul>
              <li class="one">
                <p>
                If kennel are not complimentary , What is the fee?
                </p>
              </li>
              <li class="two">
              <input type="text" class="form-control" id="kennelfee" name="kennelfee" placeholder="Enter $ Fee per hour/ per day" >
              </li>              
            </ul>
            </div>

            <ul>
              <li class="one">
                <p>
                Additional kennel information
                </p>
              </li>
              <li class="two">
              <textarea class="form-control" id="additionalKennelInfo" name="additionalKennelInfo" rows="3" placeholder="Additional kennel information"></textarea>
              </li>              
            </ul>


            <ul> 
                      
                      <li class="one">
                        <p>
                         Pet sitting services
                        </p>
                      </li>
                      <li class="two">
                        <select class="form-select" id="petSittingService" name="petSittingService" onchange="petSittingService()">
                            <option value="">Select</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                      </li>     
                    </ul>

                   


            <div class="bottom-buttons">
            <a href="Javascript:void(0);" class="previous">Previous</a>
            <a href="Javascript:void(0);" class="next">Next</a>
            </div>
            </div>
         </div>
         <div class="col-lg-5">
            <div class="right-cnt">
               <div class="right-image">
                  <img src="{{url('site/assets/images/white-lx-logo.webp')}}" alt="">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $(document).ready(function() {
       $('#receptionWeekAvailable').select2({
         placeholder: 'Select available days',
       });
       $('#multilingualstafflanguagelist').select2({
         placeholder: 'Select Languages',
       });
       $('#receptionWeekAvailable').on('change', function() {
               // Get count of selected options
               var selectedCount = $(this).val() ? $(this).val().length : 0;
               console.log('Selected days count:', selectedCount);
               $('#selectedDaysCount').val( selectedCount + ' '+'Weeks a day ');
           });
   });
</script>
<script>
   function kennelcomplimentaryOrFee() {
           var selectElement = document.getElementById("kennelcomplimentaryOrFee");
           var additionalOptions = document.getElementById("kennelfeeul");
         
   
           if (selectElement.value === "fee") {
               additionalOptions.style.display = "block";            
               
           } else {
               additionalOptions.style.display = "none";           
              
               
           }
       }
   function kennelsavailablehrs() {
           var selectElement = document.getElementById("kennelsavailablehrs");
           var additionalOptions = document.getElementById("kennelsTime");
         
   
           if (selectElement.value === "no") {
               additionalOptions.style.display = "block";            
               
           } else {
               additionalOptions.style.display = "none";           
              
               
           }
       }
   function addactionkennels() {
           var selectElement = document.getElementById("kennels");
           var additionalOptions = document.getElementById("kenelAvailablehours");
           var kennelsTime = document.getElementById("kennelsTime");
         
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";            
               kennelsTime.style.display = "block";            
               
           } else {
               additionalOptions.style.display = "none";           
               kennelsTime.style.display = "none";           
              
               
           }
       }
   function nominalOrFree() {
           var selectElement = document.getElementById("nominalOrFree");
           var additionalOptions = document.getElementById("feePercycleul");
         
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";            
               
           } else {
               additionalOptions.style.display = "none";           
              
               
           }
       }

   function guestLaundaryFacility() {
           var selectElement = document.getElementById("guestLaundaryFacility");
           var additionalOptions = document.getElementById("guestlaundary24hrsul");
           var guestlaundaryFacilityHrs = document.getElementById("guestlaundaryFacilityHrs");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";            
               guestlaundaryFacilityHrs.style.display="block"
           } else {
               additionalOptions.style.display = "none";           
               guestlaundaryFacilityHrs.style.display="none"
               
           }
       }
   function guestlaundaryhr() {
           var selectElement = document.getElementById("guestlaundary24hr");
           var additionalOptions = document.getElementById("guestlaundaryFacilityHrs");
          // var convenienceopenTime = document.getElementById("convenienceopenTime");
   
           if (selectElement.value === "no") {
               additionalOptions.style.display = "block";            
             //  convenienceopenTime.style.display="block"
           } else {
               additionalOptions.style.display = "none";           
              // convenienceopenTime.style.display="none"
               
           }
       }
   function gitshopnewsstandhrs() {
           var selectElement = document.getElementById("gitshopnewsstand24hrs");
           var additionalOptions = document.getElementById("giftShopNewsstandtime");
          // var convenienceopenTime = document.getElementById("convenienceopenTime");
   
           if (selectElement.value === "no") {
               additionalOptions.style.display = "block";            
             //  convenienceopenTime.style.display="block"
           } else {
               additionalOptions.style.display = "none";           
              // convenienceopenTime.style.display="none"
               
           }
       }
   function gitshopnewsstand() {
           var selectElement = document.getElementById("gitshopnewsstand");
           var additionalOptions = document.getElementById("gitshopnewsstand24hrsul");
           var gitshopnewsstand24hrs = document.getElementById("giftShopNewsstandtime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";            
               gitshopnewsstand24hrs.style.display="block"
           } else {
               additionalOptions.style.display = "none";           
               gitshopnewsstand24hrs.style.display="none"
               
           }
       }
   function pharmconvenienceSundryStoreacy() {
           var selectElement = document.getElementById("pharmconvenienceSundryStoreacy");
           var additionalOptions = document.getElementById("convenienceopenhourdiv");
          // var convenienceopenTime = document.getElementById("convenienceopenTime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";            
             //  convenienceopenTime.style.display="block"
           } else {
               additionalOptions.style.display = "none";           
              // convenienceopenTime.style.display="none"
               
           }
       }
   
       function convenienceopenhour() {
           var selectElement = document.getElementById("convenienceopenhour");
           var additionalOptions = document.getElementById("convenienceopenTime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
              
           } else {
               additionalOptions.style.display = "none";
              
           }
       }
   function availablepharmacy() {
           var selectElement = document.getElementById("pharmacy");
           var additionalOptions = document.getElementById("pharmacyopenhourul");
           var pharmacyTime = document.getElementById("pharmacyTime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block";
               pharmacyTime.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none";
               pharmacyTime.style.display="none"
               
           }
       }
   
     function pharmacyopenhour() {
           var selectElement = document.getElementById("pharmacyopenhour");
           var additionalOptions = document.getElementById("pharmacyTime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none"
           }
       }
     function ActionahoursfrontDesk() {
           var selectElement = document.getElementById("24hoursfrontDesk");
           var additionalOptions = document.getElementById("deskAvailableTime");
   
           if (selectElement.value === "no") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none"
           }
       }
     function multilingualstaff() {
           var selectElement = document.getElementById("multilingualstaff");
           var additionalOptions = document.getElementById("multilingualstafflanguagelistul");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none"
           }
       }
     function safeDepositboxatFrontdesk() {
           var selectElement = document.getElementById("safeDepositboxatFrontdesk");
           var additionalOptions = document.getElementById("safedepositatfrondeskcomplmentaryorfeeul");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none"
           }
       }
       function safedepositatfrondeskcomplmentaryorfee() {
           var selectElement = document.getElementById("safedepositatfrondeskcomplmentaryorfee");
           var additionalOptions = document.getElementById("safedepositboxFeeul");
   
           if (selectElement.value === "fee") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none"
           }
       }
      
       function haveeveningreception() {
           var selectElement = document.getElementById("haveeveningreception");
           var additionalOptions = document.getElementById("eveningreceptionTime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
               receptionWeekAvailableul.style.display="block"
           } else {
               additionalOptions.style.display = "none";
               receptionWeekAvailableul.style.display="none"
           }
       }
       function showAdditionalbusinesscenter24hrs() {
           var selectElement = document.getElementById("businesscenter24hrs");
           var additionalOptions = document.getElementById("businesscentertime");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
           } else {
               additionalOptions.style.display = "none";
           }
       }
   
       function additionalActionIsthereafaxfee() {
           var selectElement = document.getElementById("Isthereafaxfee");
           var additionalOptions = document.getElementById("faxfeeperpageul");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
           } else {
               additionalOptions.style.display = "none";
           }
       }
       function additionalActionhighSpeedInternetAccess() {
           var selectElement = document.getElementById("highSpeedInternetAccess");
           var additionalOptions = document.getElementById("highSpeedInternetAccesscomplimentaryOrfeeul");
   
           if (selectElement.value === "yes") {
               additionalOptions.style.display = "block";
           } else {
               additionalOptions.style.display = "none";
           }
       }
       function highSpeedInternetAccesscomplimentaryOrfeeaction() {
           var selectElement = document.getElementById("highSpeedInternetAccesscomplimentaryOrfee");
           var additionalOptions = document.getElementById("highspeedinternetperdayul");
   
           if (selectElement.value === "fee") {
               additionalOptions.style.display = "block";
           } else {
               additionalOptions.style.display = "none";
           }
       }
   
       $(document).ready(function() {
           $('.next').click(function(e) {
             
               e.preventDefault(); // Prevent default link behavior
               window.location.href = '/hoteloverview'; // Replace 'next-page-url' with the URL of the next page
           });
       });
   
   
   
       
</script>