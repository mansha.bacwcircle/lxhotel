<div class="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <div class="left-cnt">
            <ul>
              <li class="one">
                <p>
                  1. Is your hotel switching from another brand?
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="switchingFromAnotherBrand" name="switchingFromAnotherBrand" >
                  <option value="">Select</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
              </select>  
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                  2. Current hotel name listed in the GDS
                </p>
              </li>
              <li class="two">
                <select  class="form-select" aria-label="Default select example" id="hotelNameListedInGDS" name="hotelNameListedInGDS" >
                  <option value="">Select</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
              </select>
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                  3. Name of chain
                </p>
              </li>
              <li class="two">
                <input type="text" class="form-control" id="nameOfChain" name="nameOfChain" placeholder="If not please write N/A" >
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                  4. Two letter chain code
                </p>
              </li>
              <li class="two">
                <input type="text" class="form-control" id="nameOfChain" name="nameOfChain" placeholder="If not please write N/A" >
              </li>
            </ul>
            <ul>
              <li class="one">
                <p>
                  5. Current GDS Codes
                </p>
              </li>
              <li class="two">
                <select class="form-select" id="currentGDSCode" name="currentGDSCode" onchange="showAdditionalOptions()">
                  <option value="">Select</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
                  <option value="no">N/A</option>
                  <!-- <option value="Amadeus">Amadeus</option>
                  <option value="WorldSpan">WorldSpan</option>
                  <option value="Sabre">Sabre</option>
                  <option value="Room Master/Apollo">Room Master/Apollo</option> -->
                 
              </select>
              </li>
              <li class="two" id="additionalOptions" style="display:none;">
                <select class="form-select">
                    <option value="">Select</option>
                    <option value="Amadeus">Amadeus</option>
                    <option value="WorldSpan">WorldSpan</option>
                    <option value="Sabre">Sabre</option>
                    <option value="Room Master/Apollo">Room Master/Apollo</option>
                </select>
            </li>
            </ul>
           
            <div class="bottom-buttons">
              <a href="Javascript:void(0);" class="previous">Previous</a>
              <a href="Javascript:void(0);" class="next">Next</a>
            </div>
          </div>

        </div>
        <div class="col-lg-5">
          <div class="right-cnt">
            <div class="right-image">
              <img src="{{url('site/assets/images/white-lx-logo.webp')}}" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
    function showAdditionalOptions() {
        var selectElement = document.getElementById("currentGDSCode");
        var additionalOptions = document.getElementById("additionalOptions");

        if (selectElement.value === "yes") {
            additionalOptions.style.display = "block";
        } else {
            additionalOptions.style.display = "none";
        }
    }

    $(document).ready(function() {
        $('.next').click(function(e) {
          
            e.preventDefault(); // Prevent default link behavior
            window.location.href = '/hoteloverview'; // Replace 'next-page-url' with the URL of the next page
        });
    });



    </script>