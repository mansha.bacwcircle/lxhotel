<h2>Order Status Update</h2>

<p>Dear {{ $username }},&nbsp;</p>

<p>Your order with ID {{ $order_id }} has been updated. Please see the details below:</p>

<table cellspacing="0" style="border-collapse:collapse; width:100%">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Order Status</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="border-color:#dddddd; border-style:solid; border-width:1px">{{ $productname }}</td>
			<td style="border-color:#dddddd; border-style:solid; border-width:1px">{{ $orderstatus }}</td>
		</tr>
	</tbody>
</table>

<p>Thank you for choosing {{ $sitename }} Total Beverages!</p>