<div style="max-width: 600px; margin: auto; padding: 20px; border: 1px solid #ddd; font-family: Arial, sans-serif;">

    <h2 style="color: #e77600;">Order Status Update</h2>

    <p>Dear {{ $username }},</p>

    <p>Your order with ID {{ $order_id }} has been updated. Please see the details below:</p>

    <table style="width: 100%; border-collapse: collapse; margin-top: 20px;">
        <thead>
            <tr>
                <th style="border: 1px solid #ddd; padding: 8px;">Product Name</th>
                <th style="border: 1px solid #ddd; padding: 8px;">Product Price</th>
                <th style="border: 1px solid #ddd; padding: 8px;">Order Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border: 1px solid #ddd; padding: 8px;">{{ $productname }} x {{ $productquantity }}</td>
                <td style="border: 1px solid #ddd; padding: 8px;">${{ $totalprice }}</td>
                <td style="border: 1px solid #ddd; padding: 8px;">{{ $orderstatus }}</td>

            </tr>
        </tbody>
    </table>
    <p>Thank you for choosing {{ $sitename }} Total Beverages!</p>

</div>