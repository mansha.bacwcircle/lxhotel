<p>Welcome to {{$ShopName}},</p>

<h2>Welcometo {{$ShopName}}, {{$CustomerName}}!</h2>

<p>Thank you for signing up. We are thrilled to have you on board. At {{$ShopName}}, we pride ourselves on offering the finest selection of wines, spirits, and beers. Whether you&#39;re a connoisseur or just exploring, we&#39;ve got something for everyone.</p>

<p>Here&#39;s what you can look forward to:</p>

<ul>
	<li>Exclusive member discounts</li>
	<li>Early access to new arrivals</li>
	<li>Invitations to tasting events</li>
	<li>Curated recommendations just for you</li>
</ul>

<p>To get started, why not explore our <a href="{{$ShopWebsite}}">latest collections</a>? Or if you have something specific in mind, let us know. We&#39;re here to help you find the perfect bottle.</p>

<!-- <p>As a special welcome, here&#39;s a coupon for 10% off your first purchase: <strong>WELCOME10</strong>. Just use it at checkout.</p> -->

<p><a href="{{$ShopWebsite}}">Shop Now</a></p>

<p>If you have any questions or need assistance, feel free to contact us at {{$ShopEmail}} or call us at {{$ShopPhoneNumber}}.</p>

<p>We look forward to serving you.</p>

<p>Cheers,<br />
The {{$ShopName}} Team</p>

<p>{{$ShopAddress}}</p>