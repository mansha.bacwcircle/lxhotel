<p><img alt="Logo" src="{{ URL::to($sitesettings['logo']) }}" style="margin-bottom:20px" /></p>

<h1>Forget Password Email</h1>

<p>You can reset your password by clicking the link below:</p>

<p><a href="{{ route('user.reset.password.get', $token) }}">Reset Password</a></p>