<p>{{ $title }}</p>

<h2>Welcome to Our Newsletter , {{ $username }}!</h2>

<p>We are excited to have you as a subscriber. Stay tuned for exciting updates and news from our newsletter.</p>

<p>Thank you for joining us!</p>