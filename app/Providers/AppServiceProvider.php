<?php

namespace App\Providers;
use App\Models\Brand;
use App\Models\Sitesetting;
use App\Models\Settings;
use App\Models\User;
use App\Models\Order;
use App\Models\Seosettings;
use View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $sitesetting  = Sitesetting::find(1);
        if ($sitesetting) {
            View::Share('sitesettings',$sitesetting);
        }

        $seosetting  = Seosettings::find(1);
        if ($seosetting) {
            View::Share('seosetting',$seosetting);
        }

        $admindetial  = Settings::find(1);

        if ($admindetial) {
            View::Share('admindetial',$admindetial);
        }

        // $brands = Brand::where('status', '=', 'active')->get();
        // if ($brands) {
        //     View::Share('brands',$brands);
        // }

        // $footerInfo= DB::table('configoptions')
        //         ->leftJoin('configs', 'configoptions.configid', '=', 'configs.id')
        //         ->leftJoin('pages', 'configoptions.id', '=', 'pages.title')
        //             ->select('configoptions.id as id', 'configoptions.optionname as optionname','pages.name as pagename','pages.id as pageid','configs.status as config_status','pages.id as page_status')            
        //             ->where('configs.slug', '=', 'footer-page-list')
        //             ->where('pages.status', '=', 'active')
        //             ->where('configs.status', '=', 'active')
        //             ->get();
        // //  echo"<pre>";print_r($footerInfo);exit;
        // View::Share('footerInfo',$footerInfo);


        // $totalusers = User::count();
        // View::Share('totalusers',$totalusers);

        // Get today's date
        $today = now()->format('Y-m-d');

        // Get today's orders
       // $todaysOrders = Order::whereDate('created_at', $today)->get();

        // Count the number of orders for today
        //$todaysCount = $todaysOrders->count();

        // Get yesterday's date
        //$yesterday = now()->subDay()->format('Y-m-d');

        // Get yesterday's orders
        //$yesterdaysOrders = Order::whereDate('created_at', $yesterday)->get();

        // Count the number of orders for yesterday
       // $yesterdaysCount = $yesterdaysOrders->count();

        // Calculate the percentage change
        // $percentageChange = ($yesterdaysCount != 0) ? (($todaysCount - $yesterdaysCount) / $yesterdaysCount) * 100 : 100;

        // View::share('todaysCount', $todaysCount);
        // View::share('percentageChange', $percentageChange);

        // // Calculate the total revenue for today
        // $totalRevenue = $todaysOrders->sum('totalprice');

        // // Get yesterday's total revenue
        // $yesterdaysTotalRevenue = Order::whereDate('created_at', $yesterday)->sum('totalprice');

        // // Calculate the percentage change in revenue
        // $percentageChangeRevenue = ($yesterdaysTotalRevenue != 0) ? (($totalRevenue - $yesterdaysTotalRevenue) / $yesterdaysTotalRevenue) * 100 : 100;

        // View::share('totalRevenue', $totalRevenue);
        // View::share('percentageChangeRevenue', $percentageChangeRevenue);


        // Get the current month and year
        // $currentMonth = now()->format('m');
        // $currentYear = now()->format('Y');

        // // Get new members for this month with a limit of 7
        // $newmembers = User::whereMonth('created_at', $currentMonth)
        //     ->whereYear('created_at', $currentYear)
        //     ->orderBy('created_at', 'desc')
        //     ->limit(7)
        //     ->get();

        // View::share('newmembers', $newmembers);

        // Validator::extend('validateName', function ($attribute, $value, $parameters, $validator) {
        //     return preg_match('/^[A-Za-z\s]+$/', $value);
        // });
        // Validator::replacer('validateName', function ($message, $attribute, $rule, $parameters) {
        //     return str_replace(':attribute', $attribute, 'The :attribute must contain only letters and spaces.');
        // });
    }
}
