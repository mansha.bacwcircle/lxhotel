<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class DataExport implements FromCollection, WithHeadings
{
    protected $data;

    public function __construct(Collection $data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        $counter = 1;
        // Map the data to contain only the selected fields
        return $this->data->map(function ($item) use (&$counter) {
            return [
                '#' => $counter++,
                'ID' => "P-".str_pad($item->id, 6, '0', STR_PAD_LEFT),
                'Name' => $item->productname,
                'Title' => $item->title,
                'Description' => $item->description,
                'Brand'=>$item->brandname,
                'Stock' => $item->stock,
                'Regular Price' => $item->regularprice,
                'Sale Price' => $item->saleprice,
            ];
        });
    }

    public function headings(): array
    {
        // Define headers for the selected fields
        return [
            '#',
            'Code #',
            'Name',
            'Title',
            'Description',
            'Brand',
            'Stock',
            'Purchase Price',
            'Sale Price'
        ];
    }
}
