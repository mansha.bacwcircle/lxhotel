<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class MaincategoryExport implements FromCollection, WithHeadings
{
    protected $data;

    public function __construct(Collection $data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        $counter = 1;
        // Map the data to contain only the selected fields
        return $this->data->map(function ($item) use (&$counter) {
            return [
                '#' => $counter++,
                'ID' => "MCT-".str_pad($item->id, 6, '0', STR_PAD_LEFT),
                'Name' => $item->name,
                'Description' => $item->description
            ];
        });
    }

    public function headings(): array
    {
        // Define headers for the selected fields
        return [
            '#',
            'Code #',
            'Name',
            'Description'
        ];
    }
}
