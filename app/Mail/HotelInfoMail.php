<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HotelInfoMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $link;
    public $token; // Add a property for the token


    /**
     * Create a new message instance.
     */
    public function __construct($link, $token)
    {
        $this->link = $link;
        $this->token = $token; // Assign the token to a property
    }

    /**
     * Build the message.
     */
    public function build()
    {
        return $this->view('emails.hotel_info_mail')
        ->with(['link' => $this->link, 'token' => $this->token]); // Pass the token to the view
    }
}
