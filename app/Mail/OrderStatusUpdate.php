<?php



namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderStatusUpdate extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $email;
    public $subject;

    public function __construct($data, $email, $subject)
    {
        $this->data = $data;
        $this->email = $email;
        $this->subject = $subject;
    }

    public function build()
    {
        return $this->view('emails.admintemplate.order-update')
            ->to($this->email)
            ->subject($this->subject);
    }
}

