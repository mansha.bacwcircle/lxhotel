<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HotelExport; // Import your export class
 use App\Models\Brandswitch;
use View;
use PDF;

class BrandswitchController extends Controller {

   
   
    public function brand_switch() {
      
            return view('site.brandswitch.index');
       
    }

    public function add_brand_switch(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'switchingFromAnotherBrand' => 'required',
            'hotelNameListedInGDS' => 'required',
            'nameOfChain' => 'required',
            'twoletterchain' => 'required',
            'currentGDSCode' => 'required',
        ]);

        // Create a new instance of the BrandSwitch model
        $brandSwitch = new BrandSwitch();

        // Assign form data to model attributes
        $brandSwitch->switching_from_another_brand = $request->input('switchingFromAnotherBrand');
        $brandSwitch->hotel_name_listed_in_GDS = $request->input('hotelNameListedInGDS');
        $brandSwitch->name_of_chain = $request->input('nameOfChain');
        $brandSwitch->two_letter_chain_code = $request->input('twoletterchain');
        $brandSwitch->current_GDS_codes = $request->input('currentGDSCode');
        $brandSwitch->created_at = Carbon::now();


        // Save the model to the database
        $brandSwitch->save();

        // Redirect or respond with a success message
        return redirect()->back()->with('success', 'Brand switch data has been saved successfully!');
    }

   

}
