<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HotelExport; // Import your export class
 use App\Models\Hoteloverview;
use View;
use PDF;

class HoteloverviewController extends Controller {

    public function overview() {
       
            return view('site.hoteloverview.index');
       
    }
    public function hotelamenities() {
       
            return view('site.hotelamenities.index');
       
    }
    public function hotelservices() {
       
            return view('site.hotelservices.index');
       
    }
    public function brand_switch() {
        if (session()->get('adminname')) {
            return view('admin.hotel.brand_switch');
        } else {
            session()->flash('messageType', 'error');
            session()->flash('message', 'Login First!');
            return redirect()->to('adminlogin');
        }
    }

    public function add_brand_switch(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'switchingFromAnotherBrand' => 'required',
            'hotelNameListedInGDS' => 'required',
            'nameOfChain' => 'required',
            'twoletterchain' => 'required',
            'currentGDSCode' => 'required',
        ]);

        // Create a new instance of the BrandSwitch model
        $brandSwitch = new BrandSwitch();

        // Assign form data to model attributes
        $brandSwitch->switching_from_another_brand = $request->input('switchingFromAnotherBrand');
        $brandSwitch->hotel_name_listed_in_GDS = $request->input('hotelNameListedInGDS');
        $brandSwitch->name_of_chain = $request->input('nameOfChain');
        $brandSwitch->two_letter_chain_code = $request->input('twoletterchain');
        $brandSwitch->current_GDS_codes = $request->input('currentGDSCode');
        $brandSwitch->created_at = Carbon::now();


        // Save the model to the database
        $brandSwitch->save();

        // Redirect or respond with a success message
        return redirect()->back()->with('success', 'Brand switch data has been saved successfully!');
    }

    public function add_hotel_overview(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'hotelName' => 'required',
            'hotelDescription' => 'required',
            'smokeFreeHotel' => 'required',
            'nonSmokingRooms' => 'required',
            'allocatedSuits' => 'required|numeric',
            'allocatedAccessible' => 'required|numeric',
            'corridors' => 'required',
            'drivingDirection1' => 'required_without_all:drivingDirection2,drivingDirection3,drivingDirection4',
            'drivingDirection2' => 'required_without_all:drivingDirection1,drivingDirection3,drivingDirection4',
            'drivingDirection3' => 'required_without_all:drivingDirection1,drivingDirection2,drivingDirection4',
            'drivingDirection4' => 'required_without_all:drivingDirection1,drivingDirection2,drivingDirection3',
            'serviceLevel' => 'required',
        ]);
        

        // Create a new instance of the HotelInformation model
        $hotelInformation = new Hoteloverview();

        // Assign form data to model attributes
        $hotelInformation->hotel_name = $request->input('hotelName');
        $hotelInformation->hotel_description = $request->input('hotelDescription');
        $hotelInformation->smoke_free_hotel = $request->input('smokeFreeHotel');
        $hotelInformation->non_smoking_rooms_available = $request->input('nonSmokingRooms');
        $hotelInformation->total_allocated_non_smoking_rooms = $request->input('allocatedSuits');
        $hotelInformation->total_allocated_accessible_rooms = $request->input('allocatedAccessible');
        $hotelInformation->corridors_type = $request->input('corridors');
        $hotelInformation->driving_direction_1 = $request->input('drivingDirection1');
        $hotelInformation->driving_direction_2 = $request->input('drivingDirection2');
        $hotelInformation->driving_direction_3 = $request->input('drivingDirection3');
        $hotelInformation->driving_direction_4 = $request->input('drivingDirection4');
        $hotelInformation->service_level = $request->input('serviceLevel');
        $hotelInformation->created_at = Carbon::now();

        // Save the model to the database
        $hotelInformation->save();

        // Redirect or respond with a success message
        return redirect()->back()->with('success', 'Hotel overview data has been saved successfully!');
    }

   

}
