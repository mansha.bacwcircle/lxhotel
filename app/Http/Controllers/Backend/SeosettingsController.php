<?php

namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use View;
use App\Models\Seosettings;

class SeosettingsController extends Controller
{


    public function saveseosetting(Request $request)
    {
        print_r($request);exit();

        $input = $request->all();
        $id   = $input['id'];
        $seosettings                  =Seosettings::find($id);
        $seosettings->metatitle         =  $input['metatitle'];
        $seosettings->keyword           = $input['keyword'];
        $seosettings->description        = $input['description'];
        $seosettings->canonicaltag        = $input['tag'];
        $seosettings->analyticscode        = $input['analyticscode'];
        $seosettings->created_at      = Carbon::now();
        $seosettings->updated_at      = Carbon::now();
        $seosettings->update();
        Session::flash('messageType', 'success');
        Session::flash('message', 'SEO Settings successfully Update');
        return Redirect::route('admin.setting');
    }
}
