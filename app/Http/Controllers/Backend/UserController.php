<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Review;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        if (session()->get('adminname')) {
        $users = DB::table('users')
            ->leftJoin('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.id', 'users.name', 'users.email', 'users.image','users.created_at', DB::raw('COUNT(orders.id) as order_count'))
            ->groupBy('users.id', 'users.name', 'users.email', 'users.image','users.created_at')
            ->orderBy('id', 'desc')
            ->get();

        return view('admin.user.index', compact('users'));
    } else {
        session()->flash('messageType', 'error');
        session()->flash('message', 'Login First!');
        return redirect()->to('adminlogin');
    }
    }
    public function userprofile($id)
    {

        $user = User::find($id);
    
        if (!$user) {
            return redirect()->back()->with('error', 'User not found');
        }
        $userEmail = $user->email;
        $userNumber = $user->number;

        $totalOrders = Order::where('user_id', $user->id)->count();
    
        $totalReviews = Review::where('user_id', $user->id)->count();
    
        return view('admin.user.userprofile', compact('user', 'totalOrders', 'totalReviews','userEmail','userNumber'));
    }

    

}
