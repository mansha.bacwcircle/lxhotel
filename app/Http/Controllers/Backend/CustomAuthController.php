<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input; // Note: Input facade is deprecated, consider using request()
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;
use Illuminate\Support\Str;
use View;
use App\Models\Settings;
use App\Models\ResetPassword;

class CustomAuthController extends Controller {

    public function admindashboard() {
        if (session()->get('adminname')) {
            return view('admin.home.index');
        } else {
            session()->flash('messageType', 'error');
            session()->flash('message', 'Login First!');
            return redirect()->to('adminlogin');
        }
    }

    public function index() {
        return view('admin.auth.adminlogin');
    }

    public function postadminlogin(Request $request) {
        // echo "<pre>";print_r($request);exit;
        $validator = $request->validate([
            'email' => 'required',
            // 'password' => 'required',
            //'captcha' => 'required|captcha',
        ]);
        $input = $request->all();
        $email = $input['email'];
        $isEmailExist = $this->getbyemail($email);
        if($isEmailExist) {
            $password = $input['password'];
            $result = $this->checklogin($input['email'], $password);
            if ($result) {
                $this->updatelastlogin($input['email']);
                $adminuser = $this->getbyemail($input['email']);
                session()->put('adminname', $adminuser->name);
                session()->put('logintype', 'admin');
                return redirect()->to('admindashboard');
            } else {
                return redirect()->to('adminlogin')->with('messageType', 'danger')->with('message', 'Invalid Password');
            }
        } else {
            return redirect()->to('adminlogin')->with('messageType', 'danger')->with('message', 'Email does not exist');
        }
    }
    
    public function checklogin($email, $password) {
        $admin = Settings::where('email', '=', $email)->first();
        if ($admin) {
            $hashed = $admin->password;
            return Hash::check($password, $hashed);
        }
        return false;
    }

    public function updatelastlogin($email) {
        $admin = Settings::where('email', '=', $email)->first();
        $admin->lastlogindate = Carbon::now();
        $admin->save();
        return $admin->id;
    }

    public function getbyemail($email) {
        return Settings::where('email', '=', $email)->first();
    }

    public function adminlogout() {
        session()->forget('logintype');
        session()->forget('adminname');
        return redirect()->to('adminlogin');
    }

    public function reloadcaptcha() {
        return response()->json(['captcha'=>captcha_img('math')] );
    }

    public function forgetpassword() {
        return view('admin.auth.forgetpassword');
    }

    public function passwordresetrequest(Request $request) {
        $request->validate([
            'email' => 'required|email',
        ]);
        $isEmailExist = $this->getbyemail($request->email);
        if($isEmailExist) {
            $token = Str::random(64);
            DB::table('passwordresets')->insert([
                'email' => $request->email,
                'token' => $token,
                'who' => 'admin',
                'created_at' => Carbon::now()
            ]);
    
            $fromEmail = 'tharasasi333@gmail.com';
            $fromName = "Stones River";
    
            $viewName = view()->exists('emails.admintemplate.admin-forgetpassword') ? 'emails.admintemplate.admin-forgetpassword' : 'emails.admintemplate.default.default-admin-forgetpassword';
    
            Mail::send($viewName, ['token' => $token], function ($message) use ($request, $fromEmail, $fromName) {
                $message->from($fromEmail, $fromName);
                $message->to($request->email);
                $message->subject('Reset Password');
            });
    
            return back()->with('messageType', 'success')->with('message', 'Kindly verify your relevant email to reset your password!');
        } else {
            return back()->with('messageType', 'error')->with('message', 'Admin not found for the provided email!');

        }
    }
    

    public function showResetPasswordForm($token) {
        $passwordresets = DB::table('passwordresets')->where('token', '=', $token)->first();
        if($passwordresets) {
            return view('admin.auth.forgetpasswordlink', ['token' => $token]);
        } else {
            return redirect('/adminlogin')->with('messageType', 'danger')->with('message', 'Token has been expired. Try with new link');
        }
    }

    public function postresetpassword(Request $request) {
        $isEmailExist = $this->getbyemail($request->email);
        if($isEmailExist) {
            $updatePassword = DB::table('passwordresets')->where(['email' => $request->email,'token' => $request->token])->first();
            if(!$updatePassword) {
                return back()->withInput()->with('error', 'Invalid token!');
            }
            $user = Settings::where('email', $request->email)->update(['password' => Hash::make($request->password)]);
            DB::table('passwordresets')->where(['email'=> $request->email])->delete();
            return redirect('/adminlogin')->with('messageType', 'success')->with('message', 'Your password has been changed!');
        } else {
            return back()->with('messageType', 'danger')->with('message', 'Email does not exist');
        }
    }
}
