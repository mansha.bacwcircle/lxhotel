<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Settings;
use App\Models\Sitesetting;
use App\Models\Seosettings;
use App\Models\Mailsettings;
class SettingController extends Controller
{
    public function index(Request $request){
        $id       = $request->input('id');
        $setting  = Settings::find(1);
        $sitesetting  = Sitesetting::find(1);
        $seosetting=Seosettings::limit(1)->get();
        $seosettings=$seosetting[0];
    //    print_r($seosettings);
    //    exit();
        $mailsetting  = Mailsettings::limit(1)->get();
        $mailsettings=$mailsetting[0];
        //    print_r($mailsettings);
        //    exit();
        return view('admin.adminsetting.updatedetail', compact('setting','sitesetting','seosettings','mailsettings'));
    }
    public function savesetting(Request $request)
    {
      // dd($request->all());
      $input = $request->all();
      $name = $input['name'];
      $about   = $input['about'];
      $email   = $input['email'];
      $phone = $input['phone'];
      $address = $input['address'];
      $twitterlink = $input['twitterlink'];
      $facebooklink = $input['facebooklink'];
      $instagramlink = $input['instagramlink'];
      $youtubelink = $input['youtubelink'];
      $id   = $input['id'];
      $settingInfo  = Settings::find($id);
      $destinationPath    = 'uploads/images/settings';
      $settinglink = "";

      if ($request->hasFile('image')) {
          $logoimg = $request->file('image');
          $logoext = $logoimg->getClientOriginalExtension();

          // Correct the file extension check
              $filename = rand(100000, 999999);
              $file = $filename . '.' . $logoext;
              $logoimg->move($destinationPath, $file);
              $settinglink = $destinationPath . '/' . $file;
              $settingInfo->image = $settinglink;
      }
      $settingInfo->name = $name;
      $settingInfo->about = $about;
      $settingInfo->email = $email;
      $settingInfo->phone = $phone;
      $settingInfo->address = $address;
      $settingInfo->twitterlink = $twitterlink;
      $settingInfo->facebooklink = $facebooklink;
      $settingInfo->instagramlink = $instagramlink;
      $settingInfo->youtubelink = $youtubelink;
      $settingInfo->update();
      Session::flash('messageType', 'success');
      Session::flash('message', 'Profile successfully updated!');
      return redirect()->route('admin.setting');

    }

    public function savesitesetting(Request $request)
    {
        $input = $request->all();
        $name = $input['name'];
        $id   = $input['id'];
        $sitesetting  = Sitesetting::find($id);

        // Process logo image
        $destinationPath = 'uploads/images/sitesettings';
        $settinglink = "";

        if ($request->hasFile('logo_image') && $request->file('logo_image')->isValid()) {
            $logoimg     = $request->file('logo_image');
            $logoext     = strtolower($logoimg->getClientOriginalExtension());

            // Check if the file extension is allowed
                $filename = rand(100000, 999999);
                $file = $filename . '.' . $logoext;

                // Check if the destination path exists and is writable
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0755, true);
                }

                $logoimg->move($destinationPath, $file);
                $settinglink = $destinationPath . '/' . $file;
                $sitesetting->logo = $settinglink;
        }


        $destinationPath = 'uploads/images/sitesettings/favicon';
        $settinglink = "";

        if ($request->hasFile('favicon_image') && $request->file('favicon_image')->isValid()) {
            $faviconimg  = $request->file('favicon_image');
            $faviconext  = strtolower($faviconimg->getClientOriginalExtension());
                $filename = rand(100000, 999999);
                $file = $filename . '.' . $faviconext;
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0755, true);
                }
                $faviconimg->move($destinationPath, $file);
                $settinglink = $destinationPath . '/' . $file;
                $sitesetting->favicon = $settinglink;
        }

        $sitesetting->name = $name;
        $sitesetting->save();

        Session::flash('messageType', 'success');
        Session::flash('message', 'Sitesetting successfully added!');
        return redirect()->route('admin.setting');
    }

}
