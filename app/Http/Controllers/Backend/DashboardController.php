<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Hotel;

class DashboardController extends Controller {

    public function index() {
        if (!Session::get('adminname')) {
            return Redirect::to('adminlogin');
        } else {  
           
            $totalhotels=Hotel::where('deleted_at','0')->count();   
           
             return view('admin.home.index',compact('totalhotels'));
        }
    }

   
}
