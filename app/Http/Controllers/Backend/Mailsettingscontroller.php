<?php

namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use View;
use App\Models\Mailsettings;

class MailsettingsController extends Controller
{


    public function savemailsetting(Request $request)
    {

        $input = $request->all();
        $id   = $input['id'];
        $seosettings                  = Mailsettings::find($id);
        $seosettings->port         =  $input['port'];
        $seosettings->name           = $input['name'];
        $seosettings->key        = $input['key'];
        $seosettings->created_at      = Carbon::now();
        $seosettings->updated_at      = Carbon::now();
        $seosettings->save();
        Session::flash('messageType', 'success');
        Session::flash('message', 'Mail Settings successfully added');
        return Redirect::route('admin.setting');
    }
}
