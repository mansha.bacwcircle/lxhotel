<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\Models\Settings;

class ChangepasswordController extends Controller
{
    public function addpassword(Request $request)
    {
      $id       =  $request->input('id');
      $setting  = Settings::find(1);
      // echo "<pre>";print_r($setting);"<pre>";exit;
      return view('admin.profile.index',compact('setting'));
    }

    public function changepassword(Request $request)
    {
      $input = $request->all();
      $password =Hash::make($input['password']);
      $id  = $input['id'];
      $admin  = Settings::find($id);
      $admin->password= $password;
      $admin->update();
      Session::flash('messageType', 'success');
      Session::flash('message', 'Password changed successfully!');
      return Redirect::route('admin.setting');
    }
}
