<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HotelExport; // Import your export class
use App\Models\Hotel; 
 use App\Models\Brandswitch;
 use App\Models\Hoteloverview;
 use App\Mail\HotelInfoMail;
use View;
use PDF;

class HotelController extends Controller {


    public function index()
    {
        if (!Session::get('adminname')) {
            return Redirect::to('adminlogin');
        } else {  
        $hotellist = Hotel::orderBy('id', 'desc')->where('deleted_at', '0')->get();
        return view('admin.hotel.index', compact('hotellist'));
                    
        }
    }
    public function addhotel()
    {
        return view('admin.hotel.addhotel');
    }

    public function postaddhotel(Request $request)
    {
        $hotel = new Hotel;
        $hotel->name = ucwords(strtolower($request->name));
        $hotel->owner = ucwords(strtolower($request->owner));
        $hotel->email = $request->email;
        $hotel->save();
        session()->flash('messageType', 'success');
        session()->flash('message', 'Hotel added Successfully');
        return redirect()->to('/hotellist');
    }

    public function edithotel($id)
    {
        $hotelInfo = Hotel::find($id);
        return view('admin.hotel.edithotel', compact('hotelInfo'));
    }

    public function updatehotel(Request $request) {
        $input = $request->all();

        $id = $input['id'];
        $brandInfo = Hotel::find($id);

        $existingBrand = Hotel::where('name', ucfirst($input['name']))->where('id', '!=', $id)->first();
        if ($existingBrand) {
            $errorMessage = 'Brand name already present in database.';
            return view('admin.brand.editbrand')->with('errorMessage', $errorMessage)->with('brandInfo',$brandInfo);
        }
        $brandInfo->name = ucfirst($input['name']);
        $brandInfo->owner = ucfirst($input['owner']);
        $brandInfo->email = $input['email'];
        if ($brandInfo->isDirty()) {
            $brandInfo->updated_at = Carbon::now();
            $brandInfo->save();

            session()->flash('messageType', 'success');
            session()->flash('message', 'Hotel successfully updated');
            return redirect()->to('/hotellist');

        } else {
            return redirect()->to('/hotellist');
        }
    }

    public function softdeletehotel(){
        $id = request('id');
        $brand = Hotel::find($id);

        $brand->deleted_at = '1';
        $brand->save();
        session()->flash('messageType', 'success');
        session()->flash('message', 'Brand temporary deleted!');
        return redirect()->to('hotellist');
    }



public function sendMail($id)
{
    $hotelInfo = Hotel::find($id);
    $token = Str::random(32); // Generate a random token
    $link = "http://localhost:8000/branch_switch?token=" . $token; // Append the token to the link URL
    $mailto = $hotelInfo->email;

    try {
        // Send email
        Mail::to($mailto)->send(new HotelInfoMail($link, $token));

        // Update token and send flag in the database
        $hotelInfo->token = $token;
        $hotelInfo->send_flag = 1;
        $hotelInfo->save();

        // Email sent successfully
        session()->flash('messageType', 'success');
        session()->flash('message', 'Email sent successfully.');
    } catch (\Exception $e) {
        // Handle failure to send email
        session()->flash('messageType', 'error');
        session()->flash('message', 'Failed to send email: ' . $e->getMessage());
    }

    return redirect()->to('hotellist');
}



     public function docsend($token) {
        $document = DB::table('hotels')->where('token', '=', $token)->first();
        if($document) {
           
        }
    }

    public function overview() {
        if (session()->get('adminname')) {
            return view('admin.hotel.hoteloverview');
        } else {
            session()->flash('messageType', 'error');
            session()->flash('message', 'Login First!');
            return redirect()->to('adminlogin');
        }
    }
    public function hotel_amenities() {
        if (session()->get('adminname')) {
            return view('admin.hotel.hotelamenities');
        } else {
            session()->flash('messageType', 'error');
            session()->flash('message', 'Login First!');
            return redirect()->to('adminlogin');
        }
    }
    public function brand_switch() {
        if (session()->get('adminname')) {
            return view('admin.hotel.brand_switch');
        } else {
            session()->flash('messageType', 'error');
            session()->flash('message', 'Login First!');
            return redirect()->to('adminlogin');
        }
    }

    public function add_brand_switch(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'switchingFromAnotherBrand' => 'required',
            'hotelNameListedInGDS' => 'required',
            'nameOfChain' => 'required',
            'twoletterchain' => 'required',
            'currentGDSCode' => 'required',
        ]);

        // Create a new instance of the BrandSwitch model
        $brandSwitch = new BrandSwitch();

        // Assign form data to model attributes
        $brandSwitch->switching_from_another_brand = $request->input('switchingFromAnotherBrand');
        $brandSwitch->hotel_name_listed_in_GDS = $request->input('hotelNameListedInGDS');
        $brandSwitch->name_of_chain = $request->input('nameOfChain');
        $brandSwitch->two_letter_chain_code = $request->input('twoletterchain');
        $brandSwitch->current_GDS_codes = $request->input('currentGDSCode');
        $brandSwitch->created_at = Carbon::now();


        // Save the model to the database
        $brandSwitch->save();

        // Redirect or respond with a success message
        return redirect()->back()->with('success', 'Brand switch data has been saved successfully!');
    }

    public function add_hotel_overview(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'hotelName' => 'required',
            'hotelDescription' => 'required',
            'smokeFreeHotel' => 'required',
            'nonSmokingRooms' => 'required',
            'allocatedSuits' => 'required|numeric',
            'allocatedAccessible' => 'required|numeric',
            'corridors' => 'required',
            'drivingDirection1' => 'required_without_all:drivingDirection2,drivingDirection3,drivingDirection4',
            'drivingDirection2' => 'required_without_all:drivingDirection1,drivingDirection3,drivingDirection4',
            'drivingDirection3' => 'required_without_all:drivingDirection1,drivingDirection2,drivingDirection4',
            'drivingDirection4' => 'required_without_all:drivingDirection1,drivingDirection2,drivingDirection3',
            'serviceLevel' => 'required',
        ]);
        

        // Create a new instance of the HotelInformation model
        $hotelInformation = new Hoteloverview();

        // Assign form data to model attributes
        $hotelInformation->hotel_name = $request->input('hotelName');
        $hotelInformation->hotel_description = $request->input('hotelDescription');
        $hotelInformation->smoke_free_hotel = $request->input('smokeFreeHotel');
        $hotelInformation->non_smoking_rooms_available = $request->input('nonSmokingRooms');
        $hotelInformation->total_allocated_non_smoking_rooms = $request->input('allocatedSuits');
        $hotelInformation->total_allocated_accessible_rooms = $request->input('allocatedAccessible');
        $hotelInformation->corridors_type = $request->input('corridors');
        $hotelInformation->driving_direction_1 = $request->input('drivingDirection1');
        $hotelInformation->driving_direction_2 = $request->input('drivingDirection2');
        $hotelInformation->driving_direction_3 = $request->input('drivingDirection3');
        $hotelInformation->driving_direction_4 = $request->input('drivingDirection4');
        $hotelInformation->service_level = $request->input('serviceLevel');
        $hotelInformation->created_at = Carbon::now();

        // Save the model to the database
        $hotelInformation->save();

        // Redirect or respond with a success message
        return redirect()->back()->with('success', 'Hotel overview data has been saved successfully!');
    }

    // public function getHotelList() {
    //     return Hotel::orderBy('id', 'desc')->where('deleted_at','0')->get();
    // }

    // public function addHotel() {
    //     return view('admin.hotel.addhotel');
    // }

    // public function postaddhotel(Request $request) {
    //     $input = $request->all();
    //     $imagePath = 'uploads/images/hotels';
    //     if ($request->hasFile('image')) {
    //         $hotelImage = $request->file('image');
    //         $imageExtension = $hotelImage->getClientOriginalExtension();
    //         $filename = rand(100000, 999999);
    //         $file = $filename . '.' . $imageExtension;
    //         $hotelImage->move($imagePath, $file);
    //         $imageLink = $imagePath . '/' . $file;
    //         $input['image'] = $imageLink;
    //     }
    //     $result = $this->createHotel($input);
    //     if($result==1)
    //     {
    //     session()->flash('messageType', 'success');
    //     session()->flash('message', 'Hotel successfully added');
    //     }
    //     else{
    //         session()->flash('messageType', 'fail');
    //         session()->flash('message', 'TagId already Present');
    //     }
    //     return redirect()->to('/hotellist');
    // }

    // public function createHotel(array $input) {
    //     $existingHotel = Hotel::where('gtagid', $input['tagid'])->exists();
    //     if ($existingHotel) {
    //         return 0;
    //     }
    
    //     $hotel = new Hotel;
    //     $hotel->name = ucwords($input['name']);
    //     $hotel->hostname = $input['hostname'];
    //     $hotel->email = $input['email'];
    //     $hotel->contact = $input['contact'];
    //     $hotel->gtagid = $input['tagid'];
    //     $hotel->googleanalyticscode = $input['analyticscode'];
    //     $hotel->image = $input['image'];
    //     $hotel->address = $input['address'];
    //     $hotel->status = "active";
    //     $hotel->created_at = Carbon::now();
    //     $hotel->updated_at = Carbon::now();
    //     $hotel->save();
    //     return 1;
    // }
    

    // public function hotelstatuschange(Request $request)
    // {
    //     $id = $request->input('id');
    //     $hotel = Hotel::find($id);

    //     if ($hotel) {
    //         $hotel->status = $hotel->status == 'inactive' ? 'active' : 'inactive';
    //         $hotel->save();
    //     }
    //     session()->flash('messageType', 'success');
    //     session()->flash('message', 'Hotel status changed successfully');
    //     return redirect()->back();
    // }

    



    // public function softDeleteHotel() {
    //     $id = request('id');
    //     $hotel = Hotel::find($id);
    
    //     if (!$hotel) {
    //         session()->flash('messageType', 'fail');
    //         session()->flash('message', 'Hotel not found.');
    //         return redirect()->to('hotellist');
    //     }
    //     $associatedhotel = DB::table('blogs')->where('hotelid', $hotel->id)->exists();
    //     if ($associatedhotel) {
    //         session()->flash('messageType', 'fail');
    //         session()->flash('message', 'Cannot delete the hotel because it is associated with one or more blog.');
    //         return redirect('hotellist');
    //     }
    
    //     $hotel->forceDelete();    
    //     session()->flash('messageType', 'success');
    //     session()->flash('message', 'Hotel deleted successfully!');
    //     return redirect()->to('hotellist');
    // }
    
    // public function edithotel($id) {
    //     $hotelInfo = Hotel::find($id);
    //     return view('admin.hotel.edithotel', compact('hotelInfo'));
    // }
    
    // public function postupdatehotel(Request $request) {
    //     $input = $request->all();
    //     $destinationPath = 'uploads/images/hotels';
    //     if ($request->file('image')) {
    //         $hotelImage = $request->file('image');
    //         $imageExtension = $hotelImage->getClientOriginalExtension();
    //         $filename = rand(100000, 999999);
    //         $file = $filename . '.' . $imageExtension;
    //         $hotelImage->move($destinationPath, $file);
    //         $imageLink = $destinationPath . '/' . $file;
    //         $input['image'] = $imageLink;
    //     }
    //     $result = $this->updateHotels($input);
    //     if($result == 1) {
    //         session()->flash('messageType', 'success');
    //         session()->flash('message', 'Hotel successfully updated');
    //     }
    //     return redirect()->to('/hotellist');
    // }
    
    // public function updateHotels(array $input) {
    //     $hotels = new Hotel;
    //     $id = $input['hotelid'];
    //     $data = $hotels->find($id);
    //     $data->name = ucwords($input['name']);
    //     $data->hostname = $input['hostname'];
    //     $data->email = $input['email'];
    //     $data->contact = $input['contact'];
    //     $data->gtagid = $input['tagid'];
    //     $data->googleanalyticscode = $input['analyticscode'];
    //    $data->address = $input['address'];

    //     if (isset($input['image']) && !empty($input['image'])) {
    //         if (file_exists($data->image)) {
    //             unlink($data->image);
    //         }
    //         $data->image = $input['image'];
    //     }
    //     if ($data->isDirty()) {
    //         $data->updated_at = Carbon::now();
    //         $data->save();
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    // public function viewhotel($id) {
    //     if (session('adminname')) {
    //         $hoteldetails = Hotel::where('id', $id)->get();
    //         return view('admin.hotel.hotel-details', compact('hoteldetails'));
    //     } else {
    //         session()->flash('messageType', 'error');
    //         session()->flash('message', 'Login First!');
    //         return redirect()->to('adminlogin');
    //     }
    // }
    
    // public function hotelPdf() {
    //     $allHotels = Hotel::where('deleted_at','0')->get();
    //     $pdf = \PDF::loadView('admin.hotel.exportHotel', compact('allHotels'));
    //     return $pdf->download('exportHotel' . time() . rand(99, 9999) . '.pdf');
    // }
    
    // public function hotelCsv() {
    //     $allHotels = Hotel::where('deleted_at','0')->get();
    //     $export = new HotelExport($allHotels);
    //     return Excel::download($export, 'ExportHotel' . time() . rand(99, 9999) . '.csv');
    // }
    
    // public function hotelTrashList() {
    //     if (!Session::get('adminname')) {
    //         return Redirect::to('adminlogin');
    //     } else {
    //         $hotelList = Hotel::where('deleted_at', '1')->get();
    //         return view('admin.trash.hotel.index', compact('hotelList'));
    //     }
    // }
    
    // public function restoreHotel() {
    //     $id = request('id');
    //     $hotel = Hotel::find($id);
    //     if ($hotel) {
    //         $hotel->deleted_at = '0';
    //         $hotel->save();
    //         session()->flash('messageType', 'success');
    //         session()->flash('message', 'Hotel restore successfully!');
    //         return redirect()->to('hotelTrashList');
    //     }
    // }
    
    // public function checkName(Request $request) {
    //     if ($request->has('name')) {
    //         $name = $request->input('name');
    //         $id = $request->input('id', null);
    //         $query = DB::table('hotels')->where('name', $name);
    //         if ($id !== null) {
    //             $query->where('id', '!=', $id);
    //         }
    //         $count = $query->count();
    //         $response = [];
    //         if ($count > 0) {
    //             $response['message'] = "Hotel name is already in the database.";
    //         } else {
    //             $response['message'] = "";
    //         }
    //         return response()->json($response);
    //     }
    // }
    

}
