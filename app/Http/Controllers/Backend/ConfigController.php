<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request as Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Models\Config;
use App\Models\Sitesetting;

use App\Models\Configoption;
use PDF;

class ConfigController extends Controller
{
    public function index()
    {
        if (Session::get('adminname')) {
            $allconfigs = $this->getConfigList();
            $sitesettings = Sitesetting::find(1);
           
            return view('admin.config.index', compact('sitesettings','allconfigs','sitesettings'));
        } else {
            Session::flash('messageType', 'error');
            Session::flash('message', 'Login First!');
            return Redirect::to('adminlogin');
        }
    }

    public function getConfigList()
    {
        
        return Config::orderBy('id', 'desc')->where('deleted_at','0')->get();
    }

    public function addconfig()
    {
        $sitesettings = Sitesetting::find(1);
        return view('admin.config.addconfig',compact('sitesettings'));
    }

    public function postaddconfig()
    {
        $input = Input::all();
        $name = ucfirst($input['name']);
        $slug = Str::slug($input['slug']);
        $isconfigAlreadyExist = Config::where('name', $name)->first();

        if ($isconfigAlreadyExist) {
            Session::flash('messageType', 'fail');
            Session::flash('message', 'Config already exist in database');
            return Redirect::to('/listconfig');
        } else {
            $config = new Config();
            $config->name = $name;
            $config->slug = $slug;
            $config->status = 'Active';
            $config->save();

            Session::flash('messageType', 'success');
            Session::flash('message', 'Config successfully added!');
            return Redirect::to('/listconfig');
        }
    }

    public function configstatuschange(Request $request)
    {
        $input = Input::all();
        $configid = $input['id'];
        $oldstatus = $input['oldstatus'];
        $changestatus = $this->statuschange($configid, $oldstatus);

        // Session::flash('messageType', 'success');
        // Session::flash('message', 'Config status successfully changed');

        return ($changestatus == 'active') ? 1 : 2;
    }

    public function statuschange($configid, $oldstatus)
    {
        $config = new Config;
        $changestatus = $config->find($configid);

        if ($oldstatus == 'active') {
            $changestatus->status = 'inactive';
        } else {
            $changestatus->status = 'active';
        }

        $changestatus->save();
        return $changestatus->status;
    }

    public function geteditconfig($id)
    {
        
        $configInfo = Config::find($id);
        $allconfigoptionInfo = Configoption::where('configid', $id)->where('deleted_at','0')->get();
        $sitesettings = Sitesetting::find(1);
        

        return view('admin.config.editconfig', compact('sitesettings','configInfo', 'allconfigoptionInfo'));
    }



    public function updateconfigoption(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];
        $configInfo = Config::find($id);
        $addMoreInputFields = $input['addMoreInputFields'];
    
        $existingConfigOptions = Configoption::where('configid', $configInfo->id)->get();
    
        $processedOptionNames = [];
        $updateOccurred = false;
    
        foreach ($addMoreInputFields as $key => $value) {
            $optionName = strval($value['subject']);
    
            if (!empty($optionName) && !in_array($optionName, $processedOptionNames)) {
                $existingConfigOption = $existingConfigOptions
                    ->where('optionname', ucfirst($optionName))
                    ->first();
    
                if ($existingConfigOption) {
                    $existingConfigOption->update([
                        'optionname' => ucfirst($optionName)
                    ]);
                    $updateOccurred = 0;
                } else {
                    $configoption = new Configoption();
                    $configoption->configid = $configInfo->id;
                    $configoption->optionname = ucfirst($optionName);
                    $configoption->save();
                    $updateOccurred = 1;
                }
    
                $processedOptionNames[] = ucfirst($optionName);
            }
        }
    
        $deletedOptionNames = $existingConfigOptions
            ->whereNotIn('optionname', $processedOptionNames)
            ->pluck('optionname');
    
        Configoption::where('configid', $configInfo->id)
            ->whereIn('optionname', $deletedOptionNames)
            ->delete();
    
        if ($updateOccurred==1) {
            Session::flash('messageType', 'success');
            Session::flash('message', 'Config Options have been updated!');
        }
        else{
            Session::flash('messageType', 'fail');
            Session::flash('message', 'Config Options already present in database!');
        }
    
        return Redirect::to('/listconfig');
    }
    
    

    public function deleteconfig()
    {
        $id = request('id');
        $config = Config::find($id);
        $config->deleted_at = '1';
        $config->save();

        session()->flash('messageType', 'success');
        session()->flash('message', 'Config deleted successfully!');

        return redirect()->to('listconfig');
    }

    public function deleteconfiguration()
    {
            $id = request('id');
            $config = Configoption::find($id);

            if ($config) {
                $config->forceDelete(); // Permanently delete the record
                session()->flash('messageType', 'success');
                session()->flash('message', 'Configuration option deleted successfully!');
                return 1; // Assuming you're returning a success indicator
            } else {
                // Handle the case where the record with the given ID doesn't exist
                session()->flash('messageType', 'error');
                session()->flash('message', 'Configuration option not found!');
                return 0; // Indicate failure
            }

    }

    public function configtrashlist()
    {
        if (Session::get('adminname')) {
            $allconfigs = Config::orderBy('id', 'desc')->where('deleted_at','1')->get();
            $sitesettings = Sitesetting::find(1);
           
            return view('admin.trash.config.index', compact('sitesettings','allconfigs'));
        } else {
            Session::flash('messageType', 'error');
            Session::flash('message', 'Login First!');
            return Redirect::to('adminlogin');
        }
    }

    public function restore(){ 
        $id = request('id');
        $city = Config::find($id);
        if ($city) {
            $city->deleted_at = '0';
            $city->save();
            session()->flash('messageType', 'success');
            session()->flash('message', 'Config restore successfully!');
            return redirect()->to('listconfig');
        }
    }

   
}
