<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sitesetting extends Model
{
    public $timestamps = false;
    protected $table='sitesettings';

    protected $fillable = [
        'name',
        'logo',
        'favicon',
    ];
}
