<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{public $timestamps = false;
    protected $table = 'passwordresets';

    protected $fillable = [
        'email',
        'token',
        'created_at',
    ];
}
