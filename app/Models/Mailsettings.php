<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mailsettings extends Model
{
    protected $table = 'mailsettings';
    protected $fillable = [
        'port',
        'name',
        'key',
    ];
}
