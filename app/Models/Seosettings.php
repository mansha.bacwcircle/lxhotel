<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seosettings extends Model
{
    public $timestamps = false;
    protected $table = 'seosettings';
    protected $fillable = [
        'id',
        'metatitle',
        'keyword',
        'description',
        'canonicaltag',
        'analyticscode',
    ];
}
