<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configoption extends Model
{
    public $timestamps = false;
    protected $table ='configoptions';
    protected $fillable =[
     'optionname',
    ];  
}
