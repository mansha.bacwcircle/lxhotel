<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hoteloverview extends Model
{
    public $timestamps = false;
    protected $table = 'hotel_information';

    protected $fillable = [
        'hotel_name', 'hotel_description', 'smoke_free_hotel', 'non_smoking_rooms_available', 'total_allocated_non_smoking_rooms',
        'total_allocated_accessible_rooms','corridors_type','driving_direction_1','driving_direction_2','driving_direction_3','driving_direction_4','service_level'
    ];

    // Define relationships with other models if necessary
}
