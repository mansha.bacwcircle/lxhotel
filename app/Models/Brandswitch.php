<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brandswitch extends Model
{
    public $timestamps = false;
    protected $table = 'brand_switch';

    protected $fillable = [
        'switching_from_another_brand', 'hotel_name_listed_in_GDS', 'name_of_chain', 'two_letter_chain_code', 'current_GDS_codes'
    ];

    // Define relationships with other models if necessary
}
