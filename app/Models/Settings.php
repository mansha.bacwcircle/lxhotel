<?php
namespace App\Models;
use Eloquent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

Class Settings extends Eloquent {
	public $timestamps = false;
}
