-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2024 at 05:58 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lxhotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand_switch`
--

CREATE TABLE `brand_switch` (
  `id` int(11) NOT NULL,
  `switching_from_another_brand` enum('yes','no') NOT NULL,
  `hotel_name_listed_in_GDS` enum('yes','no') NOT NULL,
  `name_of_chain` varchar(255) DEFAULT NULL,
  `two_letter_chain_code` varchar(100) DEFAULT NULL,
  `current_GDS_codes` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `brand_switch`
--

INSERT INTO `brand_switch` (`id`, `switching_from_another_brand`, `hotel_name_listed_in_GDS`, `name_of_chain`, `two_letter_chain_code`, `current_GDS_codes`, `created_at`, `updated_at`) VALUES
(1, 'no', 'no', 'gfsdhgfhsdg45645', '123', 'no', '2024-03-30 08:19:02', '2024-03-30 13:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `deleted_at` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `token` text NOT NULL,
  `send_flag` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `owner`, `email`, `status`, `deleted_at`, `created_at`, `updated_at`, `token`, `send_flag`) VALUES
(1, 'Taj Connemara', 'Santhosh', 'santhosh7@gmail.com', 'active', 0, '2024-03-30 10:09:57', '2024-03-30 09:16:02', '', 0),
(2, 'ITC Grand Chola Hotel', 'karthik', 'karthik@gmail.com', 'active', 0, '2024-03-30 10:09:57', NULL, '', 0),
(3, 'Aaaaaaaaaaaaaa', 'Aaaaaaaaaa', 'aaaaaaaaa@gmail.com', 'active', 1, '2024-03-30 05:41:54', '2024-03-30 08:49:01', '', 0),
(4, 'Savera Hotel', 'Savera', 'Savera8546@gmail.com', 'active', 0, '2024-03-30 05:59:41', '2024-03-30 08:39:29', '', 0),
(15, 'India Rupeem', 'Arunm', 'Madming@gmail.com', 'active', 1, '2024-03-30 07:41:07', '2024-03-30 09:16:51', '', 0),
(16, 'Hotel Marriott', 'Obeyrow', 'admin.mansha@yopmail.com', 'active', 0, '2024-03-31 15:01:51', '2024-03-31 19:27:48', '1SkcLKGGmbhJ05AXN7qQPKi7zOJuajUG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_information`
--

CREATE TABLE `hotel_information` (
  `id` int(11) NOT NULL,
  `hotel_name` varchar(255) NOT NULL,
  `hotel_description` text DEFAULT NULL,
  `smoke_free_hotel` enum('yes','no') NOT NULL,
  `non_smoking_rooms_available` enum('yes','no') NOT NULL,
  `total_allocated_non_smoking_rooms` int(11) NOT NULL,
  `total_allocated_accessible_rooms` int(11) NOT NULL,
  `corridors_type` enum('interior','exterior','interiorExterior') NOT NULL,
  `driving_direction_1` varchar(255) NOT NULL,
  `driving_direction_2` varchar(255) NOT NULL,
  `driving_direction_3` varchar(255) NOT NULL,
  `driving_direction_4` varchar(255) NOT NULL,
  `service_level` enum('Limited Service Offer','Full Service Offer') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `hotel_information`
--

INSERT INTO `hotel_information` (`id`, `hotel_name`, `hotel_description`, `smoke_free_hotel`, `non_smoking_rooms_available`, `total_allocated_non_smoking_rooms`, `total_allocated_accessible_rooms`, `corridors_type`, `driving_direction_1`, `driving_direction_2`, `driving_direction_3`, `driving_direction_4`, `service_level`, `created_at`, `updated_at`) VALUES
(1, 'asas', 'asasasas', 'no', 'no', 23, 24, 'interior', 'Service Level And Hotel Service', 'Service Level And Hotel Service', 'Service Level And Hotel Service', 'Service Level And Hotel Service', 'Limited Service Offer', '2024-03-30 08:38:18', '2024-03-30 14:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `seosettings`
--

CREATE TABLE `seosettings` (
  `id` int(11) NOT NULL,
  `metatitle` varchar(225) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `canonicaltag` varchar(255) DEFAULT NULL,
  `analyticscode` int(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `seosettings`
--

INSERT INTO `seosettings` (`id`, `metatitle`, `keyword`, `description`, `canonicaltag`, `analyticscode`, `created_at`, `updated_at`) VALUES
(1, 'rfhyrt', 'regregrgrere', 'yuiju', 'dfvgsdffdrevgrre', 545445, '2023-11-19 08:32:35', '2023-11-19 08:32:35');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `address` varchar(191) DEFAULT NULL,
  `birthday` varchar(191) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `twitterlink` varchar(255) DEFAULT NULL,
  `facebooklink` varchar(255) DEFAULT NULL,
  `instagramlink` varchar(255) DEFAULT NULL,
  `youtubelink` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `lastlogindate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `email`, `password`, `image`, `phone`, `address`, `birthday`, `about`, `twitterlink`, `facebooklink`, `instagramlink`, `youtubelink`, `updated_at`, `lastlogindate`) VALUES
(1, 'ACW-PMS', 'mansha.b@acwcircle.com', '$2y$10$X6sOj5j4wCrCgr9XuZ7PNOYSyQvdhDjKkY0FcKLqBU6p.puvoE2be', 'uploads/images/settings/374370.jpg', '978 957 7062', 'chennai', NULL, 'chennai', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', NULL, NULL, NULL, '0000-00-00 00:00:00', '2024-03-31 18:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `sitesettings`
--

CREATE TABLE `sitesettings` (
  `id` int(11) NOT NULL,
  `name` varchar(191) DEFAULT NULL,
  `logo` varchar(191) DEFAULT NULL,
  `favicon` varchar(191) DEFAULT NULL,
  `deleted_at` int(11) DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `sitesettings`
--

INSERT INTO `sitesettings` (`id`, `name`, `logo`, `favicon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ACW-PMS', 'uploads/images/sitesettings/344042.jpg', 'uploads/images/sitesettings/favicon/969784.jpg', 0, '2023-08-05 21:51:35', '2023-11-27 07:11:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand_switch`
--
ALTER TABLE `brand_switch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_information`
--
ALTER TABLE `hotel_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seosettings`
--
ALTER TABLE `seosettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sitesettings`
--
ALTER TABLE `sitesettings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand_switch`
--
ALTER TABLE `brand_switch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hotel_information`
--
ALTER TABLE `hotel_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seosettings`
--
ALTER TABLE `seosettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sitesettings`
--
ALTER TABLE `sitesettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
