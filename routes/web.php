<?php

use Illuminate\Support\Facades\Route;
//backend Controller
use App\Http\Controllers\Backend\CustomAuthController;
use App\Http\Controllers\Backend\DashboardController;
//  use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ChangepasswordController;
use App\Http\Controllers\Backend\ConfigController;
use App\Http\Controllers\Backend\MailsettingsController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\SeosettingsController;
use App\Http\Controllers\Backend\HotelController;
// use App\Http\Controllers\Backend\BlogController;
// use App\Http\Controllers\Backend\NewsController;


// Forntend Controller
use App\Http\Controllers\Frontend\AuthController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\BrandswitchController;
use App\Http\Controllers\Frontend\HoteloverviewController;
use App\Http\Controllers\Frontend\FooterInfoController;

//frontend
Route::get('/', [CustomAuthController::class, 'index'])->name('adminlogin');



//Footer Options
Route::get('/footerinfo', [FooterInfoController::class, 'index'])->name('site.footerinfopage');

// Backend  Custom Controller
Route::get('/adminlogin', [CustomAuthController::class, 'index'])->name('adminlogin');
Route::post('/postadminlogin', [CustomAuthController::class, 'postadminlogin']);
Route::get('/admindashboard', [DashboardController::class, 'index']);
Route::get('/adminlogout', [DashboardController::class, 'adminlogout']);
Route::get('/reloadcaptcha', [CustomAuthController::class, 'reloadcaptcha']);
Route::get('/forgetpassword', [CustomAuthController::class, 'forgetpassword'])->name('forgetpassword');
Route::post('/passwordresetrequest', [CustomAuthController::class, 'passwordresetrequest']);

//Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
Route::get('resetpassword/{token}', [CustomAuthController::class, 'showResetPasswordForm'])->name('resetpassword');


Route::post('postresetpassword', [CustomAuthController::class, 'postresetpassword'])->name('postresetpassword');


Route::get('/adminsetting',  [SettingController::class, 'index'])->name('admin.setting');
Route::post('/settings/post-add-setting',  [SettingController::class, 'savesetting'])->name('admin.savesetting');
Route::post('/settings/post-add-sitesetting',  [SettingController::class, 'savesitesetting'])->name('admin.savesitesetting');
Route::post('/settings/post-add-seosetting', [SeosettingsController::class, 'saveseosetting'])->name('admin.saveseosetting');
Route::post('/settings/post-add-mailsettings', [MailsettingsController::class, 'savemailsetting'])->name('admin.savemailsetting');


//ChangePassword
Route::get('/settings/change-password',  [ChangepasswordController::class, 'addpassword'])->name('admin.changepassword');
Route::post('/settings/post-add-password', [ChangepasswordController::class, 'changepassword'])->name('admin.postchangepassword');



//Backend category
Route::get('/categorylist', [CategoryController::class, 'index']);
Route::get('/addcategory', [CategoryController::class, 'addcategory']);
Route::post('/postaddcategory', [CategoryController::class, 'postaddcategory']);
Route::get('/categorychangestatus', [CategoryController::class, 'categorystatuschange']);
Route::get('/deletecategory', [CategoryController::class, 'softdeletecategory']);
Route::get('/editcategory/{id}', [CategoryController::class, 'editcategory']);
Route::post('editcategory/postupdatecategory', [CategoryController::class, 'postupdatecategory']);
Route::get('/checkcategoryname', [CategoryController::class, 'checkName']);
Route::get('/exportcategory', [CategoryController::class, 'categorypdf']);
Route::get('/exportcategorycsv', [CategoryController::class, 'cateoryCsv']);



// Config Management
Route::get('/listconfig', [ConfigController::class, 'index']);
Route::get('/addconfig', [ConfigController::class, 'addconfig']);
Route::post('/postaddconfig', [ConfigController::class, 'postaddconfig']);
Route::get('/editconfig/{id}', [ConfigController::class, 'geteditconfig']);
Route::post('editconfig/postupdateconfig', [ConfigController::class, 'updateconfig']);
Route::post('/editconfig/postupdateconfigoption', [ConfigController::class, 'updateconfigoption']);
Route::get('/deleteconfig', [ConfigController::class, 'deleteconfig']);
Route::get('/deleteconfiguration', [ConfigController::class, 'deleteconfiguration']);
Route::post('/configstatuschange', [ConfigController::class, 'configstatuschange']);


//config  
Route::get('/configtrashlist', [ConfigController::class, 'configtrashlist']);
Route::get('/restoreconfig', [ConfigController::class, 'restore']);
Route::get('/clearSession', function () {
    session()->forget(['messageType', 'message']);
    return redirect()->back();
});


//hotelcontroller
Route::get('/hotellist', [HotelController::class, 'index']);
Route::get('/addhotel', [HotelController::class, 'addhotel']);
Route::post('/postaddhotel', [HotelController::class, 'postaddhotel']);
Route::get('/hotelchangestatus', [HotelController::class, 'hotelstatuschange']);
Route::get('/edithotel/{id}', [HotelController::class, 'edithotel']);
Route::post('/postupdatehotel', [HotelController::class, 'updatehotel']);
Route::get('/softdeletehotel', [HotelController::class, 'softdeletehotel']);
Route::get('/checkhotel', [HotelController::class, 'checkhotel']);
Route::get('/sendMail/{id}', [HotelController::class, 'sendMail']);
//Route::get('docsend/{token}', [HotelController::class, 'showResetPasswordForm']);


Route::get('/branch_switch', [BrandswitchController::class, 'brand_switch']);
Route::get('/hoteloverview', [HoteloverviewController::class, 'overview']);
Route::get('/hotelamenities', [HoteloverviewController::class, 'hotelamenities']);
Route::get('/hotelservices', [HoteloverviewController::class, 'hotelservices']);




Route::get('/brand_switch', [HotelController::class, 'brand_switch']);
Route::post('/add_brand_switch', [HotelController::class, 'add_brand_switch']);
Route::get('/hotel_overview', [HotelController::class, 'overview']);
Route::post('/add_hotel_overview', [HotelController::class, 'add_hotel_overview']);
Route::get('/hotel_amenities', [HotelController::class, 'hotel_amenities']);

// Route::get('/bloglist', [BlogController::class, 'index']);
// Route::get('/blog-details/{id}', [BlogController::class, 'viewblog']);
// Route::get('/addblog', [BlogController::class, 'addblog']);
// Route::post('/postaddblog', [BlogController::class, 'postaddblog']);
// Route::get('/blogchangestatus', [BlogController::class, 'blogstatuschange']);
// Route::get('/deleteblog', [BlogController::class, 'softdeleteblog']);
// Route::get('/editblog/{id}', [BlogController::class, 'editblog']);
// Route::post('/editblog/postupdateblog', [BlogController::class, 'updateblog']);
// Route::get('/checkblogname', [BlogController::class, 'checkName']);
// Route::get('/blogtrashlist', [BlogController::class, 'blogtrashlist']);
// Route::get('/restoreblog', [BlogController::class, 'restore']);

// Route::get('/newslist', [NewsController::class, 'index']);
// Route::get('/news-details/{id}', [NewsController::class, 'viewnews']);
// Route::get('/addnews', [NewsController::class, 'addnews']);
// Route::post('/postaddnews', [NewsController::class, 'postaddnews']);
// Route::get('/newschangestatus', [NewsController::class, 'newsstatuschange']);
// Route::get('/deletenews', [NewsController::class, 'softdeletenews']);
// Route::get('/editnews/{id}', [NewsController::class, 'editnews']);
// Route::post('/editnews/postupdatenews', [NewsController::class, 'updatenews']);
// Route::get('/checknewsname', [NewsController::class, 'checkName']);
// Route::get('/newstrashlist', [NewsController::class, 'newstrashlist']);
// Route::get('/restorenews', [NewsController::class, 'restore']);
